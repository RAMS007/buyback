/**
 * Created by RAMS on 06.09.2017.
 */
var calculatedValues = [];
var calculatedOffer = 0; //offer with conditions
var BaseOffer = 0;//offer without conditions
$(document).ready(function () {


    $('#registerForm').on('submit', function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        console.log(data);
        $.post("/register", data, function (response) {
            console.log(response);
            if (response.error == false) {
                $('#userId').val(response.user.id);
                var current_fs = $('#authFieldset');
                var next_fs = current_fs.next();
                //activate next step on progressbar using the index of next_fs
                $("#progressbar li").removeClass("active");
                $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
                next_fs.show();
                current_fs.hide();
            } else {

if(response.email.length>0){

    $('span.regemail').html(response.email[0]);
    $('label.regemail').addClass('error');
}

                if(response.password.length>0) {
                    $('span.regpassword').html(response.password[0]);
                    $('label.regpassword').addClass('error');
                }


                if(response.FirstName.length>0) {
                    $('span.regFirstname').html(response.FirstName[0]);
                    $('label.regFirstname').addClass('error');
                }

                if(response.LastName.length>0) {

                    $('span.regLastname').html(response.LastName[0]);
                    $('label.regLastname').addClass('error');

                }

            }
        });
    });

    $('#check').on('submit', function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        console.log(data);
        $.post("/register/payments", data, function (response) {
            console.log(response);
            if (response.error == false) {


                var current_fs = $('#PaymentFieldset');
                var next_fs = current_fs.next();
                //activate next step on progressbar using the index of next_fs
                $("#progressbar li").removeClass("active");
                $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
                next_fs.show();
                current_fs.hide();
            } else {
                for (var k in response.errors) {
                    if (response.errors.hasOwnProperty(k)) {
                        var message = response.errors[k];

                        $('#' + k + '_error').html(response.errors[k][0]);
                    }
                }
            }
        });
    });


    $('#paypal').on('submit', function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        console.log(data);
        $.post("/register/payments", data, function (response) {
            console.log(response);
            if (response.error == false) {


                var current_fs = $('#PaymentFieldset');
                var next_fs = current_fs.next();
                //activate next step on progressbar using the index of next_fs
                $("#progressbar li").removeClass("active");
                $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
                next_fs.show();
                current_fs.hide();
            } else {
                for (var k in response.errors) {
                    if (response.errors.hasOwnProperty(k)) {
                        var message = response.errors[k];

                        $('#' + k + '_error').html(response.errors[k][0]);
                    }
                }
            }
        });
    });

    $('#shipping').on('submit', function (e) {
        e.preventDefault();
        var data = $(this).serialize();
        console.log(data);
        $.post("/register/shipping", data, function (response) {
            console.log(response);
            if (response.error == false) {


                var current_fs = $('#shippingFieldset');
                var next_fs = current_fs.next();
                //activate next step on progressbar using the index of next_fs
                $("#progressbar li").removeClass("active");
                $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
                next_fs.show();
                current_fs.hide();
            } else {
                for (var k in response.errors) {
                    if (response.errors.hasOwnProperty(k)) {
                        var message = response.errors[k];

                        $('#' + k + '_error').html(response.errors[k][0]);
                    }
                }
            }
        });
    });


    $('body').on('change', 'input[name="condition"]', function (e) {

        var OfferId = $('input[name="condition"]:checked').val();
        console.log($('input[name="condition"]:checked').val());

        console.log(calculatedOffer);

        calculatedValues.forEach(function (item) {

            console.log(item);
            if (item.Id == OfferId) {
                $('.selectedCondition').html(item.Name);

                if (item.Deduct == 1) {


                    calculatedOffer = BaseOffer - (BaseOffer * item.Percent) / 100;
                } else {

                    calculatedOffer = BaseOffer + (BaseOffer * item.Percent) / 100;
                }

            }

        })


        $('.calculatedOfer').html(calculatedOffer.toFixed(2));
        $('#calculatedOfer').val(calculatedOffer.toFixed(2));

    });


    $('#selectModel').on('click', function (e) {
        $('.preloader').fadeIn('fast');
        console.log('click');
        var model = $('input[name="modelId"]:checked').val();
        console.log(model);

        $.get('/model/' + model, function (response) {
            console.log(response);
            if (response.error == false) {

                $('#deviceId').val(response.device.id);
                $('#ModelId').val(response.model.id);
                var options, carriers = '';
                options = '<p>Select Your Macbook Model Below to Get an Instant Quote</p>';
                for (var k in response.parametrs) {
                    if (response.parametrs.hasOwnProperty(k)) {
                        var parametrs = response.parametrs[k];

                        if (k == 'carriers') {
                            console.log('skip carrier');
                            parametrs.forEach(function (item) {
                                carriers = carriers + '  <div class="radio">' +
                                    '<input id="br' + item.Id + '" name="carrier" type="radio" data-price="" value="' + item.Name + '">' +
                                    '<label for="br' + item.Id + '" class="radio-step2">' +
                                    '<div class="step2_img">' +
                                    '<img src="' + item.Image + '" alt="">' +
                                    '</div>' +
                                    '<h4>' + item.Name + '</h4>' +
                                    '</label>' +
                                    '</div> ';
                            });
                            continue;
                        }

                        if (parametrs.length > 0) {
                            console.log(k + 'hasOptions');
                            console.log(parametrs);
                            options = options + '<label>' +
                                '<span>' + k + '</span>' +
                                '<div class="select">' +
                                '<i class="arrow down"></i>' +
                                '<select name="' + k + '" >' +
                                '<option>Select</option>';
                            parametrs.forEach(function (item) {
                                console.log(item);
                                options = options + '<option value="' + item.Id + '">' + item.Name + '</option>';
                            });
                            options = options + '</select>' +
                                '</div>' +
                                '</label>';
                        } else {
                            console.log(k + ' hasnt options');
                        }
                    }
                }
                var current_fs = $('#ModelList');
                var next_fs = current_fs.next();
                //activate next step on progressbar using the index of next_fs
                $("#progressbar li").removeClass("active");
                $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
                next_fs.show();
                current_fs.hide();
                options = options + '<input type="button" name="next" class="subb" value="Proceed" id="parametrsBtn" />'
                $('#optionsList').html(options);
                $('#CarrierList').html(carriers);
                $('#optionsList >*').not(':nth-child(2)').hide();

            } else {
                alert('error with your request');
            }
            $('.preloader').fadeOut('slow');
        })
    })


    $('#SendForm').click(function () {

        var data = $('#msform').serialize();
        console.log(data);

        $.post("/order", data, function (response) {

            console.log(response)
            if (response.error == false) {

                alert('order created')
            } else {
                alert('Cant Create order');
            }

        });


    })


    $('body').on('click', '#parametrsBtn', function (e) {

        console.log('click');
        if ($('input[name=carrier]:checked').val() == undefined) {


            alert('Please select carrier');
            return false;


        }
        $('.preloader').fadeIn('fast');
        //   var data = $('#Parametrs').serialize();

        var data = $('#Parametrs :input').serialize();
        console.log(data);

        $.post("/calculate", data, function (response) {

            console.log(response)
            if (response.error == false) {

                calculatedValues = response.conditions;
                BaseOffer = response.offer;
                var conditions = '';
                var descriptions = '';
                $('.ProductImage').attr('src', '/' + response.device.ModelImage);
                $('.ModelName').html(response.device.ModelName);
                var ModelOptionsList = '';
                response.device.options.forEach(function (item) {
                    ModelOptionsList += item + ' ';
                })
                $('.ModelOptionsList').html(ModelOptionsList);

                var hasckecked = false;
                response.conditions.forEach(function (item) {
                    /*        if ( (item.condition_description1 == null) &&  (item.condition_description2 == null) &&(item.condition_description3 == null)
                     && (item.condition_description4 == null)  && (item.condition_description5 == null)  && (item.condition_description6 == null) ){
                     continue;
                     }
                     */
                    conditions = conditions + '<div class="radio tab2">' +
                        '<input id="step3_' + item.Id + '" name="condition" type="radio" data-price="" ';

                    if (hasckecked == false) {
                        conditions = conditions + ' checked';
                        hasckecked = true;

                        if (item.Deduct == 1) {
                            calculatedOffer = BaseOffer - (BaseOffer * item.Percent) / 100;

                        } else {
                            calculatedOffer = BaseOffer + (BaseOffer * item.Percent) / 100;
                        }

                        $('.selectedCondition').html(item.Name);


                    }

                    conditions = conditions + ' value="' + item.Id + '">' +
                        '<label for="step3_' + item.Id + '" class="radio-step3">' +
                        '<div class="title_wrapp">' +
                        '<h4>' + item.Name + '</h4>' +
                        '</div>' +
                        //             '<p>Does not power on or has that affects normal use functio-nal or heavy physical damage that affects normal use</p>' +
                        '</label>' +
                        '</div>';


                    descriptions = descriptions + ' <div class="tab_item2">' +
                        '<div class="step3_cont">' +
                        '<h3>Your device is in this condition if ALL of the following are true:</h3>' +
                        '<ul>';


                    if (item.condition_description1 != null) {
                        descriptions = descriptions + '    <li>' + item.condition_description1 + '</li>';
                    }
                    if (item.condition_description2 != null) {
                        descriptions = descriptions + '    <li>' + item.condition_description2 + '</li>';
                    }
                    if (item.condition_description3 != null) {
                        descriptions = descriptions + '    <li>' + item.condition_description3 + '</li>';
                    }
                    if (item.condition_description4 != null) {
                        descriptions = descriptions + '    <li>' + item.condition_description4 + '</li>';
                    }
                    if (item.condition_description5 != null) {
                        descriptions = descriptions + '    <li>' + item.condition_description5 + '</li>';
                    }
                    if (item.condition_description6 != null) {
                        descriptions = descriptions + '    <li>' + item.condition_description6 + '</li>';
                    }
                    descriptions = descriptions + '</ul>' +
                        '</div>' +
                        '</div>';
                });

                $('#Conditions').html(conditions);
                $('#ConditionDescription').html(descriptions);

                $('.calculatedOfer').html(calculatedOffer.toFixed(2));
                $('#calculatedOfer').val(calculatedOffer.toFixed(2));

                $(".tab_item2").not(':first').hide();
                var current_fs = $('#ParametrList');
                var next_fs = current_fs.next();
                //activate next step on progressbar using the index of next_fs
                $("#progressbar li").removeClass("active");
                $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
                next_fs.show();
                current_fs.hide();


            } else {
                alert('Error with your request');

            }

            $('.preloader').fadeOut('slow');
        })


    })

});