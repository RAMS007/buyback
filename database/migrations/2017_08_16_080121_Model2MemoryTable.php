<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Model2MemoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Model2Memory', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ModelId');
            $table->unsignedInteger('MemoryId');
            $table->decimal('MemoryPrice', 7,2);
            $table->timestamps();
            $table->unique(['ModelId','MemoryId'],'uniq');
            $table->foreign('ModelId')->references('id')->on('Models');
            $table->foreign('MemoryId')->references('id')->on('Memory');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Model2Memory');
    }
}
