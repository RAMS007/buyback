<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Device2ConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Device2Conditions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('DeviceId');
            $table->unsignedInteger('ConditionId');
            $table->decimal('ConditionPricePercentage', 5,2);
            $table->boolean('deduct');
            $table->timestamps();
            $table->unique(['DeviceId','ConditionId'], 'uniq');
            $table->foreign('DeviceId')->references('id')->on('Devices');
            $table->foreign('ConditionId')->references('id')->on('Conditions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Device2Conditions');
    }
}
