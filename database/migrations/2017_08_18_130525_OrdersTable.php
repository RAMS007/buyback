<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->default(0);
            $table->unsignedInteger('deviceId');
            $table->unsignedInteger('modelId');
            $table->string('parametrs', 1024)->nullable();

            /*   colors:1
   storages:2
   screensizes:2
   processors:2
   harddrives:3
   memorys:1
   years:1
   accesories:2
   carrier:AT&T
   condition:1
   */


            $table->string('PaymentType', 255)->nullable();
            $table->string('Payments_AddressName', 255)->nullable();
            $table->string('Payments_FirstName', 255)->nullable();
            $table->string('Payments_LastName', 255)->nullable();
            $table->string('Payments_addres1', 255)->nullable();
            $table->string('Payments_addres2', 255)->nullable();
            $table->string('Payments_city', 255)->nullable();
            $table->string('Payments_zip', 255)->nullable();
            $table->string('shipping_email', 255)->nullable();
            $table->string('shipping_FirstName', 255)->nullable();
            $table->string('shipping_LastName', 255)->nullable();
            $table->string('shipping_Addres', 255)->nullable();
            $table->string('shipping_city', 255)->nullable();
            $table->string('shipping_zip', 255)->nullable();
            $table->string('shipping_phone', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('paypal_account', 255)->nullable();
            $table->string('paypal_account2', 255)->nullable();


            $table->decimal('offeredPrice', 10, 2)->default(0);
            $table->enum('status', ['new', 'completed', 'deleted'])->default('new');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Orders');
    }
}
