<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Model2ProcessorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Model2Processor', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ModelId');
            $table->unsignedInteger('ProcessorId');
            $table->decimal('ProcessorPrice', 7,2);
            $table->timestamps();
            $table->unique(['ModelId','ProcessorId'],'uniq');
            $table->foreign('ModelId')->references('id')->on('Models');
            $table->foreign('ProcessorId')->references('id')->on('Processors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Model2Processor');
    }
}
