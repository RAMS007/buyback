<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Model2HardDriveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Model2HardDrive', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ModelId');
            $table->unsignedInteger('HardDriveId');
            $table->decimal('HardDrivePrice', 7,2);
            $table->timestamps();
            $table->unique(['ModelId','HardDriveId'],'uniq');
            $table->foreign('ModelId')->references('id')->on('Models');
            $table->foreign('HardDriveId')->references('id')->on('HardDrive');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Model2HardDrive');
    }
}
