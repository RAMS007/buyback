<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Model2ScreenSizeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Model2ScreenSize', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ModelId');
            $table->unsignedInteger('ScreenSizeId');
            $table->decimal('ScreenSizePrice', 7,2);
            $table->timestamps();
            $table->unique(['ModelId','ScreenSizeId'],'uniq');
            $table->foreign('ModelId')->references('id')->on('Models');
            $table->foreign('ScreenSizeId')->references('id')->on('ScreenSizes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Model2ScreenSize');
    }
}
