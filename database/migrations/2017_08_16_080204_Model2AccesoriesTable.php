<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Model2AccesoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Model2Accesories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ModelId');
            $table->unsignedInteger('AccesorieId');
            $table->decimal('AccesoriePrice', 7,2);
            $table->timestamps();
            $table->unique(['ModelId','AccesorieId'],'uniq');
            $table->foreign('ModelId')->references('id')->on('Models');
            $table->foreign('AccesorieId')->references('id')->on('Accesoriers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Model2Accesories');
    }
}
