<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Models', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('DeviceId');
            $table->string('ModelName');
            $table->string('ModelImage');
            $table->decimal('ModelPrice', 10,2);
            $table->timestamps();
            $table->foreign('DeviceId')->references('id')->on('Devices');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Models');
    }
}
