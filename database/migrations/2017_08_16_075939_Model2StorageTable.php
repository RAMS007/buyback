<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Model2StorageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Model2Storage', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('ModelId');
            $table->unsignedInteger('StorageId');
            $table->decimal('StoragePrice', 7,2);
            $table->timestamps();
            $table->unique(['ModelId','StorageId'],'uniq');
            $table->foreign('ModelId')->references('id')->on('Models');
            $table->foreign('StorageId')->references('id')->on('Storage');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Model2Starage');
    }
}
