<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserShippingDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('UserShippingDetails', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('shipping_email')->default('');
            $table->string('shipping_FirstName')->default('');
            $table->string('shipping_LastName')->default('');
            $table->string('shipping_Addres')->default('');
            $table->string('shipping_city')->default('');
            $table->string('shipping_country')->default('');
            $table->string('shipping_province')->default('');
            $table->string('shipping_zip')->default('');
            $table->string('shipping_phone')->default('');




            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('UserShippingDetails');
    }
}
