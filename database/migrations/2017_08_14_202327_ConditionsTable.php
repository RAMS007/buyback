<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Conditions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ConditionName');
            $table->text('condition_description1');
            $table->text('condition_description2');
            $table->text('condition_description3');
            $table->text('condition_description4');
            $table->text('condition_description5');
            $table->text('condition_description6');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Conditions');
    }
}
