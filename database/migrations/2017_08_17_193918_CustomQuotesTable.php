<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CustomQuotes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category');
            $table->string('ProductBrand');
            $table->string('ProductModel');
            $table->string('ProductCondition');
            $table->string('ProductImage');
            $table->string('ProductQuantity');
            $table->string('ProductComment');
            $table->string('UserEmail');
            $table->enum('status',['new', 'readed', 'closed']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('CustomQuotes');
    }
}
