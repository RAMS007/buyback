<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Model2ColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Model2Colors', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ModelId');
            $table->unsignedInteger('ColorId');
            $table->decimal('ColorPrice', 7,2);
            $table->timestamps();
            $table->unique(['ModelId','ColorId'],'uniq');
            $table->foreign('ModelId')->references('id')->on('Models');
            $table->foreign('ColorId')->references('id')->on('Colors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Device2Colors');
    }
}
