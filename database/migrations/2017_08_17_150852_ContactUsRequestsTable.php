<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContactUsRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ContactUsRequests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('userEmail');
            $table->string('userPhone');
            $table->text('message');
            $table->enum('status',['new', 'readed', 'closed']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ContactUsRequests');
    }
}
