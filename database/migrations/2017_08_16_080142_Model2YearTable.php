<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Model2YearTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Model2Year', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ModelId');
            $table->unsignedInteger('YearId');
            $table->decimal('YearPrice', 7,2);
            $table->timestamps();
            $table->unique(['ModelId','YearId'],'uniq');
            $table->foreign('ModelId')->references('id')->on('Models');
            $table->foreign('YearId')->references('id')->on('Year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Model2Year');
    }
}
