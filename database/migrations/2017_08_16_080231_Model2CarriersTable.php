<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Model2CarriersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Model2Carriers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ModelId');
            $table->unsignedInteger('CarrierId');
            $table->decimal('CarrierPrice', 7,2);
            $table->timestamps();
            $table->unique(['ModelId','CarrierId'],'uniq');
            $table->foreign('ModelId')->references('id')->on('Models');
            $table->foreign('CarrierId')->references('id')->on('Carriers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Model2Carriers');
    }
}
