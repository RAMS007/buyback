@extends('layouts.app')

@section('mainContent')

    <div class="history_directory faq_directory">
        <div class="container">
            <a href="#">Home</a> /
            <span>Custom Quotes</span>
        </div>
    </div>


    <section class="quotes_bn">
        <div class="quotes_bn_2">
            <div class="container">
                <h2>Can't Find Something You Want to Sell on the Website?</h2>
                <p>We will provide a custom quote for you same day.  Just fill out the form below.</p>
            </div>
        </div>
    </section>

    <section class="information">
        <div class="container">
            <h4>Product Information</h4>
            <p>We need this information to provide you with accurate pricing for your item</p>
            <div class="info_form">

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif




                <form method="post" enctype="multipart/form-data" >

                    {{csrf_field()}}
                    <fieldset>
                        <div class="inform_cont">
                            <h3>1. Select the Category that best describes the item you want to sell</h3>
                            <p>We've listed some suggestions here on what we think you might want to sell, but if you still can't find your category, simply click Other at the bottom of the list and then fill in what your item is to the best of your abilities</p>
                        </div>
                        <div class="radio_cont st1_radio">

                            <div class="radio">
                                <input id="radio-1" name="category" type="radio" data-price="" value="Electronics"  cahecked>
                                <label for="radio-1" class="radio-label">Electronics</label>
                            </div>

                            <div class="radio">
                                <input id="radio-2" name="category" type="radio" data-price="" value="Designer fashion">
                                <label  for="radio-2" class="radio-label">Designer fashion</label>
                            </div>

                            <div class="radio">
                                <input id="radio-3" name="category" type="radio" data-price="" value="Collectibles">
                                <label for="radio-3" class="radio-label">Collectibles</label>
                            </div>

                            <div class="radio">
                                <input id="radio-4" name="category" type="radio" data-price="" value="Cell phones">
                                <label for="radio-4" class="radio-label">Cell phones</label>
                            </div>

                            <div class="radio">
                                <input id="radio-5" name="category" type="radio" data-price="" value="Toys & games">
                                <label for="radio-5" class="radio-label">Toys & games</label>
                            </div>

                            <div class="radio">
                                <input id="radio-6" name="category" type="radio" data-price="" value="Luxury watches">
                                <label for="radio-6" class="radio-label">Luxury watches</label>
                            </div>

                            <div class="radio">
                                <input id="radio-7" name="category" type="radio" data-price="" value="Other">
                                <label for="radio-7" class="radio-label">Other</label>
                            </div>

                        </div>
                    </fieldset>

                    <fieldset>
                        <div class="inform_cont">
                            <h3>2. Enter Your Product Details</h3>
                        </div>
                        <div class="label_cont">

                            <label>
                                <span>Manufacturer / Brand</span>
                                <input type="text" required placeholder='Manufacturer / Brand' name="brand">
                            </label>

                            <label>
                                <span>Product Name and/or Model Number</span>
                                <input type="text" required placeholder='Product Name and/or Model Number' name="model">
                            </label>


                        </div>
                    </fieldset>

                    <fieldset>
                        <div class="inform_cont">
                            <h3>3. Describe Your Item's Condition</h3>
                        </div>
                        <p>Item Condition</p>
                        <div class="radio_cont st3_radio">

                            <div class="radio">
                                <input id="radio-8" name="condition" type="radio" data-price="" value="Include defective" checked>
                                <label for="radio-8" class="radio-label">Include defective</label>
                            </div>

                            <div class="radio">
                                <input id="radio-9" name="condition" type="radio" data-price="" value="Good">
                                <label  for="radio-9" class="radio-label">Good</label>
                            </div>

                            <div class="radio">
                                <input id="radio-10" name="condition" type="radio" data-price="" value="Like new and brand new sealed">
                                <label  for="radio-10" class="radio-label">Like new and brand new sealed</label>
                            </div>

                        </div>
                    </fieldset>

                    <fieldset>
                        <div class="inform_cont">
                            <h3>4. Additional details about item box  where you can write a message and quantity.</h3>
                            <p>Please add any other details or comments that you feel are important to your product so we can provide you with quick and accurate pricing as soon as possible.</p>
                        </div>
                        <div class="st4">

                            <label>
                                <input type="file" name="photo">

                                <div class="visible_picture">
                                    <img src="img/file.svg" alt="">
                                    <strong>Upload a Picture</strong>
                                    <span><em>File is not chose</em> maximum size 100KB</span>
                                </div>
                            </label>



                            <label>
                                <span>Quantity</span>
                                <div class="select">
                                    <i class="arrow down"></i>
                                    <select name="quantity" id="slct" required>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                    </select>
                                </div>
                            </label>

                            <label>
                                <textarea placeholder='Type Comment …' name="comment"></textarea>
                                <div class="counter"><span id="counter">0</span> / <span id='max_counter'></span></div>
                            </label>


                        </div>
                    </fieldset>

                    <fieldset>
                        <div class="inform_cont">
                            <h3>5. Your Email</h3>
                        </div>
                        <div class="st5">

                            <label>
                                <span>Email</span>
                                <input type="email" placeholder='Type your E-mail' required name="email">
                            </label>

                        </div>
                    </fieldset>

                    <div class="terms">
                        <h2>Terms of Services</h2>
                        <p>I have read and agree to the <span><a href="/terms"> terms and conditions.</a> </span></p>
                        <div class="radio_cont terms_cont">
                            <div class="radio">
                                <input id="radio-11" name="radio1" type="radio" data-price="" value="yes" required>
                                <label for="radio-11" class="radio-label"></label>
                            </div>
                            <p>By checking this box, I understand the offer amount will be sent to me after the item has been shipped and inspected and I confirm the item(s) are not reported lost or stolen.</p>
                        </div>
                    </div>
                    <input type="submit" value="Request your custom quote">




                </form>
            </div>
        </div>
    </section>


    <a  data-fancybox data-src="#thank" id="link"></a>
    <div class="hidden">
        <div class="thank" id='thank'>
            <h3>Thank you for your application</h3>
        </div>
    </div>



@endsection



@section('scripts')
<script>

    $(document).ready(function(){
/*
$('#link').click();

    })

*/


</script>

@endsection