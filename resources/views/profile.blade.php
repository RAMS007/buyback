@extends('layouts.app')

@section('mainContent')



    <div class="my_orders">
        <div class="container">
            <span class='tab2'><i class="fa fa-user" aria-hidden="true"></i>my profile</span>
            <span class='tab2'><i class="fa fa-credit-card" aria-hidden="true"></i>My Orders</span>

        </div>
    </div>

    <section class="status_order">
        <div class="container">
            <div class="tab_item2 profile_t" @if (  ( $page='orders'))   style="display:none" @endif>
                <div class="profile_top">
                    <div class="profile_men">
                        <div class="proflie_men_img">
                            <img src="http://placekitten.com/g/70/70" alt="">
                        </div>
                        <div class="profile_men_name">{{$user->FirstName}} {{$user->LastName}}</div>
                        <a href='/profile/edit' class="profile_edit"><i class="fa fa-pencil" aria-hidden="true"></i>Edit Profile</a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="paid">
                        <div class="paid_text">Paid to Date</div>
                        <div class="paid_price">$??? ???</div>
                    </div>
                </div>
                <div class="profile_middle">
                    <div class="profile_colon">
                        <h2>Shipping Address<a href='/profile/edit' class="profile_edit"><i class="fa fa-pencil"
                                                                                aria-hidden="true"></i>Edit</a></h2>

                        @isset($shipping)


                        <p>{{$shipping->shipping_Addres}}</p>
                        <p>{{$shipping->shipping_city}}</p>
                        <p>{{$shipping->shipping_country}}</p>
                        <p>{{$shipping->shipping_province}}</p>
                        <p>{{$shipping->shipping_zip}}</p>
                        <p>{{$shipping->shipping_phone}}</p>

                        @endisset
                    </div>
                    <div class="profile_colon">
                        <h2>Payment Method<a href='/profile/edit' class="profile_edit"><i class="fa fa-pencil"
                                                                              aria-hidden="true"></i>Edit</a></h2>

                        @isset($payment)
                        @if($payment->paymentType=='paypal')
                            <img src='/img/paypal.svg' alt="">
                        @else
                            <img src='/img/check.svg' alt="">
                        @endif
                        @endisset

                    </div>
                    <div class="profile_colon">
                        <h2>Payment Address<a href='/profile/edit' class="profile_edit"><i class="fa fa-pencil"
                                                                               aria-hidden="true"></i>Edit</a></h2>
                        @isset($payment)
                        @if($payment->paymentType=='paypal')
                            <p>{{$payment->paypalId}}</p>
                        @else
                            <p>{{$payment->AddressName}}</p>
                        @endif
                        @endisset

                    </div>
                </div>
                <div class="profile_bottom">
                    <div class="profile_bottom_text">
                        <p>If you are having trouble finding an item you want to sell, you can always submit a <a
                                    href="#">Custom Quote !</a></p>
                        <p>You can sell designer handvags, high-end watches, sunglasses, power tools, laptops,
                            collectible toys and more. </p>
                        <p>Submit a Custom Quote today-what do have to lose? Except the cash you could be getting paid,
                            of corse!</p>
                    </div>
                </div>
            </div>
            <div class="tab_item2" @if (   ( $page!='orders'))   style="display:none" @endif >
                <div class="search_results">
                    <div class="search_name">Search results <span id="DatatableResults"></span></div>
                    <div class="cat_search">
                        <form>
                            <input type="search">
                            <input type="submit">
                        </form>
                    </div>
                </div>
                <div class="order_table">
                    <table id='myTable' class="rwd-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Picture</th>
                            <th>Name Item</th>
                            <th>Order Date</th>
                            <th>Order Status</th>
                            <th>Shipping Info</th>
                            <th>Payment</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($orders as $order)
                        <tr>
                            <td data-th="#">{{$order->id}}</td>
                            <td data-th="Picture">
                                <div class="t_img"><img src="{{$order->ModelImage}}" alt=""></div>
                            </td>
                            <td data-th="Name Item">{{$order->ModelName}}</td>
                            <td data-th="Order Date"><span>{{$order->date}}</span> {{$order->time}}</td>
                            @if($order->status=='new')
                            <td data-th="Order Status">
                                <div class="pending"><img src="/img/pending.svg" alt="">New</div>
                            </td>
                            @endif
                            @if($order->status=='pending')
                                <td data-th="Order Status">
                                    <div class="pending"><img src="/img/pending.svg" alt="">Pending</div>
                                </td>
                            @endif
                            @if($order->status=='deleted')
                                <td data-th="Order Status">
                                    <div class="pending"><img src="/img/pending.svg" alt="">Deleted</div>
                                </td>
                            @endif
                            @if($order->status=='completed')
                                <td data-th="Order Status">
                                    <div class="pending"><img src="/img/pending.svg" alt="">Completed</div>
                                </td>
                            @endif
                            <td data-th="Shipping Info"><img src="/img/adr.svg" alt="">
                               {{$order->address}}
                            </td>
                            <td data-th="Payment">
                                @if($order->PaymentType=='Check')
                                <img src="/img/check.svg" alt="">
                                    @else
                                    <img src="/img/paypal.svg" alt="">
                                @endif
                            </td>
                            <td data-th="Action">
                                <a href="#" data-fancybox data-src="#order"><img src="/img/visib.svg" alt=""></a>
                                <a href="#"><img src="/img/red.svg" alt=""></a>
                                <a href="#" data-fancybox data-src="#test_confirm"><img src="/img/del.svg" alt=""></a>
                            </td>
                        </tr>
                 @endforeach
</tbody>
                    </table>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </section>

@endsection