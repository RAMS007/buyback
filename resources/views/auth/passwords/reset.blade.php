@extends('layouts.app')

@section('mainContent')

    <section class="sign_cont">
        <div class="container">
            <fieldset>
                <div class="sign_side_wrapp">
                    <div class="sign_form">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="POST" action="/password/reset"  >
                            {{ csrf_field() }}
                            <input type="hidden" name="token" value="{{ $token }}">


                            <p class="reset_pass">Enter your new password</p>

                            <label class="{{ $errors->has('email') ? ' error' : '' }}" >
                                <span>Email:</span>
                                <input type="email"  name="email" value="{{ $email or old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span >
                                      {{ $errors->first('email') }}
                                    </span>
                                @endif

                            </label>


                            <label class="{{ $errors->has('password') ? ' error' : '' }}" >
                                <span>Type your new Password:</span>
                                <input type="password" required placeholder=''  name="password" >
                                @if ($errors->has('password'))
                                    <span >
                                     {{ $errors->first('password') }}
                                    </span>
                                @endif
                            </label>

                            <label class='{{ $errors->has('password_confirmation') ? ' error' : '' }}'>
                                <span>Confirmed Password:</span>
                                <input type="password" required placeholder=''  name="password_confirmation">
                                @if ($errors->has('password_confirmation'))
                                    <span>
                                     {{ $errors->first('password_confirmation') }}
                                    </span>
                                @endif

                            </label>
                            <input type="submit" value="Change Password">




                            <p>Don't have a BuyBackWorld Account? <a href=""><span>Sign Up Now</span></a></p>

                        </form>
                    </div>
                    <div class="sign_r_side">
                        <img src="/img/sign_up.png" alt="">
                    </div>
                </div>
            </fieldset>

        </div>
    </section>



@endsection

<!--

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" method="POST" action="">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Reset Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


-->
