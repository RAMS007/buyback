@extends('layouts.app')

@section('mainContent')

    <section class="sign_cont">
        <div class="container">
            <fieldset>
                <div class="sign_side_wrapp">
                    <div class="sign_form">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="POST" action="/password/email"  >
                            {{ csrf_field() }}
                            <p class="reset_pass">Type your E-mail and we will send you a new password</p>
                            <label class="{{ $errors->has('email') ? ' error' : '' }}">
                                <span>E-mail:</span>
                                <input type="email" required placeholder='sample@gmail.com' name="email">
                                <span>{{ $errors->first('email') }}</span>
                            </label>
                            <input type="submit" value="Reset Password">
                            <p>Don't have a BuyBackWorld Account? <a href=""><span>Sign Up Now</span></a></p>

                        </form>
                    </div>
                    <div class="sign_r_side">
                        <img src="/img/sign_up.png" alt="">
                    </div>
                </div>
            </fieldset>

        </div>
    </section>



@endsection
<!--

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" method="POST" action="">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

-->
