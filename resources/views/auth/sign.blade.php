@extends('layouts.app')

@section('mainContent')


    <section class="sign_title">
        <div class="container">
            <div class="sign_wrapp">
                <div class="sign_name">
                    Log In / Create Account
                </div>
                <div class="sign_history">
                    <ul id="progressbar">
                        <li class="active">1 Login</li>
                        <li>2 Payment Preference</li>
                        <li>3 Shiping Info</li>
                        <li>4 Confirmation</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="sign_cont">
        <div class="container">
            <fieldset id="authFieldset">
                <div class="sign_side_wrapp">
                    <div class="sign_l_side">
                        <div class="tabs">
                            <span class="tab2  @if ($page=='login')   active  @endif  ">Login</span>
                            <span class="tab2  @if ($page=='register')   active  @endif  ">New User</span>
                        </div>
                        <div class="tab_content">
                            <div class="tab_item2" @if ($page=='register')   style="display:none" @endif >
                                <div class="sign_form">
                                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                        {{ csrf_field() }}


                                        <span class="help-block">
                                                    <strong id="emailError"></strong>
                                                </span>


                                        <label>
                                            <span>E-mail:</span>
                                            <input id="email" type="email" class="form-control" name="email"
                                                   value="{{ old('email') }}" required autofocus
                                                   placeholder='sample@gmail.com'>
                                        </label>


                                        @if (count($errors) > 0)
                                            <label class='error'>
                                                <span>Confirmed Password:</span>
                                                <input type="password" required placeholder=''>
                                                <span>These credentials do not match our records.</span>
                                            </label>
                                        @else

                                            <label>


                                                <span>Password:</span>
                                                <input id="password" type="password" class="form-control"
                                                       name="password"
                                                       required placeholder='Type your password'>
                                            </label>
                                        @endif

                                        <p>Forget password ? <a href="/password/reset" class="reset">Reset</a></p>
                                        <input type="submit" value="Login">
                                        <p>Don't have a BuyBackWorld Account? <a href=""><span>Sign Up Now</span></a>
                                        </p>

                                    </form>
                                </div>
                            </div>
                            <div class="tab_item2" @if ($page=='login')   style="display:none" @endif >
                                <div class="sign_form">
                                    <form class="form-horizontal" method="POST" action="{{ route('register') }}"
                                          id="registerForm">
                                        {{ csrf_field() }}


                                            <label class="regemail" >
                                                <span>E-mail:</span>
                                                <input id="email" type="email" class="form-control" name="email"
                                                       value="{{ old('email') }}" required
                                                       placeholder='sample@gmail.com'>
                                                <span class="regemail" ></span>
                                            </label>



                                            <label>
                                                <span>Confrim E-mail:</span>
                                                <input id="email" type="email" class="form-control"
                                                       name="email_confirmation" value="{{ old('email') }}" required
                                                       placeholder='sample@gmail.com'>

                                            </label>



                                            <label class="regpassword">
                                                <span>Password:</span>
                                                <input id="password" type="password" class="form-control"
                                                       name="password"
                                                       placeholder='Type your password'>
                                                <span class="regpassword"></span>
                                            </label>




                                            <label class="regFirstname">
                                                <span>First Name:</span>
                                                <input type="text" required name="FirstName" placeholder='First Name'>
                                                <span class="regFirstname"></span>
                                            </label>



                                            <label class="regLastname">
                                                <span>Last Name:</span>
                                                <input type="text" name="LastName" required placeholder='Last Name'>
                                                <span class="regLastname"></span>
                                            </label>




                                        <label class="tasks-list-item">
                                            <input type="checkbox" name="policy" value="1" class="tasks-list-cb">
                                            <span class="tasks-list-mark"></span>
                                            <span class="tasks-list-desc">I’m happy to receive occasional emails and promotions from Cashyourtronics in accordance with the <a
                                                        href="#">Privacy & Policy</a></span>

                                        </label>
                                        <span class="clearfix"></span>
                                        <input type="hidden" id="userId">
                                        <input type="submit" value="Create Account">
                                    </form>
                                </div>

                            </div>
                        </div>
                        <input type="button" name="next" class="next" value="next"/>
                    </div>
                    <div class="sign_r_side">
                        <img src="img/sign_up.png" alt="">
                    </div>
                </div>
            </fieldset>

            <fieldset id="PaymentFieldset">
                <div class="step_cont">

                    <div class="step4">

                        <div class="radio tab">
                            <input id="paymentType" name="paymentType" type="radio" checked data-price="" value="Check">
                            <label for="paymentType" class="radio-step4">
                                <div class="step4_img">
                                    <img src="img/check.svg" alt="">
                                </div>
                                <p>Delivered by USPS First Class mail within 10 days</p>
                            </label>
                        </div>
                        <div class="radio tab">
                            <input id="paymentType2" name="paymentType" type="radio" data-price="" value="paypal">
                            <label for="paymentType2" class="radio-step4">
                                <div class="step4_img">
                                    <img src="img/paypal.svg" alt="">
                                </div>
                                <p>Sent directly to your PayPal account</p>
                            </label>
                        </div>


                    </div>
                    <div class="tab_content">
                        <form id="check">
                            {{ csrf_field() }}
                            <div class="tab_item">
                                <div class="step4_cont">

                                    <input type="hidden" name="paymentType" value="check">
                                    <h3>Check</h3>
                                    <p>We'll send you a check once we've inspected your items</p>
                                    <label>
                                          <span class="help-block">
                                                    <strong id="AddressName_error"></strong>
                                                </span>
                                        <span>Address Name (e.g. Home, Work, Beach House)</span>
                                        <input type="text" name="AddressName" placeholder='Address Name'>
                                    </label>
                                    <div class="bouble">
                                        <label>
                                            <!--         <span class="help-block">
                                                            <strong id="FirstName_error"></strong>
                                                        </span> -->
                                            <span>Firts Name</span>
                                            <input type="text" name="FirstName" placeholder='Firts Name'>
                                        </label>
                                        <label>
                                            <span>Last Name</span>
                                            <input type="text" name="LastName" placeholder='Last Name'>
                                        </label>
                                    </div>
                                    <div class="bouble">
                                        <label>
                                            <span>Address Line </span>
                                            <input type="text" name="addres1" placeholder='Address Line'>
                                        </label>
                                        <label>
                                            <span>Adress Line 2</span>
                                            <input type="text" name="addres2" placeholder='Adress Line 2'>
                                        </label>
                                    </div>
                                    <div class="bouble">
                                        <label>
                                            <span>City</span>
                                            <input type="text" name="city" placeholder='City'>
                                        </label>
                                        <div class="omg">
                                            <div class="bouble">
                                                <label>
                                                    <span>Select State</span>
                                                    <div class="select_phon">
                                                        <i class="arrow down"></i>
                                                        <select name="state" id="slct">
                                                            <option value="N/A">Select</option>
                                                            <option value="AL">Alabama</option>
                                                            <option value="AK">Alaska</option>
                                                            <option value="AZ">Arizona</option>
                                                            <option value="AR">Arkansas</option>
                                                            <option value="CA">California</option>
                                                            <option value="CO">Colorado</option>
                                                            <option value="CT">Connecticut</option>
                                                            <option value="DE">Delaware</option>
                                                            <option value="DC">District Of Columbia</option>
                                                            <option value="FL">Florida</option>
                                                            <option value="GA">Georgia</option>
                                                            <option value="HI">Hawaii</option>
                                                            <option value="ID">Idaho</option>
                                                            <option value="IL">Illinois</option>
                                                            <option value="IN">Indiana</option>
                                                            <option value="IA">Iowa</option>
                                                            <option value="KS">Kansas</option>
                                                            <option value="KY">Kentucky</option>
                                                            <option value="LA">Louisiana</option>
                                                            <option value="ME">Maine</option>
                                                            <option value="MD">Maryland</option>
                                                            <option value="MA">Massachusetts</option>
                                                            <option value="MI">Michigan</option>
                                                            <option value="MN">Minnesota</option>
                                                            <option value="MS">Mississippi</option>
                                                            <option value="MO">Missouri</option>
                                                            <option value="MT">Montana</option>
                                                            <option value="NE">Nebraska</option>
                                                            <option value="NV">Nevada</option>
                                                            <option value="NH">New Hampshire</option>
                                                            <option value="NJ">New Jersey</option>
                                                            <option value="NM">New Mexico</option>
                                                            <option value="NY">New York</option>
                                                            <option value="NC">North Carolina</option>
                                                            <option value="ND">North Dakota</option>
                                                            <option value="OH">Ohio</option>
                                                            <option value="OK">Oklahoma</option>
                                                            <option value="OR">Oregon</option>
                                                            <option value="PA">Pennsylvania</option>
                                                            <option value="RI">Rhode Island</option>
                                                            <option value="SC">South Carolina</option>
                                                            <option value="SD">South Dakota</option>
                                                            <option value="TN">Tennessee</option>
                                                            <option value="TX">Texas</option>
                                                            <option value="UT">Utah</option>
                                                            <option value="VT">Vermont</option>
                                                            <option value="VA">Virginia</option>
                                                            <option value="WA">Washington</option>
                                                            <option value="WV">West Virginia</option>
                                                            <option value="WI">Wisconsin</option>
                                                            <option value="WY">Wyoming</option>
                                                        </select>
                                                    </div>
                                                </label>
                                                <label>
                                                    <span>Zip code</span>
                                                    <input type="text" name="zip" placeholder='Select State'>
                                                </label>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="check_cont">
                                    <label class="tasks-list-item">
                                        <input type="checkbox" name="task_1" value="1" class="tasks-list-cb" checked>
                                        <span class="tasks-list-mark"></span>
                                        <span class="tasks-list-desc">"I understand that the final offer amint will be sent after item has been inspected and that the devise has not been reported lost or stolen"</span>
                                    </label>
                                    <input type="submit" name="next" class="subb" value="Continue to Shiping"/>
                                    <div class="clearfix"></div>
                                </div>


                            </div>
                        </form>
                        <form id="paypal">
                            {{ csrf_field() }}
                            <div class="tab_item">
                                <div class="step4_cont">

                                    <input type="hidden" name="paymentType" value="paypal">
                                    <h3>PayPal Information</h3>
                                    <p>We'll instantly send the funds to your PayPal account once we've inspected your
                                        items</p>
                                    <div class="bouble">
                                        <label>
                                            <span>Enter PayPal ID:</span>
                                            <input type="text" name="paypalId" placeholder='Enter PayPal ID'>
                                        </label>
                                        <label>
                                            <span>Confirm PayPal ID:</span>
                                            <input type="text" name="paypalId_confirmation"
                                                   placeholder='Confirm PayPal ID'>
                                        </label>
                                    </div>

                                </div>

                                <div class="check_cont">
                                    <label class="tasks-list-item">
                                        <input type="checkbox" name="task_1" value="1" class="tasks-list-cb" checked>
                                        <span class="tasks-list-mark"></span>
                                        <span class="tasks-list-desc">"I understand that the final offer amint will be sent after item has been inspected and that the devise has not been reported lost or stolen"</span>
                                    </label>
                                    <input type="button" name="next" class="subb" value="Continue to Shiping"/>
                                    <div class="clearfix"></div>
                                </div>

                            </div>
                        </form>
                    </div>

                </div>
            </fieldset>
            <fieldset id="shippingFieldset">
                <form id="shipping">
                    {{ csrf_field() }}
                    <div class="step4_cont step5_cont">
                        <!-- <h3>Customer information </h3> -->
                        <label>
                            <span>E-mail</span>
                            <input type="email" name="shipping_email" placeholder='E-mail'>
                        </label>

                        <div class="bouble">
                            <label>
                                <span>First Name</span>
                                <input type="text" name="shipping_FirstName" placeholder='First Name '>
                            </label>
                            <label>
                                <span>Last Name </span>
                                <input type="text" name="shipping_LastName" placeholder='Last Name '>
                            </label>
                        </div>
                        <label>
                            <span>Address</span>
                            <input type="text" name="shipping_Addres" placeholder='Address'>
                        </label>
                        <label>
                            <span>City</span>
                            <input type="text" name="shipping_city" placeholder='City'>
                        </label>

                        <div class="bouble">
                            <label>
                                <span>Country</span>
                                <div class="select_phon">
                                    <i class="arrow down"></i>
                                    <select name="shipping_country">
                                        <option value="AF">Afghanistan</option>
                                        <option value="AX">Åland Islands</option>
                                        <option value="AL">Albania</option>
                                        <option value="DZ">Algeria</option>
                                        <option value="AS">American Samoa</option>
                                        <option value="AD">Andorra</option>
                                        <option value="AO">Angola</option>
                                        <option value="AI">Anguilla</option>
                                        <option value="AQ">Antarctica</option>
                                        <option value="AG">Antigua and Barbuda</option>
                                        <option value="AR">Argentina</option>
                                        <option value="AM">Armenia</option>
                                        <option value="AW">Aruba</option>
                                        <option value="AU">Australia</option>
                                        <option value="AT">Austria</option>
                                        <option value="AZ">Azerbaijan</option>
                                        <option value="BS">Bahamas</option>
                                        <option value="BH">Bahrain</option>
                                        <option value="BD">Bangladesh</option>
                                        <option value="BB">Barbados</option>
                                        <option value="BY">Belarus</option>
                                        <option value="BE">Belgium</option>
                                        <option value="BZ">Belize</option>
                                        <option value="BJ">Benin</option>
                                        <option value="BM">Bermuda</option>
                                        <option value="BT">Bhutan</option>
                                        <option value="BO">Bolivia, Plurinational State of</option>
                                        <option value="BQ">Bonaire, Sint Eustatius and Saba</option>
                                        <option value="BA">Bosnia and Herzegovina</option>
                                        <option value="BW">Botswana</option>
                                        <option value="BV">Bouvet Island</option>
                                        <option value="BR">Brazil</option>
                                        <option value="IO">British Indian Ocean Territory</option>
                                        <option value="BN">Brunei Darussalam</option>
                                        <option value="BG">Bulgaria</option>
                                        <option value="BF">Burkina Faso</option>
                                        <option value="BI">Burundi</option>
                                        <option value="KH">Cambodia</option>
                                        <option value="CM">Cameroon</option>
                                        <option value="CA">Canada</option>
                                        <option value="CV">Cape Verde</option>
                                        <option value="KY">Cayman Islands</option>
                                        <option value="CF">Central African Republic</option>
                                        <option value="TD">Chad</option>
                                        <option value="CL">Chile</option>
                                        <option value="CN">China</option>
                                        <option value="CX">Christmas Island</option>
                                        <option value="CC">Cocos (Keeling) Islands</option>
                                        <option value="CO">Colombia</option>
                                        <option value="KM">Comoros</option>
                                        <option value="CG">Congo</option>
                                        <option value="CD">Congo, the Democratic Republic of the</option>
                                        <option value="CK">Cook Islands</option>
                                        <option value="CR">Costa Rica</option>
                                        <option value="CI">Côte d'Ivoire</option>
                                        <option value="HR">Croatia</option>
                                        <option value="CU">Cuba</option>
                                        <option value="CW">Curaçao</option>
                                        <option value="CY">Cyprus</option>
                                        <option value="CZ">Czech Republic</option>
                                        <option value="DK">Denmark</option>
                                        <option value="DJ">Djibouti</option>
                                        <option value="DM">Dominica</option>
                                        <option value="DO">Dominican Republic</option>
                                        <option value="EC">Ecuador</option>
                                        <option value="EG">Egypt</option>
                                        <option value="SV">El Salvador</option>
                                        <option value="GQ">Equatorial Guinea</option>
                                        <option value="ER">Eritrea</option>
                                        <option value="EE">Estonia</option>
                                        <option value="ET">Ethiopia</option>
                                        <option value="FK">Falkland Islands (Malvinas)</option>
                                        <option value="FO">Faroe Islands</option>
                                        <option value="FJ">Fiji</option>
                                        <option value="FI">Finland</option>
                                        <option value="FR">France</option>
                                        <option value="GF">French Guiana</option>
                                        <option value="PF">French Polynesia</option>
                                        <option value="TF">French Southern Territories</option>
                                        <option value="GA">Gabon</option>
                                        <option value="GM">Gambia</option>
                                        <option value="GE">Georgia</option>
                                        <option value="DE">Germany</option>
                                        <option value="GH">Ghana</option>
                                        <option value="GI">Gibraltar</option>
                                        <option value="GR">Greece</option>
                                        <option value="GL">Greenland</option>
                                        <option value="GD">Grenada</option>
                                        <option value="GP">Guadeloupe</option>
                                        <option value="GU">Guam</option>
                                        <option value="GT">Guatemala</option>
                                        <option value="GG">Guernsey</option>
                                        <option value="GN">Guinea</option>
                                        <option value="GW">Guinea-Bissau</option>
                                        <option value="GY">Guyana</option>
                                        <option value="HT">Haiti</option>
                                        <option value="HM">Heard Island and McDonald Islands</option>
                                        <option value="VA">Holy See (Vatican City State)</option>
                                        <option value="HN">Honduras</option>
                                        <option value="HK">Hong Kong</option>
                                        <option value="HU">Hungary</option>
                                        <option value="IS">Iceland</option>
                                        <option value="IN">India</option>
                                        <option value="ID">Indonesia</option>
                                        <option value="IR">Iran, Islamic Republic of</option>
                                        <option value="IQ">Iraq</option>
                                        <option value="IE">Ireland</option>
                                        <option value="IM">Isle of Man</option>
                                        <option value="IL">Israel</option>
                                        <option value="IT">Italy</option>
                                        <option value="JM">Jamaica</option>
                                        <option value="JP">Japan</option>
                                        <option value="JE">Jersey</option>
                                        <option value="JO">Jordan</option>
                                        <option value="KZ">Kazakhstan</option>
                                        <option value="KE">Kenya</option>
                                        <option value="KI">Kiribati</option>
                                        <option value="KP">Korea, Democratic People's Republic of</option>
                                        <option value="KR">Korea, Republic of</option>
                                        <option value="KW">Kuwait</option>
                                        <option value="KG">Kyrgyzstan</option>
                                        <option value="LA">Lao People's Democratic Republic</option>
                                        <option value="LV">Latvia</option>
                                        <option value="LB">Lebanon</option>
                                        <option value="LS">Lesotho</option>
                                        <option value="LR">Liberia</option>
                                        <option value="LY">Libya</option>
                                        <option value="LI">Liechtenstein</option>
                                        <option value="LT">Lithuania</option>
                                        <option value="LU">Luxembourg</option>
                                        <option value="MO">Macao</option>
                                        <option value="MK">Macedonia, the former Yugoslav Republic of</option>
                                        <option value="MG">Madagascar</option>
                                        <option value="MW">Malawi</option>
                                        <option value="MY">Malaysia</option>
                                        <option value="MV">Maldives</option>
                                        <option value="ML">Mali</option>
                                        <option value="MT">Malta</option>
                                        <option value="MH">Marshall Islands</option>
                                        <option value="MQ">Martinique</option>
                                        <option value="MR">Mauritania</option>
                                        <option value="MU">Mauritius</option>
                                        <option value="YT">Mayotte</option>
                                        <option value="MX">Mexico</option>
                                        <option value="FM">Micronesia, Federated States of</option>
                                        <option value="MD">Moldova, Republic of</option>
                                        <option value="MC">Monaco</option>
                                        <option value="MN">Mongolia</option>
                                        <option value="ME">Montenegro</option>
                                        <option value="MS">Montserrat</option>
                                        <option value="MA">Morocco</option>
                                        <option value="MZ">Mozambique</option>
                                        <option value="MM">Myanmar</option>
                                        <option value="NA">Namibia</option>
                                        <option value="NR">Nauru</option>
                                        <option value="NP">Nepal</option>
                                        <option value="NL">Netherlands</option>
                                        <option value="NC">New Caledonia</option>
                                        <option value="NZ">New Zealand</option>
                                        <option value="NI">Nicaragua</option>
                                        <option value="NE">Niger</option>
                                        <option value="NG">Nigeria</option>
                                        <option value="NU">Niue</option>
                                        <option value="NF">Norfolk Island</option>
                                        <option value="MP">Northern Mariana Islands</option>
                                        <option value="NO">Norway</option>
                                        <option value="OM">Oman</option>
                                        <option value="PK">Pakistan</option>
                                        <option value="PW">Palau</option>
                                        <option value="PS">Palestinian Territory, Occupied</option>
                                        <option value="PA">Panama</option>
                                        <option value="PG">Papua New Guinea</option>
                                        <option value="PY">Paraguay</option>
                                        <option value="PE">Peru</option>
                                        <option value="PH">Philippines</option>
                                        <option value="PN">Pitcairn</option>
                                        <option value="PL">Poland</option>
                                        <option value="PT">Portugal</option>
                                        <option value="PR">Puerto Rico</option>
                                        <option value="QA">Qatar</option>
                                        <option value="RE">Réunion</option>
                                        <option value="RO">Romania</option>
                                        <option value="RU">Russian Federation</option>
                                        <option value="RW">Rwanda</option>
                                        <option value="BL">Saint Barthélemy</option>
                                        <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                                        <option value="KN">Saint Kitts and Nevis</option>
                                        <option value="LC">Saint Lucia</option>
                                        <option value="MF">Saint Martin (French part)</option>
                                        <option value="PM">Saint Pierre and Miquelon</option>
                                        <option value="VC">Saint Vincent and the Grenadines</option>
                                        <option value="WS">Samoa</option>
                                        <option value="SM">San Marino</option>
                                        <option value="ST">Sao Tome and Principe</option>
                                        <option value="SA">Saudi Arabia</option>
                                        <option value="SN">Senegal</option>
                                        <option value="RS">Serbia</option>
                                        <option value="SC">Seychelles</option>
                                        <option value="SL">Sierra Leone</option>
                                        <option value="SG">Singapore</option>
                                        <option value="SX">Sint Maarten (Dutch part)</option>
                                        <option value="SK">Slovakia</option>
                                        <option value="SI">Slovenia</option>
                                        <option value="SB">Solomon Islands</option>
                                        <option value="SO">Somalia</option>
                                        <option value="ZA">South Africa</option>
                                        <option value="GS">South Georgia and the South Sandwich Islands</option>
                                        <option value="SS">South Sudan</option>
                                        <option value="ES">Spain</option>
                                        <option value="LK">Sri Lanka</option>
                                        <option value="SD">Sudan</option>
                                        <option value="SR">Suriname</option>
                                        <option value="SJ">Svalbard and Jan Mayen</option>
                                        <option value="SZ">Swaziland</option>
                                        <option value="SE">Sweden</option>
                                        <option value="CH">Switzerland</option>
                                        <option value="SY">Syrian Arab Republic</option>
                                        <option value="TW">Taiwan, Province of China</option>
                                        <option value="TJ">Tajikistan</option>
                                        <option value="TZ">Tanzania, United Republic of</option>
                                        <option value="TH">Thailand</option>
                                        <option value="TL">Timor-Leste</option>
                                        <option value="TG">Togo</option>
                                        <option value="TK">Tokelau</option>
                                        <option value="TO">Tonga</option>
                                        <option value="TT">Trinidad and Tobago</option>
                                        <option value="TN">Tunisia</option>
                                        <option value="TR">Turkey</option>
                                        <option value="TM">Turkmenistan</option>
                                        <option value="TC">Turks and Caicos Islands</option>
                                        <option value="TV">Tuvalu</option>
                                        <option value="UG">Uganda</option>
                                        <option value="UA">Ukraine</option>
                                        <option value="AE">United Arab Emirates</option>
                                        <option value="GB">United Kingdom</option>
                                        <option value="US">United States</option>
                                        <option value="UM">United States Minor Outlying Islands</option>
                                        <option value="UY">Uruguay</option>
                                        <option value="UZ">Uzbekistan</option>
                                        <option value="VU">Vanuatu</option>
                                        <option value="VE">Venezuela, Bolivarian Republic of</option>
                                        <option value="VN">Viet Nam</option>
                                        <option value="VG">Virgin Islands, British</option>
                                        <option value="VI">Virgin Islands, U.S.</option>
                                        <option value="WF">Wallis and Futuna</option>
                                        <option value="EH">Western Sahara</option>
                                        <option value="YE">Yemen</option>
                                        <option value="ZM">Zambia</option>
                                        <option value="ZW">Zimbabwe</option>
                                    </select>
                                </div>
                            </label>
                            <label>
                                <span>State/Province</span>
                                <div class="select_phon">
                                    <i class="arrow down"></i>
                                    <select name="shipping_province">
                                        <option value="N/A">Select</option>
                                        <option value="AL">Alabama</option>
                                        <option value="AK">Alaska</option>
                                        <option value="AZ">Arizona</option>
                                        <option value="AR">Arkansas</option>
                                        <option value="CA">California</option>
                                        <option value="CO">Colorado</option>
                                        <option value="CT">Connecticut</option>
                                        <option value="DE">Delaware</option>
                                        <option value="DC">District Of Columbia</option>
                                        <option value="FL">Florida</option>
                                        <option value="GA">Georgia</option>
                                        <option value="HI">Hawaii</option>
                                        <option value="ID">Idaho</option>
                                        <option value="IL">Illinois</option>
                                        <option value="IN">Indiana</option>
                                        <option value="IA">Iowa</option>
                                        <option value="KS">Kansas</option>
                                        <option value="KY">Kentucky</option>
                                        <option value="LA">Louisiana</option>
                                        <option value="ME">Maine</option>
                                        <option value="MD">Maryland</option>
                                        <option value="MA">Massachusetts</option>
                                        <option value="MI">Michigan</option>
                                        <option value="MN">Minnesota</option>
                                        <option value="MS">Mississippi</option>
                                        <option value="MO">Missouri</option>
                                        <option value="MT">Montana</option>
                                        <option value="NE">Nebraska</option>
                                        <option value="NV">Nevada</option>
                                        <option value="NH">New Hampshire</option>
                                        <option value="NJ">New Jersey</option>
                                        <option value="NM">New Mexico</option>
                                        <option value="NY">New York</option>
                                        <option value="NC">North Carolina</option>
                                        <option value="ND">North Dakota</option>
                                        <option value="OH">Ohio</option>
                                        <option value="OK">Oklahoma</option>
                                        <option value="OR">Oregon</option>
                                        <option value="PA">Pennsylvania</option>
                                        <option value="RI">Rhode Island</option>
                                        <option value="SC">South Carolina</option>
                                        <option value="SD">South Dakota</option>
                                        <option value="TN">Tennessee</option>
                                        <option value="TX">Texas</option>
                                        <option value="UT">Utah</option>
                                        <option value="VT">Vermont</option>
                                        <option value="VA">Virginia</option>
                                        <option value="WA">Washington</option>
                                        <option value="WV">West Virginia</option>
                                        <option value="WI">Wisconsin</option>
                                        <option value="WY">Wyoming</option>
                                    </select>
                                </div>
                            </label>

                            <label>
                                <span>Zip/Postal code  </span>
                                <input type="text" name="shipping_zip" placeholder='Zip/Postal code'>
                            </label>

                            <label>
                                <span>Phone Number </span>
                                <input type="text" name="shipping_phone" placeholder='Phone Number'>
                            </label>
                        </div>
                        <input type="submit" name="next" class="subb" value="Continue to Shiping"/>
                    </div>
                </form>
            </fieldset>
            <fieldset>
                <section class="thn_text">
                    <div class="container">
                        <h2>Thank you !</h2>
                  <!--      <p>We’ll sen a confirmation email with tracking info, a packing slip, checklist and details
                            about your sale to sample@gmail.com</p> -->
                        <p>You can sign in now</p>
                    </div>
                </section>
            </fieldset>

        </div>
    </section>






@endsection

