@extends('layouts.app')

@section('mainContent')


    <div class="history_directory faq_directory">
        <div class="container">
            <a href="#">Home</a> /
            <a href="#">Categories</a> /
            <span>Phones</span>
        </div>
    </div>


    <section class="phone_step">
        <!-- <div class="container"> -->
        <div class="phone_form">
            <form id="msform">
                <div class="container">
                    <ul id="progressbar">
                        <li class="active">1 <span>step</span></li>
                        <li>2 <span>step</span></li>
                        <li>3 <span>step</span></li>
                        <li>4 <span>step</span></li>
                        <li>5 <span>step</span></li>
                        <li>6 <span>step</span></li>
                    </ul>
                </div>

                <fieldset id="ModelList">
                    <div class="container">
                        <div class="step_name">
                            <h2><span>1</span>Which Apple Device Do You Have to Sell?</h2>
                            <p>Select Your iPhone Model Below to Get an Instant Quote</p>
                        </div>

                        <span><img src="/img/whi.svg" alt="">Which model do you have ?</span>
                        <!-- <input type="search" placeholder="Search">
                        <div class="clearfix"></div> -->

                        <div class="step1">


                            @foreach ($models as $model)
                                <div class="radio">
                                    <input id="model{{$model->id}}" name="modelId" type="radio" data-price=""
                                           value="{{$model->id}}">
                                    <label for="model{{$model->id}}" class="radio-step1">
                                        <img src="/{{$model->ModelImage}}" alt="">
                                        <h4>{{$model->ModelName}}</h4>
                                    </label>
                                </div>
                            @endforeach

                        </div>


                        <input type="button" name="next" class="subb" value="Continue" id="selectModel"/>
                    </div>
                </fieldset>

                <fieldset id="ParametrList" style="display:none;">
                    <div class="container">
                        <div class="step_name">
                            <h2><span>2</span>Choose the Carrier of Your iPhone Below</h2>
                            <p>Select Your iPhone Model Below to Get an Instant Quote</p>
                        </div>
                        <input type="button" name="previous" class="previous" value="Back"/>
                    </div>
                    <div class="container">
                        <div id="Parametrs">
                            {{csrf_field()}}
                            <input type="hidden" name="deviceId" id="deviceId">
                            <input type="hidden" name="modelId" id="ModelId">

                            <div class="select_cont" id="optionsList">
                                <!--    <div class="clearfix"></div> -->
                            </div>
                            <div class="step2" id="CarrierList">
                            </div>
                        </div>

                    </div>
                </fieldset>

                <fieldset id="ConditionList" style="display:none;">
                    <div class="container">
                        <div class="step_name margin_none">
                            <h2><span>3</span>What condition is your product in?</h2>
                        </div>
                        <input type="button" class="previous" value="Back"/>
                    </div>

                    <div class="step_wrapp_color">
                        <div class="container">
                            <div class="step_wrapp">
                                <div class="step_cont">
                                    <div class="step3" id="Conditions">
                                    </div>
                                    <div id="ConditionDescription">

                                    </div>

                                    <input type="button" class="next" value="Continue"/>

                                </div>

                                <div class="step_tov">

                                    <div class="tov_cont"><img class="ProductImage" src="" alt=""></div>
                                    <div class="tov_desc">
                                        <div class="model_wrapp">
                                            <div class="model">Model</div>
                                            <div class="model_name ModelName"></div>
                                        </div>
                                        <div class="model_desc ModelOptionsList">
                                            <p></p>
                                        </div>
                                    </div>

                                    <div class="condition">
                                        <p>Condition <a class="selectedCondition" href="#"></a></p>
                                    </div>
                                    <div class="tov_price">
                                        <div class="price_desc">Condition: <span class="selectedCondition"></span>
                                            Instant Quote:
                                        </div>
                                        <div class="price">$ <span class="calculatedOfer"> </span></div>
                                        <input type="hidden" name="offeredPrice" id="calculatedOfer">
                                    </div>

                                </div>
                            </div>


                        </div>
                    </div>
                </fieldset>

                <fieldset style="display:none;">
                    <div class="container">
                        <div class="step_name margin_none">
                            <h2><span>4</span>Choose How You Want To Be Paid?</h2>
                            <p>We'll send your payment within 2 business days after your item has been inspected and
                                send you an email to confirm payment has been sent</p>
                        </div>
                        <input type="button" class="previous" value="Back"/>
                    </div>

                    <div class="step_wrapp_color">
                        <div class="container">
                            <div class="step_wrapp">
                                <div class="step_cont">

                                    <div class="step4">

                                        <div class="radio tab">
                                            <input id="step4_1" name="PaymentType" type="radio" data-price="" checked
                                                   value="Check">
                                            <label for="step4_1" class="radio-step4">
                                                <div class="step4_img">
                                                    <img src="/img/check.svg" alt="">
                                                </div>
                                                <p>Delivered by USPS First Class mail within 10 days</p>
                                            </label>
                                        </div>
                                        <div class="radio tab">
                                            <input id="step4_2" name="PaymentType" type="radio" data-price=""
                                                   value="paypal">
                                            <label for="step4_2" class="radio-step4">
                                                <div class="step4_img">
                                                    <img src="/img/paypal.svg" alt="">
                                                </div>
                                                <p>Sent directly to your PayPal account</p>
                                            </label>
                                        </div>


                                    </div>
                                    <div class="tab_item">
                                        <div class="step4_cont">
                                            <h3>Check</h3>
                                            <p>We'll send you a check once we've inspected your items</p>
                                            <label>
                                                <span>Address Name (e.g. Home, Work, Beach House)</span>
                                                <input type="text" required placeholder='Address Name'
                                                       name="Payments_AddressName"
                                                       value="{{ (isset($userPaymentDetails->AddressName)? $userPaymentDetails->AddressName :'') }}">
                                            </label>
                                            <div class="bouble">
                                                <label>
                                                    <span>Firts Name</span>
                                                    <input type="text" required placeholder='Firts Name'
                                                           name="Payments_FirstName"
                                                           value="{{ (isset($userPaymentDetails->FirstName)? $userPaymentDetails->FirstName :'') }}">
                                                </label>
                                                <label>
                                                    <span>Last Name</span>
                                                    <input type="text" required placeholder='Last Name'
                                                           name="Payments_LastName"
                                                           value="{{ (isset($userPaymentDetails->LastName)? $userPaymentDetails->LastName :'') }}">
                                                </label>
                                            </div>
                                            <div class="bouble">
                                                <label>
                                                    <span>Address Line </span>
                                                    <input type="text" required placeholder='Address Line'
                                                           name="Payments_addres1"
                                                           value="{{ (isset($userPaymentDetails->addres1)? $userPaymentDetails->addres1 :'') }}">
                                                </label>
                                                <label>
                                                    <span>Adress Line 2</span>
                                                    <input type="text" required placeholder='Adress Line 2'
                                                           name="Payments_addres2"
                                                           value="{{ (isset($userPaymentDetails->addres2)? $userPaymentDetails->addres2 :'') }}">
                                                </label>
                                            </div>
                                            <div class="bouble">
                                                <label>
                                                    <span>City</span>
                                                    <input type="text" required placeholder='City' name="Payments_city"
                                                           value="{{ (isset($userPaymentDetails->city)? $userPaymentDetails->city :'') }}">
                                                </label>
                                                <div class="omg">
                                                    <div class="bouble">
                                                        <label>
                                                            <span>Select State</span>
                                                            <div class="select_phon">
                                                                <i class="arrow down"></i>
                                                                <select name="slct" id="slct" name="Payments_state">
                                                                    <option value="N/A">Select</option>
                                                                    <option value="AL">Alabama</option>
                                                                    <option value="AK">Alaska</option>
                                                                    <option value="AZ">Arizona</option>
                                                                    <option value="AR">Arkansas</option>
                                                                    <option value="CA">California</option>
                                                                    <option value="CO">Colorado</option>
                                                                    <option value="CT">Connecticut</option>
                                                                    <option value="DE">Delaware</option>
                                                                    <option value="DC">District Of Columbia</option>
                                                                    <option value="FL">Florida</option>
                                                                    <option value="GA">Georgia</option>
                                                                    <option value="HI">Hawaii</option>
                                                                    <option value="ID">Idaho</option>
                                                                    <option value="IL">Illinois</option>
                                                                    <option value="IN">Indiana</option>
                                                                    <option value="IA">Iowa</option>
                                                                    <option value="KS">Kansas</option>
                                                                    <option value="KY">Kentucky</option>
                                                                    <option value="LA">Louisiana</option>
                                                                    <option value="ME">Maine</option>
                                                                    <option value="MD">Maryland</option>
                                                                    <option value="MA">Massachusetts</option>
                                                                    <option value="MI">Michigan</option>
                                                                    <option value="MN">Minnesota</option>
                                                                    <option value="MS">Mississippi</option>
                                                                    <option value="MO">Missouri</option>
                                                                    <option value="MT">Montana</option>
                                                                    <option value="NE">Nebraska</option>
                                                                    <option value="NV">Nevada</option>
                                                                    <option value="NH">New Hampshire</option>
                                                                    <option value="NJ">New Jersey</option>
                                                                    <option value="NM">New Mexico</option>
                                                                    <option value="NY">New York</option>
                                                                    <option value="NC">North Carolina</option>
                                                                    <option value="ND">North Dakota</option>
                                                                    <option value="OH">Ohio</option>
                                                                    <option value="OK">Oklahoma</option>
                                                                    <option value="OR">Oregon</option>
                                                                    <option value="PA">Pennsylvania</option>
                                                                    <option value="RI">Rhode Island</option>
                                                                    <option value="SC">South Carolina</option>
                                                                    <option value="SD">South Dakota</option>
                                                                    <option value="TN">Tennessee</option>
                                                                    <option value="TX">Texas</option>
                                                                    <option value="UT">Utah</option>
                                                                    <option value="VT">Vermont</option>
                                                                    <option value="VA">Virginia</option>
                                                                    <option value="WA">Washington</option>
                                                                    <option value="WV">West Virginia</option>
                                                                    <option value="WI">Wisconsin</option>
                                                                    <option value="WY">Wyoming</option>
                                                                </select>
                                                            </div>
                                                        </label>
                                                        <label>
                                                            <span>Zip code</span>
                                                            <input type="text" required placeholder='Select State'
                                                                   name="Payments_zip"
                                                                   value="{{ (isset($userPaymentDetails->zip)? $userPaymentDetails->zip :'') }}">
                                                        </label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="check_cont">
                                            <label class="tasks-list-item">
                                                <input type="checkbox" name="task_1" value="1" class="tasks-list-cb"
                                                       checked>
                                                <span class="tasks-list-mark"></span>
                                                <span class="tasks-list-desc">"I understand that the final offer amint will be sent after item has been inspected and that the devise has not been reported lost or stolen"</span>
                                            </label>
                                            <input type="button" class="next" value="Continue to Shiping"/>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                    <div class="tab_item">
                                        <div class="step4_cont">
                                            <h3>PayPal Information</h3>
                                            <p>We'll instantly send the funds to your PayPal account once we've
                                                inspected your items</p>
                                            <div class="bouble">
                                                <label>
                                                    <span>Enter PayPal ID:</span>
                                                    <input type="text" required placeholder='Enter PayPal ID'
                                                           nmae="Payments_paypalId"
                                                           value="{{ (isset($userPaymentDetails->paypalId)? $userPaymentDetails->paypalId :'') }}">
                                                </label>
                                                <label>
                                                    <span>Confirm PayPal ID:</span>
                                                    <input type="text" required placeholder='Confirm PayPal ID'
                                                           nmae="Payments_paypalId_confirm"
                                                           value="{{ (isset($userPaymentDetails->paypalId)? $userPaymentDetails->paypalId :'') }}">
                                                </label>
                                            </div>
                                        </div>

                                        <div class="check_cont">
                                            <label class="tasks-list-item">
                                                <input type="checkbox" name="task_1" value="1" class="tasks-list-cb"
                                                       checked>
                                                <span class="tasks-list-mark"></span>
                                                <span class="tasks-list-desc">"I understand that the final offer amint will be sent after item has been inspected and that the devise has not been reported lost or stolen"</span>
                                            </label>
                                            <input type="button" class="next" value="Continue to Shiping"/>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="step_tov">
                                    <div class="tov_cont"><img class="ProductImage" src="" alt=""></div>
                                    <div class="tov_desc">
                                        <div class="model_wrapp">
                                            <div class="model">Model</div>
                                            <div class="model_name ModelName"></div>
                                        </div>
                                        <div class="model_desc ModelOptionsList">
                                            <p></p>
                                        </div>
                                    </div>

                                    <div class="condition">
                                        <p>Condition <a class="selectedCondition" href="#"></a></p>
                                    </div>
                                    <div class="tov_price">
                                        <div class="price_desc">Condition: <span class="selectedCondition"></span>
                                            Instant Quote:
                                        </div>
                                        <div class="price">$ <span class="calculatedOfer"> </span></div>
                                    </div>

                                </div>
                            </div>


                        </div>
                    </div>
                </fieldset>

                <fieldset style="display:none;">
                    <div class="container">
                        <div class="step_name margin_none">
                            <h2><span>5</span>Shipping Info</h2>
                        </div>
                        <input type="button" name="previous" class="previous" value="Back"/>
                    </div>

                    <div class="step_wrapp_color">
                        <div class="container">
                            <div class="step_wrapp">
                                <div class="step_cont">

                                    <div class="step5">
                                        <div class="radio">
                                            <input id="step5_1" name="radio5" type="radio" data-price=""
                                                   value="Shipping Information">
                                            <label for="step5_1" class="radio-step5">
                                                <div class="step5_img">
                                                    <img src="/img/ship.svg" alt="">
                                                    <span>Shipping <br>Information</span>
                                                </div>
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <input id="step5_2" name="radio5" type="radio" data-price=""
                                                   value="I have account">
                                            <label for="step5_2" class="radio-step5">
                                                <div class="step5_img">
                                                    <img src="/img/acco.svg" alt="">
                                                    <span>I have <br> account</span>
                                                </div>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="step4_cont step5_cont">
                                        <h3>Customer information </h3>
                                        <label>
                                            <span>E-mail</span>
                                            <input type="email" placeholder='E-mail' name="shipping_email"
                                                   value="{{ (isset($userShippingDetails->shipping_email)? $userShippingDetails->shipping_email :'') }}">
                                        </label>

                                        <div class="bouble">
                                            <label>
                                                <span>First Name</span>
                                                <input type="text" placeholder='First Name ' name="shipping_FirstName"
                                                       value="{{ (isset($userShippingDetails->shipping_FirstName)? $userShippingDetails->shipping_FirstName :'') }}">
                                            </label>
                                            <label>
                                                <span>Last Name </span>
                                                <input type="text" placeholder='Last Name ' name="shipping_LastName"
                                                       value="{{ (isset($userShippingDetails->shipping_LastName)? $userShippingDetails->shipping_LastName :'') }}">
                                            </label>
                                        </div>
                                        <label>
                                            <span>Address</span>
                                            <input type="text" placeholder='Address' name="shipping_Addres"
                                                   value="{{ (isset($userShippingDetails->shipping_Addres)? $userShippingDetails->shipping_Addres :'') }}">
                                        </label>
                                        <label>
                                            <span>City</span>
                                            <input type="text" placeholder='City' name="shipping_city"
                                                   value="{{ (isset($userShippingDetails->shipping_city)? $userShippingDetails->shipping_city :'') }}">
                                        </label>

                                        <div class="bouble">
                                            <label>
                                                <span>Country</span>
                                                <div class="select_phon">
                                                    <i class="arrow down"></i>
                                                    <select name="shipping_country"
                                                            value="{{ (isset($userShippingDetails->shipping_country)? $userShippingDetails->shipping_country :'') }}">
                                                            <option value="AF">Afghanistan</option>
                                                            <option value="AX">Åland Islands</option>
                                                            <option value="AL">Albania</option>
                                                            <option value="DZ">Algeria</option>
                                                            <option value="AS">American Samoa</option>
                                                            <option value="AD">Andorra</option>
                                                            <option value="AO">Angola</option>
                                                            <option value="AI">Anguilla</option>
                                                            <option value="AQ">Antarctica</option>
                                                            <option value="AG">Antigua and Barbuda</option>
                                                            <option value="AR">Argentina</option>
                                                            <option value="AM">Armenia</option>
                                                            <option value="AW">Aruba</option>
                                                            <option value="AU">Australia</option>
                                                            <option value="AT">Austria</option>
                                                            <option value="AZ">Azerbaijan</option>
                                                            <option value="BS">Bahamas</option>
                                                            <option value="BH">Bahrain</option>
                                                            <option value="BD">Bangladesh</option>
                                                            <option value="BB">Barbados</option>
                                                            <option value="BY">Belarus</option>
                                                            <option value="BE">Belgium</option>
                                                            <option value="BZ">Belize</option>
                                                            <option value="BJ">Benin</option>
                                                            <option value="BM">Bermuda</option>
                                                            <option value="BT">Bhutan</option>
                                                            <option value="BO">Bolivia, Plurinational State of</option>
                                                            <option value="BQ">Bonaire, Sint Eustatius and Saba</option>
                                                            <option value="BA">Bosnia and Herzegovina</option>
                                                            <option value="BW">Botswana</option>
                                                            <option value="BV">Bouvet Island</option>
                                                            <option value="BR">Brazil</option>
                                                            <option value="IO">British Indian Ocean Territory</option>
                                                            <option value="BN">Brunei Darussalam</option>
                                                            <option value="BG">Bulgaria</option>
                                                            <option value="BF">Burkina Faso</option>
                                                            <option value="BI">Burundi</option>
                                                            <option value="KH">Cambodia</option>
                                                            <option value="CM">Cameroon</option>
                                                            <option value="CA">Canada</option>
                                                            <option value="CV">Cape Verde</option>
                                                            <option value="KY">Cayman Islands</option>
                                                            <option value="CF">Central African Republic</option>
                                                            <option value="TD">Chad</option>
                                                            <option value="CL">Chile</option>
                                                            <option value="CN">China</option>
                                                            <option value="CX">Christmas Island</option>
                                                            <option value="CC">Cocos (Keeling) Islands</option>
                                                            <option value="CO">Colombia</option>
                                                            <option value="KM">Comoros</option>
                                                            <option value="CG">Congo</option>
                                                            <option value="CD">Congo, the Democratic Republic of the</option>
                                                            <option value="CK">Cook Islands</option>
                                                            <option value="CR">Costa Rica</option>
                                                            <option value="CI">Côte d'Ivoire</option>
                                                            <option value="HR">Croatia</option>
                                                            <option value="CU">Cuba</option>
                                                            <option value="CW">Curaçao</option>
                                                            <option value="CY">Cyprus</option>
                                                            <option value="CZ">Czech Republic</option>
                                                            <option value="DK">Denmark</option>
                                                            <option value="DJ">Djibouti</option>
                                                            <option value="DM">Dominica</option>
                                                            <option value="DO">Dominican Republic</option>
                                                            <option value="EC">Ecuador</option>
                                                            <option value="EG">Egypt</option>
                                                            <option value="SV">El Salvador</option>
                                                            <option value="GQ">Equatorial Guinea</option>
                                                            <option value="ER">Eritrea</option>
                                                            <option value="EE">Estonia</option>
                                                            <option value="ET">Ethiopia</option>
                                                            <option value="FK">Falkland Islands (Malvinas)</option>
                                                            <option value="FO">Faroe Islands</option>
                                                            <option value="FJ">Fiji</option>
                                                            <option value="FI">Finland</option>
                                                            <option value="FR">France</option>
                                                            <option value="GF">French Guiana</option>
                                                            <option value="PF">French Polynesia</option>
                                                            <option value="TF">French Southern Territories</option>
                                                            <option value="GA">Gabon</option>
                                                            <option value="GM">Gambia</option>
                                                            <option value="GE">Georgia</option>
                                                            <option value="DE">Germany</option>
                                                            <option value="GH">Ghana</option>
                                                            <option value="GI">Gibraltar</option>
                                                            <option value="GR">Greece</option>
                                                            <option value="GL">Greenland</option>
                                                            <option value="GD">Grenada</option>
                                                            <option value="GP">Guadeloupe</option>
                                                            <option value="GU">Guam</option>
                                                            <option value="GT">Guatemala</option>
                                                            <option value="GG">Guernsey</option>
                                                            <option value="GN">Guinea</option>
                                                            <option value="GW">Guinea-Bissau</option>
                                                            <option value="GY">Guyana</option>
                                                            <option value="HT">Haiti</option>
                                                            <option value="HM">Heard Island and McDonald Islands</option>
                                                            <option value="VA">Holy See (Vatican City State)</option>
                                                            <option value="HN">Honduras</option>
                                                            <option value="HK">Hong Kong</option>
                                                            <option value="HU">Hungary</option>
                                                            <option value="IS">Iceland</option>
                                                            <option value="IN">India</option>
                                                            <option value="ID">Indonesia</option>
                                                            <option value="IR">Iran, Islamic Republic of</option>
                                                            <option value="IQ">Iraq</option>
                                                            <option value="IE">Ireland</option>
                                                            <option value="IM">Isle of Man</option>
                                                            <option value="IL">Israel</option>
                                                            <option value="IT">Italy</option>
                                                            <option value="JM">Jamaica</option>
                                                            <option value="JP">Japan</option>
                                                            <option value="JE">Jersey</option>
                                                            <option value="JO">Jordan</option>
                                                            <option value="KZ">Kazakhstan</option>
                                                            <option value="KE">Kenya</option>
                                                            <option value="KI">Kiribati</option>
                                                            <option value="KP">Korea, Democratic People's Republic of</option>
                                                            <option value="KR">Korea, Republic of</option>
                                                            <option value="KW">Kuwait</option>
                                                            <option value="KG">Kyrgyzstan</option>
                                                            <option value="LA">Lao People's Democratic Republic</option>
                                                            <option value="LV">Latvia</option>
                                                            <option value="LB">Lebanon</option>
                                                            <option value="LS">Lesotho</option>
                                                            <option value="LR">Liberia</option>
                                                            <option value="LY">Libya</option>
                                                            <option value="LI">Liechtenstein</option>
                                                            <option value="LT">Lithuania</option>
                                                            <option value="LU">Luxembourg</option>
                                                            <option value="MO">Macao</option>
                                                            <option value="MK">Macedonia, the former Yugoslav Republic of</option>
                                                            <option value="MG">Madagascar</option>
                                                            <option value="MW">Malawi</option>
                                                            <option value="MY">Malaysia</option>
                                                            <option value="MV">Maldives</option>
                                                            <option value="ML">Mali</option>
                                                            <option value="MT">Malta</option>
                                                            <option value="MH">Marshall Islands</option>
                                                            <option value="MQ">Martinique</option>
                                                            <option value="MR">Mauritania</option>
                                                            <option value="MU">Mauritius</option>
                                                            <option value="YT">Mayotte</option>
                                                            <option value="MX">Mexico</option>
                                                            <option value="FM">Micronesia, Federated States of</option>
                                                            <option value="MD">Moldova, Republic of</option>
                                                            <option value="MC">Monaco</option>
                                                            <option value="MN">Mongolia</option>
                                                            <option value="ME">Montenegro</option>
                                                            <option value="MS">Montserrat</option>
                                                            <option value="MA">Morocco</option>
                                                            <option value="MZ">Mozambique</option>
                                                            <option value="MM">Myanmar</option>
                                                            <option value="NA">Namibia</option>
                                                            <option value="NR">Nauru</option>
                                                            <option value="NP">Nepal</option>
                                                            <option value="NL">Netherlands</option>
                                                            <option value="NC">New Caledonia</option>
                                                            <option value="NZ">New Zealand</option>
                                                            <option value="NI">Nicaragua</option>
                                                            <option value="NE">Niger</option>
                                                            <option value="NG">Nigeria</option>
                                                            <option value="NU">Niue</option>
                                                            <option value="NF">Norfolk Island</option>
                                                            <option value="MP">Northern Mariana Islands</option>
                                                            <option value="NO">Norway</option>
                                                            <option value="OM">Oman</option>
                                                            <option value="PK">Pakistan</option>
                                                            <option value="PW">Palau</option>
                                                            <option value="PS">Palestinian Territory, Occupied</option>
                                                            <option value="PA">Panama</option>
                                                            <option value="PG">Papua New Guinea</option>
                                                            <option value="PY">Paraguay</option>
                                                            <option value="PE">Peru</option>
                                                            <option value="PH">Philippines</option>
                                                            <option value="PN">Pitcairn</option>
                                                            <option value="PL">Poland</option>
                                                            <option value="PT">Portugal</option>
                                                            <option value="PR">Puerto Rico</option>
                                                            <option value="QA">Qatar</option>
                                                            <option value="RE">Réunion</option>
                                                            <option value="RO">Romania</option>
                                                            <option value="RU">Russian Federation</option>
                                                            <option value="RW">Rwanda</option>
                                                            <option value="BL">Saint Barthélemy</option>
                                                            <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                                                            <option value="KN">Saint Kitts and Nevis</option>
                                                            <option value="LC">Saint Lucia</option>
                                                            <option value="MF">Saint Martin (French part)</option>
                                                            <option value="PM">Saint Pierre and Miquelon</option>
                                                            <option value="VC">Saint Vincent and the Grenadines</option>
                                                            <option value="WS">Samoa</option>
                                                            <option value="SM">San Marino</option>
                                                            <option value="ST">Sao Tome and Principe</option>
                                                            <option value="SA">Saudi Arabia</option>
                                                            <option value="SN">Senegal</option>
                                                            <option value="RS">Serbia</option>
                                                            <option value="SC">Seychelles</option>
                                                            <option value="SL">Sierra Leone</option>
                                                            <option value="SG">Singapore</option>
                                                            <option value="SX">Sint Maarten (Dutch part)</option>
                                                            <option value="SK">Slovakia</option>
                                                            <option value="SI">Slovenia</option>
                                                            <option value="SB">Solomon Islands</option>
                                                            <option value="SO">Somalia</option>
                                                            <option value="ZA">South Africa</option>
                                                            <option value="GS">South Georgia and the South Sandwich Islands</option>
                                                            <option value="SS">South Sudan</option>
                                                            <option value="ES">Spain</option>
                                                            <option value="LK">Sri Lanka</option>
                                                            <option value="SD">Sudan</option>
                                                            <option value="SR">Suriname</option>
                                                            <option value="SJ">Svalbard and Jan Mayen</option>
                                                            <option value="SZ">Swaziland</option>
                                                            <option value="SE">Sweden</option>
                                                            <option value="CH">Switzerland</option>
                                                            <option value="SY">Syrian Arab Republic</option>
                                                            <option value="TW">Taiwan, Province of China</option>
                                                            <option value="TJ">Tajikistan</option>
                                                            <option value="TZ">Tanzania, United Republic of</option>
                                                            <option value="TH">Thailand</option>
                                                            <option value="TL">Timor-Leste</option>
                                                            <option value="TG">Togo</option>
                                                            <option value="TK">Tokelau</option>
                                                            <option value="TO">Tonga</option>
                                                            <option value="TT">Trinidad and Tobago</option>
                                                            <option value="TN">Tunisia</option>
                                                            <option value="TR">Turkey</option>
                                                            <option value="TM">Turkmenistan</option>
                                                            <option value="TC">Turks and Caicos Islands</option>
                                                            <option value="TV">Tuvalu</option>
                                                            <option value="UG">Uganda</option>
                                                            <option value="UA">Ukraine</option>
                                                            <option value="AE">United Arab Emirates</option>
                                                            <option value="GB">United Kingdom</option>
                                                            <option value="US">United States</option>
                                                            <option value="UM">United States Minor Outlying Islands</option>
                                                            <option value="UY">Uruguay</option>
                                                            <option value="UZ">Uzbekistan</option>
                                                            <option value="VU">Vanuatu</option>
                                                            <option value="VE">Venezuela, Bolivarian Republic of</option>
                                                            <option value="VN">Viet Nam</option>
                                                            <option value="VG">Virgin Islands, British</option>
                                                            <option value="VI">Virgin Islands, U.S.</option>
                                                            <option value="WF">Wallis and Futuna</option>
                                                            <option value="EH">Western Sahara</option>
                                                            <option value="YE">Yemen</option>
                                                            <option value="ZM">Zambia</option>
                                                            <option value="ZW">Zimbabwe</option>
                                                    </select>
                                                </div>
                                            </label>
                                            <label>
                                                <span>State/Province</span>
                                                <div class="select_phon">
                                                    <i class="arrow down"></i>
                                                    <select name="shipping_province"
                                                            value="{{ (isset($userShippingDetails->shipping_province)? $userShippingDetails->shipping_province :'') }}">
                                                        <option value="N/A">Select</option>
                                                        <option value="AL">Alabama</option>
                                                        <option value="AK">Alaska</option>
                                                        <option value="AZ">Arizona</option>
                                                        <option value="AR">Arkansas</option>
                                                        <option value="CA">California</option>
                                                        <option value="CO">Colorado</option>
                                                        <option value="CT">Connecticut</option>
                                                        <option value="DE">Delaware</option>
                                                        <option value="DC">District Of Columbia</option>
                                                        <option value="FL">Florida</option>
                                                        <option value="GA">Georgia</option>
                                                        <option value="HI">Hawaii</option>
                                                        <option value="ID">Idaho</option>
                                                        <option value="IL">Illinois</option>
                                                        <option value="IN">Indiana</option>
                                                        <option value="IA">Iowa</option>
                                                        <option value="KS">Kansas</option>
                                                        <option value="KY">Kentucky</option>
                                                        <option value="LA">Louisiana</option>
                                                        <option value="ME">Maine</option>
                                                        <option value="MD">Maryland</option>
                                                        <option value="MA">Massachusetts</option>
                                                        <option value="MI">Michigan</option>
                                                        <option value="MN">Minnesota</option>
                                                        <option value="MS">Mississippi</option>
                                                        <option value="MO">Missouri</option>
                                                        <option value="MT">Montana</option>
                                                        <option value="NE">Nebraska</option>
                                                        <option value="NV">Nevada</option>
                                                        <option value="NH">New Hampshire</option>
                                                        <option value="NJ">New Jersey</option>
                                                        <option value="NM">New Mexico</option>
                                                        <option value="NY">New York</option>
                                                        <option value="NC">North Carolina</option>
                                                        <option value="ND">North Dakota</option>
                                                        <option value="OH">Ohio</option>
                                                        <option value="OK">Oklahoma</option>
                                                        <option value="OR">Oregon</option>
                                                        <option value="PA">Pennsylvania</option>
                                                        <option value="RI">Rhode Island</option>
                                                        <option value="SC">South Carolina</option>
                                                        <option value="SD">South Dakota</option>
                                                        <option value="TN">Tennessee</option>
                                                        <option value="TX">Texas</option>
                                                        <option value="UT">Utah</option>
                                                        <option value="VT">Vermont</option>
                                                        <option value="VA">Virginia</option>
                                                        <option value="WA">Washington</option>
                                                        <option value="WV">West Virginia</option>
                                                        <option value="WI">Wisconsin</option>
                                                        <option value="WY">Wyoming</option>
                                                    </select>
                                                </div>
                                            </label>

                                            <label>
                                                <span>Zip/Postal code  </span>
                                                <input type="text" placeholder='Zip/Postal code' name="shipping_zip"
                                                       value="{{ (isset($userShippingDetails->shipping_zip)? $userShippingDetails->shipping_zip :'') }}">
                                            </label>

                                            <label>
                                                <span>Phone Number </span>
                                                <input type="text" placeholder='Phone Number' name="shipping_phone"
                                                       value="{{ (isset($userShippingDetails->shipping_phone)? $userShippingDetails->shipping_phone :'') }}">
                                            </label>
                                        </div>
                                    </div>

                                    <!-- <input type="submit" class='subb' value="Continue to Shiping"> -->
                                    <input type="button" name="next" class="next" value="Continue to Shiping"/>
                                </div>

                                <div class="step_tov">

                                    <div class="tov_cont"><img class="ProductImage" src="" alt=""></div>
                                    <div class="tov_desc">
                                        <div class="model_wrapp">
                                            <div class="model">Model</div>
                                            <div class="model_name ModelName"></div>
                                        </div>
                                        <div class="model_desc ModelOptionsList">
                                            <p></p>
                                        </div>
                                    </div>

                                    <div class="condition">
                                        <p>Condition <a class="selectedCondition" href="#"></a></p>
                                    </div>
                                    <div class="tov_price">
                                        <div class="price_desc">Condition: <span class="selectedCondition"></span>
                                            Instant Quote:
                                        </div>
                                        <div class="price">$ <span class="calculatedOfer"> </span></div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </fieldset>

                <fieldset style="display:none;">
                    <div class="container">
                        <div class="step_name margin_none">
                            <h2><span>6</span>Confirm your details</h2>
                            <p>Please confirm the following information is correct.</p>
                        </div>
                        <input type="button" name="previous" class="previous" value="Back"/>
                    </div>

                    <div class="step_wrapp_color">
                        <div class="container">
                            <div class="step_wrapp">
                                <div class="step_cont">

                                    <div class="step6">

                                        <div class="step6_radio">
                                            <p>You can pick the most convenient way to send your items to us.</p>
                                            <strong>Choose from:</strong>
                                            <div class="radio">
                                                <input id="step6_1" name="SendType" type="radio" data-price=""
                                                       value="A box and prepaid label">
                                                <label for="step6_1" class="radio-step6">
                                                    <div class="step6_img">
                                                        <img src="/img/step6.svg" alt="">
                                                    </div>
                                                    <div class="step6_text">
                                                        <h2>A box and prepaid label </h2>
                                                        <p>We’ll send a prepaid shipping label abd a box right to your
                                                            house.</p>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <input id="step6_2" name="SendType" type="radio" data-price=""
                                                       value="Prepaid FedEX shipping label">
                                                <label for="step6_2" class="radio-step6">
                                                    <div class="step6_img">
                                                        <img src="/img/step6_2.png" alt="">
                                                    </div>
                                                    <div class="step6_text">
                                                        <h2>Prepaid FedEX shipping label</h2>
                                                        <p>We’ll send a prepaid label to your own box and drop it at any
                                                            FedEx location.</p>
                                                    </div>
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <input id="step6_3" name="SendType" type="radio" data-price=""
                                                       value="Prepaid USPS shipping label">
                                                <label for="step6_3" class="radio-step6">
                                                    <div class="step6_img">
                                                        <img src="/img/step6_3.png" alt="">
                                                    </div>
                                                    <div class="step6_text">
                                                        <h2>Prepaid USPS shipping label</h2>
                                                        <p>We’ll send a prepaid shipping label abd a box right to your
                                                            house.</p>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="step6_form">
                                            <label>
                                                <div class="st_form_text">
                                                    <h2>Contact Email.</h2>
                                                    <p>All emails about your order will be sent here:</p>
                                                </div>
                                                <input type="text" required placeholder='sample@gmail.com' name="email"
                                                       value="{{ (isset($user->email)? $user->email :'') }}">
                                            </label>
                                            <label>
                                                <div class="st_form_text">
                                                    <h2>Payment Details</h2>
                                                    <p>We’ll send your PayPal payment to:</p>
                                                </div>
                                                <input type="text" required placeholder='sample@gmail.com'
                                                       name="paypal_account"
                                                       value="{{ (isset($userPaymentDetails->paypalId)? $userPaymentDetails->paypalId :'') }}">
                                            </label>
                                            <label>
                                                <div class="st_form_text">
                                                    <h2>Shipping Details</h2>
                                                    <p>We’ll send your PayPal payment to:</p>
                                                </div>
                                                <textarea required placeholder='sample@gmail.com' name="paypal_account2"
                                                          value="{{ (isset($userPaymentDetails->paypalId)? $userPaymentDetails->paypalId :'') }}"></textarea>
                                            </label>
                                        </div>
                                        <div class="team_serv">
                                            <h2>Term of Service</h2>
                                            <div class="team_serv_cont">
                                                <label class="tasks-list-item">
                                                    <input type="checkbox" name="Terms" value="1" class="tasks-list-cb"
                                                           checked>
                                                    <span class="tasks-list-mark"></span>
                                                    <span class="tasks-list-desc">"By checking this box, you agree to our <a
                                                                href="/terms">terms service</a> and confirm that the item(s) you are selling have not been reported lost or stolen.</span>
                                                </label>
                                            </div>

                                        </div>

                                    </div>

                                    <input type="submit" class='subb' value="Complete" id="SendForm">
                                </div>

                                <div class="step_tov">

                                    <div class="tov_cont"><img class="ProductImage" src="" alt=""></div>
                                    <div class="tov_desc">
                                        <div class="model_wrapp">
                                            <div class="model">Model</div>
                                            <div class="model_name ModelName"></div>
                                        </div>
                                        <div class="model_desc ModelOptionsList">
                                            <p></p>
                                        </div>
                                    </div>

                                    <div class="condition">
                                        <p>Condition <a class="selectedCondition" href="#"></a></p>
                                    </div>
                                    <div class="tov_price">
                                        <div class="price_desc">Condition: <span class="selectedCondition"></span>
                                            Instant Quote:
                                        </div>
                                        <div class="price">$ <span clkass="calculatedOfer"> </span></div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </fieldset>

            </form>
        </div>
        <!-- </div> -->
    </section>



@endsection