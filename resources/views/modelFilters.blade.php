@extends('layouts.bootstrap')

@section('content')

    <div class="well"><h1>Select additional filters please</h1></div>


    <form method="Post" action="/calculate">
        {{csrf_field()}}
        <input type="hidden" name="model" value="{{$model->id}}">
        <input type="hidden" name="device" value="{{$device->id}}">


        @if(!empty($conditions))
            <div class="form-group">
                <label for="sel1">Conditions:</label>
                <select class="form-control" name="condition">
                    @foreach( $conditions as $condition            )
                        <option value="{{$condition->ConditionId}}">  {{$condition->ConditionName}}</option>
                    @endforeach
                </select>
            </div>
        @endif


        @if(!empty($colors))
            <div class="form-group">
                <label for="sel1">colors:</label>
                <select class="form-control" name="color">
                    @foreach( $colors as $color            )
                        <option value="{{$color->ColorId}}">  {{$color->ColorName}}</option>
                    @endforeach
                </select>
            </div>
        @endif



        @if(!empty($storages))
            <div class="form-group">
                <label for="sel1">storages:</label>
                <select class="form-control" name="storage">
                    @foreach( $storages as $storage            )
                        <option value="{{$storage->StorageId}}">  {{$storage->StorageName}}</option>
                    @endforeach
                </select>
            </div>
        @endif



        @if(!empty($screensizes))
            <div class="form-group">
                <label for="sel1">screensizes:</label>
                <select class="form-control" name="screensize">
                    @foreach( $screensizes as $screensize            )
                        <option value="{{$screensize->ScreenSizeId}}">  {{$screensize->ScreenSizeName}}</option>
                    @endforeach
                </select>
            </div>
        @endif

        @if(!empty($processors))
            <div class="form-group">
                <label for="sel1">processors:</label>
                <select class="form-control" name="processor">
                    @foreach( $processors as $processor            )
                        <option value="{{$processor->ProcessorId}}">  {{$processor->ProcessorName}}</option>
                    @endforeach
                </select>
            </div>
        @endif


        @if(!empty($harddrives))
            <div class="form-group">
                <label for="sel1">harddrives:</label>
                <select class="form-control" name="harddrive">
                    @foreach( $harddrives as $harddrive            )
                        <option value="{{$harddrive->HardDriveId}}">  {{$harddrive->HardDriveName}}</option>
                    @endforeach
                </select>
            </div>
        @endif


        @if(!empty($memorys))
            <div class="form-group">
                <label for="sel1">memorys:</label>
                <select class="form-control" name="memory">
                    @foreach( $memorys as $memory            )
                        <option value="{{$memory->MemoryId}}">  {{$memory->MemoryName}}</option>
                    @endforeach
                </select>
            </div>
        @endif

        @if(!empty($years))
            <div class="form-group">
                <label for="sel1">years:</label>
                <select class="form-control" name="year">
                    @foreach( $years as $year            )
                        <option value="{{$year->YearId}}">  {{$year->YearName}}</option>
                    @endforeach
                </select>
            </div>
        @endif



        @if(!empty($accesories))
            <div class="form-group">
                <label for="sel1">accesories:</label>
                <select class="form-control" name="accesorie">
                    @foreach( $accesories as $accesorie            )
                        <option value="{{$accesorie->AccesorieId}}">  {{$accesorie->AccesoriesName}}</option>
                    @endforeach
                </select>
            </div>
        @endif


        @if(!empty($carriers))
            <div class="form-group">
                <label for="sel1">carriers:</label>
                <select class="form-control" name="carrier">
                    @foreach( $carriers as $carrier            )
                        <option value="{{$carrier->CarrierId}}">  {{$carrier->CarrierName}}</option>
                    @endforeach
                </select>
            </div>
        @endif




        <button type="submit" class="btn btn-primary">Calculate</button>

    </form>



@endsection