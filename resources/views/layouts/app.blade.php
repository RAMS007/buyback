<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">

    <title>title</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Template Basic Images Start -->
    <!-- 	<meta property="og:image" content="path/to/image.jpg">
        <link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" href="img/favicon/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-touch-icon-114x114.png"> -->
    <!-- Template Basic Images End -->

    <link rel="stylesheet" href="/css/fonts.min.css">
    <link rel="stylesheet" href="/css/libs.min.css">
    <link rel="stylesheet" href="/css/main.min.css">

    <!-- Custom Browsers Color Start -->
    <!-- Chrome, Firefox OS and Opera -->
    <!-- <meta name="theme-color" content="#fff"> -->
    <!-- Windows Phone -->
    <!-- <meta name="msapplication-navbutton-color" content="#fff"> -->
    <!-- iOS Safari -->
    <!-- <meta name="apple-mobile-web-app-status-bar-style" content="#fff"> -->
    <!-- Custom Browsers Color End -->

</head>

<body>
<header>
    <div class="top_mnu_wrapp">
        <div class="container">
            <nav class="top_mnu">
                <a href="#" class="toggle-mnu"><span></span></a>
                <ul class='mnu'>
                    <li><a href="#">How it works</a></li>
                    <li><a href="/aboutUs">Why us us?</a></li>
                    <li><a href="/faq">FaQs</a></li>
                    <li><a href="/quotes">Custom Quotes</a></li>
                    <li><a href="/privacy">Privacy & Policy</a></li>
                    <li><a href="/contactUs">Contact US</a></li>
                </ul>
                <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="116" data-layout="button_count" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div>


                @if (Auth::guest())
                    <a class='sign' href="/login">Sign in</a>
                    <a class='sign' href="/register"> Sign up</a>
                @endif



            </nav>
        </div>
    </div>

    <div class="cat_mnu_wrapp">
        <div class="container">
            <div class="cat_mnu">
                <a href="/" class="logo"><img src="/img/logo.png" alt=""></a>
                <div class="cat_rside">
                    <ul class="categories">
                        <li>
                            <a href="#">categories</a>
                            <ul>
                                <li><a href="/device/1">Phones</a></li>
                                <li><a href="/device/2">Notebooks</a></li>
                                <li><a href="/device/3">Gaming Concoles</a></li>
                                <li><a href="/device/4">Cameras</a></li>
                                <li><a href="/device/5">Tablets</a></li>
                                <li><a href="/device/6">iPhones</a></li>
                                <li><a href="/device/7">Apple Desktops</a></li>
                                <li><a href="/device/8">Apple Accessories</a></li>
                                <li><a href="/device/9">iPods</a></li>
                                <li><a href="/device/10">Macbook</a></li>
                                <li><a href="/device/11">Smart Watch</a></li>
                                <li><a href="/device/12">Everything Else</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="cat_search">
                        <form>
                            <input type="search" placeholder='Search for what you want to sell and qet an instant quote'>
                            <input type="submit">
                        </form>
                    </div>
                </div>




                @if (Auth::guest())
                @else

                    <div class="profile">
                        <div class="profile_img">
                            <img src="http://placekitten.com/g/41/41" alt="">
                        </div>
                        <a href="#">{{ Auth::user()->name }}</a>
                        <ul>
                            <li><a href="/profile/orders">my orders</a></li>
                            <li><a href="/profile">settings</a></li>
                            <li><a href="{{ url('/logout') }}">log out</a></li>
                        </ul>
                    </div>

                @endif



            </div>
        </div>
    </div>
</header>
<main>
@yield('mainContent')



</main>
<footer>
    <div class="container">
        <div class="f_top">
            <div class="f_info">
                <a href="#" class="f_logo"><img src="img/logo.png" alt=""></a>
                <p>
                    It is a long established fact that a reader will be distracted by the
                    readable content of a page when looking at its layout.
                </p>
            </div>
            <div class="f_nav_cont">
                <div class="f_nav doub_list">
                    <h5>Categories</h5>
                    <ul>
                        <li><a href="/device/1">Phones</a></li>
                        <li><a href="/device/2">Notebooks</a></li>
                        <li><a href="/device/3">Gaming Concoles</a></li>
                        <li><a href="/device/4">Cameras</a></li>
                        <li><a href="/device/5">Tablets</a></li>
                        <li><a href="/device/6">iPhones</a></li>
                        <li><a href="/device/7">Apple Desktops</a></li>
                        <li><a href="/device/8">Apple Accessories</a></li>
                        <li><a href="/device/9">iPods</a></li>
                        <li><a href="/device/10">Macbook</a></li>
                        <li><a href="/device/11">Smart Watch</a></li>
                        <li><a href="/device/12">Everything Else</a></li>
                    </ul>
                </div>
                <div class="f_nav">
                    <h5>About Us</h5>
                    <ul>
                        <li><a href="#">How it works </a></li>
                        <li><a href="/aboutUs">Why us us?</a></li>
                        <li><a href="/quotes">Custom quotes </a></li>
                    </ul>
                </div>
                <div class="f_nav">
                    <h5>Support</h5>
                    <ul>
                        <li><a href="/faq">FAQS</a></li>
                        <li><a href="/privacy">Privacy & Policy</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
    <hr>
    <div class="container">
        <div class="f_bot">
            <div class="copy">All Right Reserved 2017</div>
            <div class="social">
                <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</footer>

<div class="preloader" style="display:none;">
    <div class="pulse"></div>
</div>


<script src="/js/libs.min.js"></script>

<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.10";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<script src="/js/common.js"></script>
<script src="/js/custom.js"></script>

@yield('scripts')


</body>
</html>
