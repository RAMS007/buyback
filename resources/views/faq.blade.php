@extends('layouts.app')

@section('mainContent')

    <div class="history_directory faq_directory">
        <div class="container">
            <a href="#">Home</a> /
            <span>Faqs</span>
        </div>
    </div>



    <section class="faq_wrapp">
        <div class="container">
            <h2>FAQs</h2>
            <div class="faqs">

                <div class="item">
                    <div class="question">
                        <p>How do I deactivate iOS 7 AFTER I send  it to BuyBackWorld?</p>
                        <div class="icon"></div>
                    </div>
                    <div class="answer" style="display: none;">We can help you get into a new machine even if you are currently in a lease even if it is with a different company.</div>
                </div>
                <div class="item">
                    <div class="question">
                        <p>How do I deactivate iOS 7 AFTER I send it to BuyBackWorld?</p>
                        <div class="icon"></div>
                    </div>
                    <div class="answer">Yes, we have over 10 models that allow you to do all these things depending on your requirements.</div>
                </div>
                <div class="item">
                    <div class="question">
                        <p>How do I deactivate iOS 7 AFTER I send it to BuyBackWorld?</p>
                        <div class="icon"></div>
                    </div>
                    <div class="answer">Most of our machines are leased, depending on your credit we can come up with different options 24,26,48,60 month leases.</div>
                </div>
                <div class="item">
                    <div class="question">
                        <p>Can I order an item now and pay for it later?</p>
                        <div class="icon"></div>
                    </div>
                    <div class="answer">We offer a service contract with all of our machines that include all parts, labor, retrofits and toner (everything except paper).</div>
                </div>
                <div class="item">
                    <div class="question">
                        <p>Can I order an item now and pay for it later?</p>
                        <div class="icon"></div>
                    </div>
                    <div class="answer" style="display: none;">We will respond in a guaranteed 3 hours after the initial call.</div>
                </div>
                <div class="item">
                    <div class="question">
                        <p>What is your return policy for Certified Pre-Owned Devices?</p>
                        <div class="icon"></div>
                    </div>
                    <div class="answer">We provide sales and service to most cities and towns in Ontario, including Toronto, Mississauga, Brampton, Hamilton, Burlington, Oakville, Markham, Vaughan, Scarborough, North York, Richmond Hill, Etobicoke, Pickering, Ajax, Ottawa, London, Woodstock, St. Thomas, Kitchener, Waterloo, Cambridge, Guelph, Windsor, Chatham, Leamington, Sudbury, Oshawa, Whitby, Barrie, St. Catharines, Niagara Falls, Welland, Kingston, Belleville, Thunder Bay, Brantford, Paris, Brant, Peterborough, Kawartha Lakes, Lindsay, Sault Ste. Marie, Sarnia, Petrolia, Owen Sound, Pembroke and Petawawa.</div>
                </div>





            </div>
        </div>
    </section>
    <section class="questions faq_quest">
        <div class="container">
            <div class="questions_cont">
                <h3>Do you have any Questions ?</h3>
                <a href="/contactUs" class="cont_us">Contact US</a>
            </div>
        </div>
    </section>



@endsection