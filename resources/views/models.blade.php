@extends('layouts.bootstrap')

@section('content')
               @foreach ($models as $model)
                   <a href="/model/{{$model->id}}">
                   <div class="panel panel-default" style=" width: 200px; display: inline-block;">

                       <div class="panel-heading">{{$model->ModelName}}</div>
                       <div class="panel-body"> <img src="/{{$model->ModelImage}}" style="width:100%"></div>
                   </div>
                   </a>
               @endforeach
@endsection