@extends('layouts.app')

@section('mainContent')


    <div class="history_directory faq_directory">
        <div class="container">
            <a href="/">Home</a> /
            <span>About US</span>
        </div>
    </div>

    <section class="about_bn">
        <div class="container">
            <div class="ab_l">
                <img src="img/about_gn.png" alt="">
            </div>
            <div class="ab_r">
                <h2>About <br> US</h2>
            </div>
            <div class="clearfix"></div>
        </div>
    </section>

    <section class="privacy">
        <div class="container">
            <div class="privacy_cont">
                <div class="privacy_l">
                    <ul>
                        <li><a href='#eb1'>Our Mission</a></li>
                        <li><a href='#eb2'>Why Cash Your Tronics</a></li>
                        <li><a href='#eb3'>About Us</a></li>
                    </ul>
                </div>
                <div class="privacy_r">
                    <div class="privacy_item">
                        <h3 id='eb1'>Our Mission - </h3>
                        <p>Give you the most money for your unwanted electronics.  That’s it.</p>
                    </div>
                    <div class="privacy_item">
                        <h3 id='eb2'>Why Cash Your Tronics </h3>
                        <p>We know we are not the only ones you can sell your unwanted items to, but we like to think we are the best ones (or at least that’s what our moms tell us.)</p>
                        <p>Out platform is easy to use and most of all, we will give you the best price for your unwanted items.  Again, its that simple.  Find your item, get a quote, send it your stuff and get your cash.  Our belief is that you should get a fair price for your items is, so if you think your instant quote looks low, just contact us.  One of our employees will work with you to make sure you are getting a fair price.</p>
                    </div>

                    <div class="privacy_item">
                        <h3 id='eb3'>About US</h3>
                        <p>What started as a desire to solve a problem, has now turned into a dream job for the founders of Cash  Your  Tronics.    After  attempting  to  sell  back  an  older  IPHONE  4  and  a  DSLR  camera  on one of the buy back sites, it quickly became apparent that the prices were UNFAIR.  That’s when we  decided  enough  was  enough  and  we  can  offer  consumers  more money  for  their  unwanted items, through a simple interface and that’s how Cash Your Tronics was born.  We have stuck to our mission ever since, give you the most money for you unwanted electronics.  That’s it.  </p>
                    </div>
                </div>

            </div>
        </div>
    </section>


@endsection