@extends('layouts.app')

@section('mainContent')




    <div class="history_directory faq_directory">
        <div class="container">
            <a href="#">Home</a> /
            <a href="#">Categories</a> /
            <a href="#">Phones</a> /
            <a href="#">iPhone</a> /
            <span>iPhone 7 Plus</span>
        </div>
    </div>

    <section class="thn_text">
        <div class="container">
            <h2>Thank you !</h2>
            <p>We’ll sen a confirmation email with tracking info, a packing slip, checklist and details about your sale
                to sample@gmail.com</p>
        </div>
    </section>
    <section class="happens">
        <div class="container">
            <h3>What Happens Next?</h3>
            <div class="happens_cont">
                <div class="happens_item">
                    <span>1.</span><img src="img/happ1.svg" alt="">
                    <h2>Prep Your Phone</h2>
                    <ul>
                        <li>Turn off Find my Phone</li>
                        <li>Deactivate your service</li>
                    </ul>
                </div>
                <div class="happens_item">
                    <span>2.</span><img src="img/happ2.svg" alt="">
                    <h2>Pack & Send</h2>
                    <ul>
                        <li>Check email for prepaid shipping label</li>
                        <li>Pack your Device in box</li>
                        <li>Drag it off at your nearest Fedex location</li>
                    </ul>
                </div>
                <div class="happens_item">
                    <span>3.</span><img src="img/happ3.svg" alt="">
                    <h2>Get Paid </h2>
                    <ul>
                        <li>Once we receive an item it typicaly takes about a week for your PayPal funds to be
                            processed
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <div class="happens_inform">
        <div class="container">
            <div class="hap_inf_cont">
                <h2>Prep Your Phone - important steps to get full value for your device</h2>
                <p>Make sure to complete these steps to avoid reduced or delayed payment</p>
            </div>
        </div>
    </div>
    <section class="happens big_marg">
        <div class="container">
            <div class="happens_cont">
                <div class="happens_item">
                    <span>1.</span>
                    <h2>Turn off Find my iPhone.</h2>
                    <p>Leaving this on will lock your device and/or reduced payment. </p>
                    <a href="#" data-fancybox data-src="#zero">How to turn off</a>
                </div>
                <div class="happens_item">
                    <span>2.</span>
                    <h2>Deactivate your service</h2>
                    <p>It is very important that you contact your carrier to terminate service to this phone, and pay
                        any remaining balance</p>
                    <a href="#" data-fancybox data-src="#zero">How to deactivate service</a>
                </div>
                <div class="happens_item">
                    <span>3.</span>
                    <h2>Remove data</h2>
                    <p>We suggest that you erase all data from your phone. Also, if your phone hes a SIM card, remember
                        to remove it.</p>
                    <a href="#" data-fancybox data-src="#zero">How to erase data from your phone</a>
                </div>
                <div class="happens_item">
                    <span>4.</span>
                    <h2>For Android Phones - Delete your Google account! </h2>
                    <ul>
                        <li>From you home screen, find and open the Setting menu.</li>
                        <li>Under Accounts find and tap Google</li>
                        <li>Tap More or the Menu icon at the top right corner of the screen</li>
                        <li>From you home screen, find and open the Setting menu.</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>


    <div class="hidden">
        <div class="zero" id='zero'>
            <h2>ZERO Scratches and /or flaws</h2>
            <p>Developed by the Intel Corporation, HDCP stands for high-bandwidth digital content protection. As the
                descriptive name implies, HDCP is all about protecting the integrity of various audio and video content
                as it travels over a multiplicity of different types of interfaces. Such data interfaces as GVIF, DVI,
                and HDMI will all support the functionality of HDCP.</p>
            <p>Is HDCP Free?</p>
            <p>No. HDCP requires an authorized license. The license can be obtained through Digital Content Protection,
                which is a subsidiary of Intel Corporation. Generally, the license can be obtained by filing an
                application and paying an annual fee. Once the application is accepted and the user agrees to the terms
                found in the licensing agreement, the right to make use of HDCP is granted.</p>
            <p>What Are Some Of The Key Terms of Use For HDCP?</p>
            <p>One key term has to do with the transmission of data to unauthorized receivers. That is, an HDCP
                protected video source is not allowed to transmit protected content to any receiver that has not be
                verified to be HDCP compliant. There is also a restriction on the quality of the content, making sure
                that the DVD-audio content is equal to or less than CD-audio quality on any non-HDCP digital audio
                outputs. The licensed operator also covenants to not use their equipment to produce copies of content,
                and also to make sure that original content is created within the confines of current content protection
                requirements. What Type of Devices Make Use of HDCP?</p>
            <p>There are a number of different devices that make use of HDCP. DVD players are a common example,
                including systems that support high-definition DVD components. Blu-Ray discs are another example.
                Generally, it is possible for the manufacturer of the device to set what is known as an Image Constraint
                Token that will structure the type of output signal that can be read and translated. How Widespread Is
                The Use of HDCP?</p>
            <p>Since 2004, HDCP has been widely used in the United States. Elsewhere, HDCP has also gained a firm hold.
                The European Industry Association for Information Systems has deemed HDCP as a required component on all
                HD ready European devices. The latest Microsoft operating system, Windows Vista, makes use of HDCP as
                part of the function of graphics cards and monitors.</p>
        </div>
    </div>



@section