@extends('layouts.app')

@section('mainContent')

    <div class="history_directory faq_directory">
        <div class="container">
            <a href="#">Home</a> /
            <span>Privacy&Policy</span>
        </div>
    </div>


    <section class="privacy">
        <div class="container">
            <h3>Privacy & Policy</h3>

            <div class="privacy_cont">
                <div class="privacy_l">
                    <ul>
                        <li><a href='#pr1'>Collection of Information</a></li>
                        <li><a href='#pr2'>Non Personally Identifiable Information (Cookies)</a></li>
                        <li><a href='#pr3'>Personally Identifiable Information</a></li>
                        <li><a href='#pr4'>Use of Personally Identifiable Information</a></li>
                        <li><a href='#pr5'>International Users</a></li>
                        <li><a href='#pr6'>Third Party Sites</a></li>
                        <li><a href='#pr7'>Storage of Information</a></li>
                        <li><a href='#pr8'>Acceptance of and Changes to this Privacy Policy</a></li>
                    </ul>
                </div>
                <div class="privacy_r">
                    <div class="privacy_item">
                        <h3 id='pr1'>Collection of Information:</h3>
                        <h3 id='pr2'>Non Personally Identifiable Information (Cookies):</h3>
                        <p>Cashyourtronics uses cookies solely for internal recordkeeping, external statistics about our users as a whole and aggregating data about the traffic on the Site. If you would prefer not to have cookies on your computer, you can block by selecting that option on your computer and/or internet browser. The Site gathers IP addresses from all visitors to the Site and may use this information in a variety of ways including, but not limited to, research purposes.</p>
                    </div>
                    <div class="privacy_item">
                        <h3 id='pr3'>Personally Identifiable Information:</h3>
                        <p>When you sign up on the Site to receive email announcements,  send an email to Cashyourtronics and/or register as a member, Cashyourtronics may require that you provide certain personally identifiable information which may include, but is not necessarily limited to:</p>
                        <ul>
                            <li>First and last name;</li>
                            <li>physical address; </li>
                            <li>zip code; </li>
                            <li>and email address.</li>
                        </ul>
                        <p>Cashyourtronics may also request other personally identifiable information, which you may choose whether or not to provide which is treated in the same manner as all other personally identifiable information under this privacy policy.</p>
                        <p>We do not knowingly collect any personally identifiable information from anyone under the age of 13 on the Site. If we are informed that personally identifiable information has been collected from a person under the age of 13, we will immediately remove the personally identifiable information that may have been inadvertently collected by Cashyourtronics.com. If a legal guardian of a minor under the age of 18 requests the removal of personally identifiable information collected from that minor, we will immediately remove said information.</p>
                    </div>
                    <div class="privacy_item">
                        <h3 id='pr4'>Use of Personally Identifiable Information:</h3>
                        <p>Cashyourtronics will use the personally identifiable information that it collects about you to contact you in response to consumer-initiated inquiries, to provide the services to you that you request, to alert you to changes in services or features, and to provide you with requested information regarding Cashyourtronics or our services. You will also receive emails regarding special offers, updates, etc. if you have checked the box during your registration indicating that you would like to receive this information. Cashyourtronics reserves the right to release current or past user information:</p>
                        <ul>
                            <li>In the event that we believe that the Site is being or has been used to commit unlawful acts; </li>
                            <li>Upon receipt of a properly authorized and authenticated governmental request for information; </li>
                            <li>In response to a subpoena or a court order;</li>
                            <li>To comply with relevant laws;</li>
                            <li>In response to an investigation of fraud or other wrongdoing regarding a specific consumer;</li>
                            <li>In an effort to safeguard the person or property of a Cashyourtronics employee or a third party;</li>
                            <li>If Cashyourtronics is sold or acquired.</li>
                        </ul>
                        <p>Moreover, you hereby consent to disclosure of any record or communication to any third party when Cashyourtronics, in its sole discretion, determines the disclosure to be appropriate including, without limitation, sharing your e-mail address with other third parties for suppression purposes in compliance with the CAN-SPAM Act of 2003, as amended from time to time. If you no longer wish to receive emails from us which you have not specifically initiated, you may opt-out of receiving these communications by following the instructions contained in the applicable e-mail. Cashyourtronics processes all unsubscribe requests in a prompt fashion. </p>
                    </div>
                    <div class="privacy_item">
                        <h3 id='pr5'>International Users:</h3>
                        <p>By using the Site and providing Cashyourtronics with data, you understand and agree that due to the international dimension of the Internet, Cashyourtronics may use the data collected in the course of our relationship as set forth in this Privacy Policy or in other communications with you, including the transmission of information outside your resident jurisdiction. In addition, you acknowledge that your personally identifiable information and other data may be stored on servers located in the United States or in other international locations. By providing us with your information, you consent to the transfer and storage of such information.</p>
                    </div>
                    <div class="privacy_item">
                        <h3 id='pr6'>Third Party Sites:</h3>
                        <p>Cashyourtronics may direct you to third party websites. Those websites are not affiliated with Cashyourtronics and their privacy policies may differ from Cashyourtronics’s privacy policy. Cashyourtronics is in no way responsible for the privacy policies or treatment of information from third party websites and strongly encourages you to review the privacy policies of every website you visit to determine what information they collect and how such information is handled by that website. We may use third-party advertising companies to serve ads when you visit our website. These companies may use information (not including your name, address, email address, or telephone number) about your visits to this and other websites in order to provide advertisements about goods and services of interest to you. If you would like more information about this practice and to know your choices about not having this information used by these companies. </p>
                    </div>
                    <div class="privacy_item">
                        <h3 id='pr7'>Storage of Information:</h3>
                        <p>We are serious about the security of your personal information. We limit access to your personally identifiable information to those individuals (i.e., our employees and agents) who we reasonably believe need to have access to that information to provide products or services to you or in order to do their jobs. Your personally identifiable information is stored on our servers located in the United States. Cashyourtronics treats your data as an asset that must be protected and use many tools (encryption, passwords, physical security, etc.) to protect your personal information against unauthorized access and disclosure. Although Cashyourtronics does not release your personally identifiable information to the public and Cashyourtronics makes a reasonable good faith effort to store your personally identifiable information in a secure operating environment, we cannot guarantee 100% security of such information due to factors beyond our control. Byback Inc.  may take reasonable steps to confirm your identity prior to supplying any personally identifiable information to you in order to prevent identity theft. </p>
                    </div>
                    <div class="privacy_item">
                        <h3 id='pr8'>Acceptance of and Changes to this Privacy Policy:</h3>
                        <p>We are serious about the security of your personal information. We limit access to your personally identifiable information to those individuals (i.e., our employees and agents) who we reasonably believe need to have access to that information to provide products or services to you or in order to do their jobs. Your personally identifiable information is stored on our servers located in the United States. Cashyourtronics treats your data as an asset that must be protected and use many tools (encryption, passwords, physical security, etc.) to protect your personal information against unauthorized access and disclosure. Although Cashyourtronics does not release your personally identifiable information to the public and Cashyourtronics makes a reasonable good faith effort to store your personally identifiable information in a secure operating environment, we cannot guarantee 100% security of such information due to factors beyond our control. Byback Inc.  may take reasonable steps to confirm your identity prior to supplying any personally identifiable information to you in order to prevent identity theft. </p>
                    </div>
                </div>

            </div>

        </div>
    </section>



@endsection