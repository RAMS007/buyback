@extends('layouts.app')

@section('mainContent')





    <div class="bn_cont">
        <div class="container">
            <h2>I want to <br> Sell:</h2>
            <div class="bn_tov"><img src="img/bn_tov.png" alt=""></div>
        </div>
    </div>

    <section class="home_categ">
        <div class="container">
            <div class="home_categ_cont">
                @foreach ($categories as $category)
                    <a href='/device/{{$category->id}}' style='background-image: url({{$category->DeviceImage}});' class="home_categ_item">
                        <!-- <p></p> -->
                        <h3>{{$category->DeviceName}}</h3>
                    </a>
                @endforeach
            </div>
        </div>
    </section>
    <section class="works">
        <div class="container">
            <h2>How it works</h2>
            <div class="works_cont">
                <div class="works_item">
                    <h3>GET AN OFFER</h3>
                    <p>It takes less than a minute.</p>
                </div>
                <div class="works_item">
                    <h3>SHIP IT FREE</h3>
                    <p>It's free for items worth $1 or more.</p>
                </div>
                <div class="works_item">
                    <h3>GET PAID FAST</h3>
                    <p>By Amazon Gift Card, PayPal or check.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="customer">
        <div class="container">
            <h2>Customer Stories</h2>
        </div>

        <div class="center">

         <!--
            <div class="stories_cont">
                <div class="stories_item">
                    <div class="titr"></div>
                    <p>“I was struggling to get my course off the ground, but I discovered buyback and it was so easy to
                        set everything up, great support team, and overall an awesome experience.”</p>
                    <div class="autor">
                        <div class="autor_img">
                            <img src="img/autor-img.png" alt="">
                        </div>
                        <h3>Leona Cole</h3>
                        <h6>Director M&S</h6>
                    </div>
                </div>
            </div>
            -->

            @foreach ($allStories as $story)
                <div class="stories_cont">
                    <div class="stories_item">
                        <div class="titr"></div>
                        <p>{{$story->CustomerText}}</p>
                        <div class="autor">
                            <div class="autor_img">
                                <img src="/{{$story->CustomerImage}}" alt="">
                            </div>
                            <h3>{{$story->CustomerName}}</h3>
                            <h6>{{$story->CustomerPosition}}</h6>
                        </div>
                    </div>
                </div>

            @endforeach


        </div>
    </section>
    <section class="inst">
        <div class="container">
            <div class="inst_cont">
                <img src="img/inst1.png" alt="">
                <img src="img/inst2.png" alt="">
                <img src="img/inst3.png" alt="">
                <img src="img/inst4.png" alt="">
                <img src="img/inst5.png" alt="">
                <img src="img/inst6.png" alt="">
                <img src="img/inst7.png" alt="">
                <img src="img/inst8.png" alt="">
            </div>
            <a href='' class="inst_fl"><i class="fa fa-instagram" aria-hidden="true"></i>Chek our Instagram</a>
        </div>
    </section>
    <section class="questions">
        <div class="container">
            <div class="questions_cont">
                <h3>Do you have any Questions ?</h3>
                <a href="/contactUs" class="cont_us">Contact US</a>
            </div>
        </div>
    </section>



@endsection