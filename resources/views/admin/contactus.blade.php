@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">ContactUs</h1>
            </div>
            <div class="btn-group btn-group-justified">
                <a href="/admin/requests" class="btn btn-primary">All</a>
                <a href="/admin/requests/new" class="btn btn-primary">New Only</a>
                <a href="/admin/requests/readed" class="btn btn-primary">Readed only</a>
                <a href="/admin/requests/closed" class="btn btn-primary">Closed only</a>
            </div>


            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        Contact Us
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Text</th>
                            <th>status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach ( $requests as $Request)


                        <tr>
                            <td>
                                {{$Request->id}}
                            </td>

                            <td>
                               {{$Request->userEmail}}
                            </td>
                            <td>{{ $Request->userPhone    }}</td>
                            <td>{{  $Request->message }}</td>
                            <td>{{  $Request->status }}</td>

                            <td>
                                <a href="#" data-toggle="modal" data-target="#ModalRequest" class="OpenRequest"  data-id="{{$Request->id}}" data-email="{{$Request->userEmail}}"
                                data-phone="{{ $Request->userPhone    }}" data-message="{{  $Request->message }}" data-status="{{  $Request->status }}"
                                >
                                    <span class="fa fa-eye"></span>
                                </a>


                                <a href="/admin/request/delete/{{$Request->id}}" class="delete_action" msg="Are you sure you want to delete this device and all corresponding models?">
                                    <span class="fa fa-trash"></span>
                                </a>
                            </td>
                        </tr>
                    @endforeach


                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->




    <!-- Modal -->
    <div class="modal fade" id="ModalRequest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">UserRequest</h4>
                </div>
                <form action="/admin/requests/save" method="POST" >
                    {{ csrf_field() }}

                    <div class="modal-body">
                        <input type="hidden" name="requestId" id="requestId" >
                        <div class="form-group">
                            <label for="device_title" class="form-label">id</label>
                            <input type="text" class="form-control" name="request_id" id="request_id" disabled >
                        </div>

                        <div class="form-group">
                            <label for="device_title" class="form-label">Email</label>
                            <input type="text" class="form-control" name="request_email" id="request_email" disabled >
                        </div>

                        <div class="form-group">
                            <label for="device_title" class="form-label">Phone</label>
                            <input type="text" class="form-control" name="request_phone" id="request_phone" disabled >
                        </div>

                        <div class="form-group">
                            <label for="device_title" class="form-label">Message</label>
                            <textarea class="form-control" name="request_mesage" id="request_message" ></textarea>
                        </div>

                        <div class="form-group">
                            <label for="device_price" class="form-label">Status</label>
                            <select  class="form-control" name="status" >
                                <option value="new"> new </option>
                                <option value="readed"> readed </option>
                                <option value="closed"> closed </option>
                            </select>


                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /Modal -->







@endsection


@section('scripts')

    <script>

        $(document).on("click", ".OpenRequest", function () {

            $(".modal-body #request_id").val( $(this).data('id') );
            $(".modal-body #requestId").val( $(this).data('id') );

            $(".modal-body #request_email").val( $(this).data('email') );
            $(".modal-body #request_phone").val( $(this).data('phone') );
            $(".modal-body #request_phone").val( $(this).data('phone') );
            $(".modal-body #request_message").val( $(this).data('message') );
            $(".modal-body #request_status").val( $(this).data('status') );


        });


    </script>
@endsection

