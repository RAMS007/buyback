@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">carriers</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="row">

            <br>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            <form action="/admin/carrier/update/{{$carrier->id}}" method="POST">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="carrier_name" class="form-label">carrier</label>

                        <select name="carrier_name" value="{{$carrier->CarrierName}}">
                            <option value="AT&T"> AT&T</option>
                            <option value="Sprint"> Sprint</option>
                            <option value="T-Mobile"> T-Mobile</option>
                            <option value="Verizon"> Verizon</option>
                            <option value="Metro PCS"> Metro PCS</option>
                            <option value="Factory Unlocked"> Factory Unlocked </option>
                            <option value="Boost Mobile"> Boost Mobile</option>
                            <option value="Virgin Mobile"> Virgin Mobile</option>
                            <option value="Straight Talk"> Straight Talk</option>
                            <option value="U.S. Cellular"> U.S. Cellular</option>
                            <option value="Cricket"> Cricket</option>
                            <option value="Other Carriers"> Other Carriers</option>
                        </select>



                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>




        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->






@endsection
