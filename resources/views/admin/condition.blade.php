@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Conditions</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="row">

            <br>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            <form action="/admin/condition/update/{{$condition->id}}" method="POST">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="condition_name" class="form-label">Condition</label>
                        <input type="text" class="form-control" placeholder="Like New, Poor, etc." name="condition_name" value="{{$condition->ConditionName}}">
                    </div>


                    <div class="form-group">
                        <label for="condition_description1" class="form-label">description1</label>
                        <textarea  class="form-control" placeholder="Brand new in the original factory sealed box. All plastics must still be intact." name="condition_description1">{{$condition->condition_description1}}</textarea>
                    </div>


                    <div class="form-group">
                        <label for="condition_description2" class="form-label">description2</label>
                        <textarea  class="form-control" placeholder="ZERO Scratches and/or flaws. Phone must never have been taken out of the box." name="condition_description2">{{$condition->condition_description2}}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="condition_description3" class="form-label">description3</label>
                        <textarea  class="form-control" placeholder="Perfect display with no dust under the screen." name="condition_description3">{{$condition->condition_description3}}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="condition_description4" class="form-label">description4</label>
                        <textarea  class="form-control" placeholder="No defects or cosmetic wear whatsoever" name="condition_description4">{{$condition->condition_description4}}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="condition_description5" class="form-label">description5</label>
                        <textarea  class="form-control" placeholder="Clear ESN/IMEI for activation by a new user" name="condition_description5">{{$condition->condition_description5}}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="condition_description6" class="form-label">description6</label>
                        <textarea  class="form-control" placeholder="Please Note: Any device found to be reported as lost or stolen is not eligible for trade-in" name="condition_description6">{{$condition->condition_description6}}</textarea>
                    </div>









                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>




        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->






@endsection
