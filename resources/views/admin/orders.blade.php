@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Orders</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        Orders
                        <a href="#" class="pull-right" data-target="#addModelModal" data-toggle="modal">
                            <span class="fa fa-plus-circle"></span>
                        </a>
                    </div>
                </div>

                <div >

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif



                </div>
            </div>

        </div>

        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->





@endsection
