@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">harddrives</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="row">
            <br>
            <!-- 1st Column -->
            <div class="col-md-4 col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            harddrives
                            <a href="#" class="pull-right" data-toggle="modal" data-target="#addDeviceharddrive">
                                <span class="fa fa-plus-circle"></span>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>harddrive</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($harddrives as $harddrive)
                                <tr>
                                    <td>{{$harddrive->HardDriveName}}</td>
                                    <td>
                                        <a href="/admin/harddrive/update/{{$harddrive->id}}">
                                            <span class="fa fa-pencil"></span>
                                        </a>
                                        <a href="/admin/harddrive/delete/{{$harddrive->id}}" class="delete_action" msg="Are you sure you want to delete all entries of this harddrive?">
                                            <span class="fa fa-trash"></span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <!-- 1st Column -->
            <!-- 2nd Column -->
            <div class="col-md-8 col-sm-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            harddrives for Devices
                            <a href="#" class="pull-right" data-toggle="modal" data-target="#addharddrivesToDeviceModal">
                                <span class="fa fa-plus-circle"></span>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Device</th>
                                <th>harddrive</th>
                                <th>Price </th>

                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($devices2harddrive as $item)
                                <tr>
                                    <td>{{$item->ModelName}}</td>
                                    <td>{{$item->HardDriveName}}</td>
                                    <td>{{$item->HardDrivePrice}}</td>

                                    <td>
                                        <a href="/admin/model2harddrive/update/{{$item->id}}">
                                            <span class="fa fa-pencil"></span>
                                        </a>
                                        <a href="/admin/model2harddrive/delete/{{$item->id}}" class="delete_action" msg="Are you sure you want to delete this harddrive?">
                                            <span class="fa fa-trash"></span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- 2nd Column -->

        </div>




        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->




    <!-- addDeviceharddrive -->
    <div class="modal fade" id="addDeviceharddrive" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add a harddrive</h4>
                </div>
                <form action="/admin/harddrives/add" method="POST">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="harddrive_name" class="form-label">harddrive</label>
                            <input type="text" class="form-control" placeholder="Like New, Poor, etc." name="harddrive_name">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /addDeviceharddrive -->

    <!-- addharddrivesToDeviceModal -->
    <div class="modal fade" id="addharddrivesToDeviceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Different harddrives for each Model</h4>
                </div>
                <form action="/admin/model2harddrives/add" method="POST">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="model_id" class="form-label">Model</label>
                            <select name="model_id" id="model_id" class="form-control">

                                @foreach($models as $model)
                                    <option value="{{$model->id}}">{{$model->ModelName}}</option>

                                @endforeach


                            </select>
                        </div>

                        <div class="form-group">
                            <label for="harddrive" class="form-label">harddrive</label>
                            <select name="harddrive_id" id="harddrive" class="form-control">
                                @foreach($harddrives as $harddrive)
                                    <option value="{{$harddrive->id}}">{{$harddrive->HardDriveName}}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="form-group">
                            <label for="price_percentage" class="form-label">Price </label>
                            <input type="text" class="form-control" name="price" placeholder="Add or subtract a percentage of price.">
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /addharddrivesToDeviceModal -->





@endsection
