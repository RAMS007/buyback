@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">accesories</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="row">
            <br>
            <!-- 1st Column -->
            <div class="col-md-4 col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            accesories
                            <a href="#" class="pull-right" data-toggle="modal" data-target="#addDeviceaccesorie">
                                <span class="fa fa-plus-circle"></span>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>accesorie</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($accesories as $accesorie)
                            <tr>
                                <td>{{$accesorie->AccesoriesName}}</td>
                                <td>
                                    <a href="/admin/accesorie/update/{{$accesorie->id}}">
                                        <span class="fa fa-pencil"></span>
                                    </a>
                                    <a href="/admin/accesorie/delete/{{$accesorie->id}}" class="delete_action" msg="Are you sure you want to delete all entries of this accesorie?">
                                        <span class="fa fa-trash"></span>
                                    </a>
                                </td>
                            </tr>
                          @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <!-- 1st Column -->
            <!-- 2nd Column -->
            <div class="col-md-8 col-sm-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            accesories for Devices
                            <a href="#" class="pull-right" data-toggle="modal" data-target="#addaccesoriesToDeviceModal">
                                <span class="fa fa-plus-circle"></span>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Device</th>
                                <th>accesorie</th>
                                <th>Price </th>

                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($devices2accesorie as $item)
                            <tr>
                                <td>{{$item->ModelName}}</td>
                                <td>{{$item->AccesoriesName}}</td>
                                <td>{{$item->AccesoriePrice}}</td>

                                <td>
                                    <a href="/admin/model2accesorie/update/{{$item->id}}">
                                        <span class="fa fa-pencil"></span>
                                    </a>
                                    <a href="/admin/model2accesorie/delete/{{$item->id}}" class="delete_action" msg="Are you sure you want to delete this accesorie?">
                                        <span class="fa fa-trash"></span>
                                    </a>
                                </td>
                            </tr>
                           @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- 2nd Column -->

        </div>




        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->




    <!-- addDeviceaccesorie -->
    <div class="modal fade" id="addDeviceaccesorie" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add a accesorie</h4>
                </div>
                <form action="/admin/accesories/add" method="POST">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="accesorie_name" class="form-label">accesorie</label>
                            <input type="text" class="form-control" placeholder="Like New, Poor, etc." name="accesorie_name">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /addDeviceaccesorie -->

    <!-- addaccesoriesToDeviceModal -->
    <div class="modal fade" id="addaccesoriesToDeviceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Different accesories for each Model</h4>
                </div>
                <form action="/admin/model2accesories/add" method="POST">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="model_id" class="form-label">Model</label>
                            <select name="model_id" id="model_id" class="form-control">

                                @foreach($models as $model)
                                    <option value="{{$model->id}}">{{$model->ModelName}}</option>

                                @endforeach


                            </select>
                        </div>

                        <div class="form-group">
                            <label for="accesorie" class="form-label">accesorie</label>
                            <select name="accesorie_id" id="accesorie" class="form-control">
                                @foreach($accesories as $accesorie)
                                <option value="{{$accesorie->id}}">{{$accesorie->AccesoriesName}}</option>
                        @endforeach
                            </select>
                        </div>


                        <div class="form-group">
                            <label for="price_percentage" class="form-label">Price </label>
                            <input type="text" class="form-control" name="price" placeholder="Add or subtract a percentage of price.">
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /addaccesoriesToDeviceModal -->





@endsection
