@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Colors</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="row">
            <br>

            <form action="/admin/model2color/update/{{$Model2Colors->id}}" method="POST">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="model" class="form-label">Models</label>
                        <select name="model_id" id="device" class="form-control">

                            @foreach($models as $model)
                                <option value="{{$model->id}}"

                                        @if($model->id == $Model2Colors->ModelId)
                                        selected

                                        @endif

                                >{{$model->ModelName}}</option>

                            @endforeach


                        </select>
                    </div>

                    <div class="form-group">
                        <label for="color_id" class="form-label">Colors</label>
                        <select name="color_id" id="color_id" class="form-control">
                            @foreach($colors as $color)
                                <option value="{{$color->id}}"

                                        @if($color->id == $Model2Colors->ColorId)
                                        selected

                                        @endif

                                >{{$color->ColorName}}</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group">
                        <label for="price_percentage" class="form-label">Price </label>
                        <input type="text" class="form-control" name="price" placeholder="Add or subtract a percentage of price." value="{{$Model2Colors->ColorPrice}}">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>


        </div>




        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->








@endsection
