@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">carriers</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="row">
            <br>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form action="/admin/model2carrier/update/{{$Model2carrier->id}}" method="POST">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="model_id" class="form-label">Model</label>
                        <select name="model_id" id="model_id" class="form-control">

                            @foreach($models as $model)
                                <option value="{{$model->id}}"

                                        @if($model->id == $Model2carrier->ModelId)
                                        selected

                                        @endif


                                >{{$model->ModelName}}</option>

                            @endforeach


                        </select>
                    </div>

                    <div class="form-group">
                        <label for="carrier" class="form-label">carrier</label>
                        <select name="carrier_id" id="carrier" class="form-control">
                            @foreach($carriers as $carrier)
                                <option value="{{$carrier->id}}"


                                        @if($carrier->id == $Model2carrier->CarrierId)
                                        selected

                                        @endif


                                >{{$carrier->CarrierName}}</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group">
                        <label for="price_percentage" class="form-label">Price </label>
                        <input type="text" class="form-control" name="price" placeholder="Add or subtract a percentage of price." value="{{$Model2carrier->CarrierPrice}}">
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>



        </div>




        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->










@endsection
