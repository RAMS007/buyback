@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Storages</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="row">
            <br>
            <!-- 1st Column -->
            <div class="col-md-4 col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            storages
                            <a href="#" class="pull-right" data-toggle="modal" data-target="#addDevicestorage">
                                <span class="fa fa-plus-circle"></span>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>storage</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($storages as $storage)
                                <tr>
                                    <td>{{$storage->StorageName}}</td>
                                    <td>
                                        <a href="/admin/storage/update/{{$storage->id}}">
                                            <span class="fa fa-pencil"></span>
                                        </a>
                                        <a href="/admin/storage/delete/{{$storage->id}}" class="delete_action" msg="Are you sure you want to delete all entries of this storage?">
                                            <span class="fa fa-trash"></span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <!-- 1st Column -->
            <!-- 2nd Column -->
            <div class="col-md-8 col-sm-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            storages for models
                            <a href="#" class="pull-right" data-toggle="modal" data-target="#addstoragesToDeviceModal">
                                <span class="fa fa-plus-circle"></span>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Model</th>
                                <th>Storage</th>
                                <th>Price </th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($devices2storage as $item)
                                <tr>
                                    <td>{{$item->ModelName}}</td>
                                    <td>{{$item->StorageName}}</td>
                                    <td>{{$item->StoragePrice}}</td>

                                    <td>
                                        <a href="/admin/model2storage/update/{{$item->id}}">
                                            <span class="fa fa-pencil"></span>
                                        </a>
                                        <a href="/admin/model2storage/delete/{{$item->id}}" class="delete_action" msg="Are you sure you want to delete this storage?">
                                            <span class="fa fa-trash"></span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- 2nd Column -->

        </div>




        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->




    <!-- addDevicestorage -->
    <div class="modal fade" id="addDevicestorage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add a storage</h4>
                </div>
                <form action="/admin/storages/add" method="POST">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="storage_name" class="form-label">storage</label>
                            <input type="text" class="form-control" placeholder="Like New, Poor, etc." name="storage_name">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /addDevicestorage -->

    <!-- addstoragesToDeviceModal -->
    <div class="modal fade" id="addstoragesToDeviceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Different storages for each Model</h4>
                </div>
                <form action="/admin/model2storage/add" method="POST">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="device" class="form-label">Device</label>
                            <select name="model_id" id="device" class="form-control">

                                @foreach($models as $model)
                                    <option value="{{$model->id}}">{{$model->ModelName}}</option>

                                @endforeach


                            </select>
                        </div>

                        <div class="form-group">
                            <label for="storage" class="form-label">storage</label>
                            <select name="storage_id" id="storage" class="form-control">
                                @foreach($storages as $storage)
                                    <option value="{{$storage->id}}">{{$storage->StorageName}}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="form-group">
                            <label for="price_percentage" class="form-label">Price </label>
                            <input type="text" class="form-control" name="price" placeholder="Add or subtract a percentage of price.">
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /addstoragesToDeviceModal -->





@endsection
