@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Models</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        Device Models
                        <a href="#" class="pull-right" data-target="#addModelModal" data-toggle="modal">
                            <span class="fa fa-plus-circle"></span>
                        </a>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Model icon</th>
                            <th>Title</th>
                            <th>Device</th>
                            <th>Price</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($models as $model)
                            <tr>
                                <td>
                                    <img src="/{{$model->ModelImage}}" alt="" width="128" height="128">
                                </td>
                                <td>{{$model->ModelName}}</td>
                                <td>{{$model->DeviceName}}</td>
                                <td>{{$model->ModelPrice}}</td>
                                <td>
                                    <a href="/admin/model/update/{{$model->id}}">
                                        <span class="fa fa-pencil"></span>
                                    </a>
                                    <a href="/admin/model/delete/{{$model->id}}" class="delete_action"
                                       msg="Are you sure you want to delete this Device Model? WARNING: Deleting this model will result in the deletion of all the child entries in all categories.">
                                        <span class="fa fa-trash"></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>

        </div>

        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->


    <!-- Modal -->
    <div class="modal fade" id="addModelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Model</h4>
                </div>
                <form action="/admin/models/add" method="POST" enctype="multipart/form-data">
                    <div class="modal-body">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="device" class="form-label">Select Device</label>
                            <select name="device_id" id="device" class="form-control">

                                @foreach($devices as $device)
                                    <option value="{{$device->id}}">{{$device->DeviceName}}</option>

                                @endforeach


                            </select>
                        </div>

                        <div class="form-group">
                            <label for="model_name" class="form-label">Title</label>
                            <input type="text" class="form-control" name="model_name"
                                   placeholder="iPhone 4, iPad 2, etc.">
                        </div>

                        <div class="form-group">
                            <label for="model_price" class="form-label">Price</label>
                            <input type="text" class="form-control" name="model_price" placeholder="Price in $$$">
                        </div>

                        <div class="form-group">
                            <label for="model_img" class="form-label">Image</label>
                            <input name="model_img" type="file">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /Modal -->




@endsection
