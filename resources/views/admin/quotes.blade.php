@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">ContactUs</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        Contact Us

                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Id</th>

                            <th>Category</th>
                            <th>Brand</th>
                            <th>Model</th>
                            <th>Condition</th>
                            <th>Image</th>
                            <th>Quantity</th>
                            <th>Comment</th>
                            <th>Email</th>
                            <th>status</th>
                            <th>created at</th>
                            <th>Actions</th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach ( $quotes as $quote)

                            <tr>
                                <td>
                                    {{$quote->id}}
                                </td>

                                <td>
                                    {{$quote->category}}
                                </td>
                                <td>{{ $quote->ProductBrand    }}</td>
                                <td>{{  $quote->ProductBrand }}</td>

                                <td>{{  $quote->ProductCondition }}</td>
                                <td><img src="/{{$quote->ProductImage}}"></td>

                                <td>{{  $quote->ProductQuantity }}</td>

                                <td>{{  $quote->ProductComment }}</td>
                                <td>{{  $quote->UserEmail }}</td>


                                <td>{{  $quote->status }}</td>
                                <td>{{  $quote->created_at }}</td>


                                <td>
                                    <a>
                                        <span class="fa fa-pencil"></span>
                                    </a>
                                    <a class="delete_action"
                                       msg="Are you sure you want to delete this device and all corresponding models?">
                                        <span class="fa fa-trash"></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->




    <!-- Modal -->
    <div class="modal fade" id="addDeviceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add a Device</h4>
                </div>
                <form action="/admin/devices/add" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="device_title" class="form-label">Name</label>
                            <input type="text" class="form-control" name="device_name" placeholder="iPhone, iPad, etc.">
                        </div>

                        <div class="form-group">
                            <label for="device_price" class="form-label">Price</label>
                            <input type="text" class="form-control" name="device_price" placeholder="Price in $$$">
                        </div>

                        <div class="form-group">
                            <label for="device_img" class="form-label">Image</label>
                            <input name="device_img" type="file">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /Modal -->



@endsection
