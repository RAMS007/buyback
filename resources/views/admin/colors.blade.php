@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Colors</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="row">
            <br>
            <!-- 1st Column -->
            <div class="col-md-4 col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Colors
                            <a href="#" class="pull-right" data-toggle="modal" data-target="#addDeviceCondition">
                                <span class="fa fa-plus-circle"></span>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Colors</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($colors as $color)
                                <tr>
                                    <td>{{$color->ColorName}}</td>
                                    <td>
                                        <a href="/admin/color/update/{{$color->id}}">
                                            <span class="fa fa-pencil"></span>
                                        </a>
                                        <a href="/admin/color/delete/{{$color->id}}" class="delete_action" msg="Are you sure you want to delete all entries of this condition?">
                                            <span class="fa fa-trash"></span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <!-- 1st Column -->
            <!-- 2nd Column -->
            <div class="col-md-8 col-sm-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Colors for Models
                            <a href="#" class="pull-right" data-toggle="modal" data-target="#addConditionsToDeviceModal">
                                <span class="fa fa-plus-circle"></span>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Model</th>
                                <th>Color</th>
                                <th>Price </th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($model2color as $item)
                                <tr>
                                    <td>{{$item->ModelName}}</td>
                                    <td>{{$item->ColorName}}</td>
                                    <td>{{$item->ColorPrice}}</td>

                                    <td>
                                        <a href="/admin/model2color/update/{{$item->id}}">
                                            <span class="fa fa-pencil"></span>
                                        </a>
                                        <a href="/admin/model2color/delete/{{$item->id}}" class="delete_action" msg="Are you sure you want to delete this condition?">
                                            <span class="fa fa-trash"></span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- 2nd Column -->

        </div>




        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->




    <!-- addDeviceCondition -->
    <div class="modal fade" id="addDeviceCondition" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add a Color</h4>
                </div>
                <form action="/admin/color/add" method="POST">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="condition_name" class="form-label">Color</label>
                            <input type="text" class="form-control" placeholder="Like New, Poor, etc." name="color_name">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /addDeviceCondition -->

    <!-- addConditionsToDeviceModal -->
    <div class="modal fade" id="addConditionsToDeviceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Different Conditions for each Model</h4>
                </div>
                <form action="/admin/model2color/add" method="POST">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="model" class="form-label">Device</label>
                            <select name="model_id" id="device" class="form-control">

                                @foreach($models as $model)
                                    <option value="{{$model->id}}">{{$model->ModelName}}</option>

                                @endforeach


                            </select>
                        </div>

                        <div class="form-group">
                            <label for="color_id" class="form-label">Colors</label>
                            <select name="color_id" id="color_id" class="form-control">
                                @foreach($colors as $color)
                                    <option value="{{$color->id}}">{{$color->ColorName}}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="form-group">
                            <label for="price_percentage" class="form-label">Price </label>
                            <input type="text" class="form-control" name="price" placeholder="Add or subtract a percentage of price.">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /addConditionsToDeviceModal -->





@endsection
