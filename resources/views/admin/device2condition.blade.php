@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Conditions</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="row">
            <br>
            <form action="/admin/device2condition/update/{{$model->id}}" method="POST">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="device" class="form-label">Device</label>
                        <select name="device_id" id="device" class="form-control">
                            @foreach($devices as $device)
                                <option value="{{$device->id}}"

                                        @if($device->id == $model->DeviceId)
                                        selected

                                        @endif
                                >{{$device->DeviceName}}</option>
                            @endforeach

                        </select>
                    </div>

                    <div class="form-group">
                        <label for="condition" class="form-label">Condition</label>
                        <select name="condition_id" id="condition" class="form-control">
                            @foreach($conditions as $condition)
                                <option value="{{$condition->id}}"


                                        @if($condition->id == $model->ConditionId)
                                        selected

                                        @endif


                                >{{$condition->ConditionName}}</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group">
                        <label for="price_percentage" class="form-label">Price Percentage</label>
                        <input type="text" class="form-control" name="price_percentage" placeholder="Add or subtract a percentage of price." value="{{$model->ConditionPricePercentage}}">
                    </div>

                    <div class="form-group">
                        <label for="deduct" class="form-label">Operation</label>
                        <select name="deduct" id="deduct" class="form-control">
                            <option value="1"

                                    @if($model->deduct==1)
                                    selected

                                    @endif


                            >Add</option>
                            <option value="0"

                                    @if($model->deduct==0)
                                    selected

                                    @endif


                            >Deduct</option>
                        </select>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>

        </div>




        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->







@endsection
