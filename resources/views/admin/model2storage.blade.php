@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Storages</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="row">
            <br>


            <form action="/admin/model2storage/update/{{$Model2storages->id}}" method="POST">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="device" class="form-label">Device</label>
                        <select name="model_id" id="device" class="form-control">

                            @foreach($models as $model)
                                <option value="{{$model->id}}"
                                        @if($model->id == $Model2storages->ModelId)
                                        selected

                                        @endif

                                >{{$model->ModelName}}</option>

                            @endforeach


                        </select>
                    </div>

                    <div class="form-group">
                        <label for="storage" class="form-label">storage</label>
                        <select name="storage_id" id="storage" class="form-control">
                            @foreach($storages as $storage)
                                <option value="{{$storage->id}}"


                                        @if($storage->id == $Model2storages->StorageId)
                                        selected

                                        @endif



                                >{{$storage->StorageName}}</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group">
                        <label for="price_percentage" class="form-label">Price </label>
                        <input type="text" class="form-control" name="price" placeholder="Add or subtract a percentage of price." value="{{$Model2storages->StoragePrice}}">
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>




        </div>




        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->










@endsection
