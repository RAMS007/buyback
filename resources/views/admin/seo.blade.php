@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">SEO tags</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="row">
            <h1>SEO Meta Tags</h1>
            <form action="/admin/seo" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <label for="home" class="form-label">Home Page</label>
                    <textarea class="form-control" name="seo_home" id="home" cols="30" rows="10">{{$seo_home}}</textarea>
                </div>

                <div class="form-group">
                    <label for="sell" class="form-label">Sell Page</label>
                    <textarea class="form-control" name="seo_sell" id="sell" cols="30" rows="10">{{$seo_sell}}</textarea>
                </div>


                <div class="form-group">
                    <label for="register" class="form-label">Register Page</label>
                    <textarea class="form-control" name="seo_register" id="register" cols="30" rows="10">{{$seo_register}}</textarea>
                </div>

                <div class="form-group">
                    <label for="login" class="form-label">Login Page</label>
                    <textarea class="form-control" name="seo_login" id="login" cols="30" rows="10">{{$seo_login}}</textarea>
                </div>

                <div class="form-group">
                    <label for="terms" class="form-label">Terms Page</label>
                    <textarea class="form-control" name="seo_terms" id="terms" cols="30" rows="10">{{$seo_terms}}&gt;</textarea>
                </div>

                <div class="form-group">
                    <label for="faqs" class="form-label">FAQs Page</label>
                    <textarea class="form-control" name="seo_faqs" id="faqs" cols="30" rows="10">{{$seo_faqs}}</textarea>
                </div>

                <div class="form-group">
                    <label for="contact" class="form-label">Contact Us Page</label>
                    <textarea class="form-control" name="seo_contact" id="contact" cols="30" rows="10">{{$seo_contact}}</textarea>
                </div>

                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->





@endsection
