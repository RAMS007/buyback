@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">memorys</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="row">
            <br>
            <!-- 1st Column -->
            <div class="col-md-4 col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            memorys
                            <a href="#" class="pull-right" data-toggle="modal" data-target="#addDevicememory">
                                <span class="fa fa-plus-circle"></span>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>memory</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($memorys as $memory)
                            <tr>
                                <td>{{$memory->MemoryName}}</td>
                                <td>
                                    <a href="/admin/memory/update/{{$memory->id}}">
                                        <span class="fa fa-pencil"></span>
                                    </a>
                                    <a href="/admin/memory/delete/{{$memory->id}}" class="delete_action" msg="Are you sure you want to delete all entries of this memory?">
                                        <span class="fa fa-trash"></span>
                                    </a>
                                </td>
                            </tr>
                          @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <!-- 1st Column -->
            <!-- 2nd Column -->
            <div class="col-md-8 col-sm-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            memorys for Devices
                            <a href="#" class="pull-right" data-toggle="modal" data-target="#addmemorysToDeviceModal">
                                <span class="fa fa-plus-circle"></span>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Model</th>
                                <th>memory</th>
                                <th>Price </th>

                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($devices2memory as $item)
                            <tr>
                                <td>{{$item->ModelName}}</td>
                                <td>{{$item->MemoryName}}</td>
                                <td>{{$item->MemoryPrice}}</td>

                                <td>
                                    <a href="/admin/model2memory/update/{{$item->id}}">
                                        <span class="fa fa-pencil"></span>
                                    </a>
                                    <a href="/admin/model2memory/delete/{{$item->id}}" class="delete_action" msg="Are you sure you want to delete this memory?">
                                        <span class="fa fa-trash"></span>
                                    </a>
                                </td>
                            </tr>
                           @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- 2nd Column -->

        </div>




        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->




    <!-- addDevicememory -->
    <div class="modal fade" id="addDevicememory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add a memory</h4>
                </div>
                <form action="/admin/memorys/add" method="POST">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="memory_name" class="form-label">memory</label>
                            <input type="text" class="form-control" placeholder="Like New, Poor, etc." name="memory_name">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /addDevicememory -->

    <!-- addmemorysToDeviceModal -->
    <div class="modal fade" id="addmemorysToDeviceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Different memorys for each Model</h4>
                </div>
                <form action="/admin/model2memorys/add" method="POST">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="model_id" class="form-label">Model</label>
                            <select name="model_id" id="model_id" class="form-control">

                                @foreach($models as $model)
                                    <option value="{{$model->id}}">{{$model->ModelName}}</option>

                                @endforeach


                            </select>
                        </div>

                        <div class="form-group">
                            <label for="memory" class="form-label">memory</label>
                            <select name="memory_id" id="memory" class="form-control">
                                @foreach($memorys as $memory)
                                <option value="{{$memory->id}}">{{$memory->MemoryName}}</option>
                        @endforeach
                            </select>
                        </div>


                        <div class="form-group">
                            <label for="price_percentage" class="form-label">Price </label>
                            <input type="text" class="form-control" name="price" placeholder="Add or subtract a percentage of price.">
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /addmemorysToDeviceModal -->





@endsection
