@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Models</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        Device Models
                        <a href="#" class="pull-right" data-target="#addModelModal" data-toggle="modal">
                            <span class="fa fa-plus-circle"></span>
                        </a>
                    </div>
                </div>

                <div >

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                        <form action="/admin/model/update/{{$model->id}}" method="POST" enctype="multipart/form-data">
                            <div class="modal-body">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="device" class="form-label">Select Device</label>
                                    <select name="device_id" id="device" class="form-control">

                                        @foreach($devices as $device)
                                            <option value="{{$device->id}}"

                                            @if($device->id == $model->DeviceId)
selected

                                                @endif
                                            >{{$device->DeviceName}}</option>

                                        @endforeach


                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="model_name" class="form-label">Title</label>
                                    <input type="text" class="form-control" name="model_name" placeholder="iPhone 4, iPad 2, etc." value="{{$model->ModelName}}">
                                </div>

                                <div class="form-group">
                                    <label for="model_price" class="form-label">Price</label>
                                    <input type="text" class="form-control" name="model_price" placeholder="Price in $$$" value="{{$model->ModelPrice}}">
                                </div>

                                <div class="form-group">
                                    <label for="model_img" class="form-label">Image</label>
                                    <input name="model_img" type="file">
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>



                </div>
            </div>

        </div>

        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->





@endsection
