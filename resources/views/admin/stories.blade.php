@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Stories</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        Stories
                        <a href="" class="pull-right" data-target="#addModal" data-toggle="modal"><span class="fa fa-plus-circle"></span></a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Story</th>
                            <th>Actions</th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach ( $stories as $story)


                        <tr>
                            <td>
                                <img src="/{{$story->CustomerImage}}" alt="" width="128" height="128">
                            </td>
                            <td>{{$story->CustomerName}}</td>
                            <td>{{$story->CustomerPosition}}</td>
                            <td>{{$story->CustomerText}}</td>

                            <td>

                                <a href="#" data-toggle="modal" data-target="#addModal" class="OpenModal"  data-id="{{$story->id}}" data-name="{{$story->CustomerName}}"
                                   data-position="{{ $story->CustomerPosition    }}" data-text="{{  $story->CustomerText }}"
                                >



                                    <span class="fa fa-pencil"></span>
                                </a>
                                <a href="/admin/story/delete/{{$story->id}}" class="delete_action" msg="Are you sure you want to delete this device and all corresponding models?">
                                    <span class="fa fa-trash"></span>
                                </a>
                            </td>
                        </tr>
                    @endforeach


                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->




    <!-- Modal -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Story</h4>
                </div>
                <form action="/admin/stories/add" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="modal-body">
                        <input type="hidden" name="storyId" id="storyId" >
                        <div class="form-group">
                            <label for="device_title" class="form-label"> Customer Name</label>
                            <input type="text" class="form-control" name="customer_name" id="customer_name" placeholder="Leona Cole etc.">
                        </div>

                        <div class="form-group">
                            <label for="device_price" class="form-label">Customer Position</label>
                            <input type="text" class="form-control" name="customer_position"  id="customer_position" placeholder="Director M&S">
                        </div>

                        <div class="form-group">
                            <label for="device_img" class="form-label"> Customer Image</label>
                            <input name="customer_img" type="file">
                        </div>

                        <div class="form-group">
                            <label for="device_img" class="form-label"> Customer Text</label>
                            <textarea name="customer_text" id="customer_text"  class="form-control" placeholder="“I was struggling to get my course off the ground, but I discovered buyback and it was so easy to set everything up, great support team, and overall an awesome experience.”"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /Modal -->



@endsection


@section('scripts')

    <script>

        $(document).on("click", ".OpenModal", function () {

            $(".modal-body #storyId").val( $(this).data('id') );
            $(".modal-body #customer_name").val( $(this).data('name') );
            $(".modal-body #customer_position").val( $(this).data('position') );
            $(".modal-body #customer_text").val( $(this).data('text') );



        });


    </script>
@endsection
