@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Devices</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        Devices
                        <a href="" class="pull-right" data-target="#addDeviceModal" data-toggle="modal"><span class="fa fa-plus-circle"></span></a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Device Icon</th>
                            <th>Device Title</th>
                            <th>Device Price</th>
                            <th>Actions</th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach ( $devices as $device)


                        <tr>
                            <td>
                                <img src="/{{$device->DeviceImage}}" alt="" width="128" height="128">
                            </td>
                            <td>{{$device->DeviceName}}</td>
                            <td>${{$device->DevicePrice}}</td>
                            <td>
                                <a href="/admin/device/update/{{$device->id}}">
                                    <span class="fa fa-pencil"></span>
                                </a>
                                <a href="/admin/device/delete/{{$device->id}}" class="delete_action" msg="Are you sure you want to delete this device and all corresponding models?">
                                    <span class="fa fa-trash"></span>
                                </a>
                            </td>
                        </tr>
                    @endforeach


                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->




    <!-- Modal -->
    <div class="modal fade" id="addDeviceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add a Device</h4>
                </div>
                <form action="/admin/devices/add" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="device_title" class="form-label">Name</label>
                            <input type="text" class="form-control" name="device_name" placeholder="iPhone, iPad, etc.">
                        </div>

                        <div class="form-group">
                            <label for="device_price" class="form-label">Price</label>
                            <input type="text" class="form-control" name="device_price" placeholder="Price in $$$">
                        </div>

                        <div class="form-group">
                            <label for="device_img" class="form-label">Image</label>
                            <input name="device_img" type="file">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /Modal -->



@endsection
