@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Years</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="row">
            <br>
            <!-- 1st Column -->
            <div class="col-md-4 col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            years
                            <a href="#" class="pull-right" data-toggle="modal" data-target="#addDeviceyear">
                                <span class="fa fa-plus-circle"></span>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Year</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($years as $year)
                            <tr>
                                <td>{{$year->YearName}}</td>
                                <td>
                                    <a href="/admin/year/update/{{$year->id}}">
                                        <span class="fa fa-pencil"></span>
                                    </a>
                                    <a href="/admin/year/delete/{{$year->id}}" class="delete_action" msg="Are you sure you want to delete all entries of this year?">
                                        <span class="fa fa-trash"></span>
                                    </a>
                                </td>
                            </tr>
                          @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <!-- 1st Column -->
            <!-- 2nd Column -->
            <div class="col-md-8 col-sm-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            years for Devices
                            <a href="#" class="pull-right" data-toggle="modal" data-target="#addyearsToDeviceModal">
                                <span class="fa fa-plus-circle"></span>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Device</th>
                                <th>year</th>
                                <th>Price </th>

                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($devices2year as $item)
                            <tr>
                                <td>{{$item->ModelName}}</td>
                                <td>{{$item->YearName}}</td>
                                <td>{{$item->YearPrice}}</td>

                                <td>
                                    <a href="/admin/model2year/update/{{$item->id}}">
                                        <span class="fa fa-pencil"></span>
                                    </a>
                                    <a href="/admin/model2year/delete/{{$item->id}}" class="delete_action" msg="Are you sure you want to delete this year?">
                                        <span class="fa fa-trash"></span>
                                    </a>
                                </td>
                            </tr>
                           @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- 2nd Column -->

        </div>




        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->




    <!-- addDeviceyear -->
    <div class="modal fade" id="addDeviceyear" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add a year</h4>
                </div>
                <form action="/admin/years/add" method="POST">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="year_name" class="form-label">year</label>
                            <input type="text" class="form-control" placeholder="Like New, Poor, etc." name="year_name">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /addDeviceyear -->

    <!-- addyearsToDeviceModal -->
    <div class="modal fade" id="addyearsToDeviceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Different years for each Model</h4>
                </div>
                <form action="/admin/model2years/add" method="POST">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="model_id" class="form-label">Model</label>
                            <select name="model_id" id="model_id" class="form-control">

                                @foreach($models as $model)
                                    <option value="{{$model->id}}">{{$model->ModelName}}</option>

                                @endforeach


                            </select>
                        </div>

                        <div class="form-group">
                            <label for="year" class="form-label">year</label>
                            <select name="year_id" id="year" class="form-control">
                                @foreach($years as $year)
                                <option value="{{$year->id}}">{{$year->YearName}}</option>
                        @endforeach
                            </select>
                        </div>


                        <div class="form-group">
                            <label for="price_percentage" class="form-label">Price Percentage</label>
                            <input type="text" class="form-control" name="price" placeholder="Add or subtract a percentage of price.">
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /addyearsToDeviceModal -->





@endsection
