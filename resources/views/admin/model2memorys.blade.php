@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">memorys</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="row">
            <br>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="/admin/model2memory/update/{{$Model2memory->id}}" method="POST">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="model_id" class="form-label">Model</label>
                        <select name="model_id" id="model_id" class="form-control">

                            @foreach($models as $model)
                                <option value="{{$model->id}}"


                                        @if($model->id == $Model2memory->ModelId)
                                        selected

                                        @endif


                                >{{$model->ModelName}}</option>

                            @endforeach


                        </select>
                    </div>

                    <div class="form-group">
                        <label for="memory" class="form-label">memory</label>
                        <select name="memory_id" id="memory" class="form-control">
                            @foreach($memorys as $memory)
                                <option value="{{$memory->id}}"


                                        @if($memory->id == $Model2memory->MemoryId)
                                        selected

                                        @endif

                                >{{$memory->MemoryName}}</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group">
                        <label for="price_percentage" class="form-label">Price </label>
                        <input type="text" class="form-control" name="price" placeholder="Add or subtract a percentage of price." value="{{$Model2memory->MemoryPrice}}">
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>


        </div>




        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->










@endsection
