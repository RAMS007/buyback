@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Devices</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        Device
                    </div>
                </div>
                <div >
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="/admin/device/update/{{$device->id}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="device_title" class="form-label">Name</label>
                                <input type="text" class="form-control" name="device_name" placeholder="iPhone, iPad, etc." value="{{$device->DeviceName}}">
                            </div>

                            <div class="form-group">
                                <label for="device_price" class="form-label">Price</label>
                                <input type="text" class="form-control" name="device_price" placeholder="Price in $$$" value="{{$device->DevicePrice}}">
                            </div>

                            <div class="form-group">
                                <label for="device_img" class="form-label">Image</label>
                                <input name="device_img" type="file">
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>



                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->








@endsection
