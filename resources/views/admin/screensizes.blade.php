@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">ScreenSizes</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="row">
            <br>
            <!-- 1st Column -->
            <div class="col-md-4 col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            ScreenSizes
                            <a href="#" class="pull-right" data-toggle="modal" data-target="#addDevicescreensize">
                                <span class="fa fa-plus-circle"></span>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>ScreenSize</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($screensizes as $screensize)
                                <tr>
                                    <td>{{$screensize->ScreenSizeName}}</td>
                                    <td>
                                        <a href="/admin/screenSize/update/{{$screensize->id}}">
                                            <span class="fa fa-pencil"></span>
                                        </a>
                                        <a href="/admin/screenSize/delete/{{$screensize->id}}" class="delete_action" msg="Are you sure you want to delete all entries of this screensize?">
                                            <span class="fa fa-trash"></span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <!-- 1st Column -->
            <!-- 2nd Column -->
            <div class="col-md-8 col-sm-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            ScreenSizes for Devices
                            <a href="#" class="pull-right" data-toggle="modal" data-target="#addscreensizesToDeviceModal">
                                <span class="fa fa-plus-circle"></span>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>

                                <th>Model</th>
                                <th>ScreenSize</th>
                                <th>Price </th>
                                <th>Actions</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($devices2screensize as $item)
                                <tr>
                                    <td>{{$item->ModelName}}</td>
                                    <td>{{$item->ScreenSizeName}}</td>
                                    <td>{{$item->ScreenSizePrice}}</td>

                                    <td>
                                        <a href="/admin/model2screensize/update/{{$item->id}}">
                                            <span class="fa fa-pencil"></span>
                                        </a>
                                        <a href="/admin/model2screensize/delete/{{$item->id}}" class="delete_action" msg="Are you sure you want to delete this screensize?">
                                            <span class="fa fa-trash"></span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- 2nd Column -->

        </div>




        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->




    <!-- addDevicescreensize -->
    <div class="modal fade" id="addDevicescreensize" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add a screensize</h4>
                </div>
                <form action="/admin/screenSize/add" method="POST">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="screensize_name" class="form-label">ScreenSize</label>
                            <input type="text" class="form-control" placeholder="Like New, Poor, etc." name="screensize_name">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /addDevicescreensize -->

    <!-- addscreensizesToDeviceModal -->
    <div class="modal fade" id="addscreensizesToDeviceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Different screensizes for each Model</h4>
                </div>
                <form action="/admin/model2screensizes/add" method="POST">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="device" class="form-label">Device</label>
                            <select name="model_id" id="device" class="form-control">

                                @foreach($models as $model)
                                    <option value="{{$model->id}}">{{$model->ModelName}}</option>

                                @endforeach


                            </select>
                        </div>

                        <div class="form-group">
                            <label for="screensize" class="form-label">screensize</label>
                            <select name="screensize_id" id="screensize" class="form-control">
                                @foreach($screensizes as $screensize)
                                    <option value="{{$screensize->id}}">{{$screensize->ScreenSizeName}}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="form-group">
                            <label for="price_percentage" class="form-label">Price </label>
                            <input type="text" class="form-control" name="price" placeholder="Add or subtract a percentage of price.">
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /addscreensizesToDeviceModal -->





@endsection
