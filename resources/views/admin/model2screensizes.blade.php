@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">ScreenSizes</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="row">
            <br>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            <form action="/admin/model2screensize/update/{{$Model2screensizess->id}}" method="POST">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="device" class="form-label">Device</label>
                        <select name="model_id" id="device" class="form-control">

                            @foreach($models as $model)
                                <option value="{{$model->id}}">{{$model->ModelName}}</option>

                            @endforeach


                        </select>
                    </div>

                    <div class="form-group">
                        <label for="screensize" class="form-label">screensize</label>
                        <select name="screensize_id" id="screensize" class="form-control">
                            @foreach($screensizes as $screensize)
                                <option value="{{$screensize->id}}">{{$screensize->ScreenSizeName}}</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="form-group">
                        <label for="price_percentage" class="form-label">Price </label>
                        <input type="text" class="form-control" name="price"
                               placeholder="Add or subtract a percentage of price." value="{{$Model2screensizess->ScreenSizePrice}}">
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>


        </div>


        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->










@endsection
