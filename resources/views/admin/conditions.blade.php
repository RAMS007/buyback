@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Conditions</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="row">
            <br>
            <!-- 1st Column -->
            <div class="col-md-4 col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Conditions
                            <a href="#" class="pull-right" data-toggle="modal" data-target="#addDeviceCondition">
                                <span class="fa fa-plus-circle"></span>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Condition</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($conditions as $condition)
                            <tr>
                                <td>{{$condition->ConditionName}}</td>
                                <td>
                                    <a href="/admin/condition/update/{{$condition->id}}">
                                        <span class="fa fa-pencil"></span>
                                    </a>
                                    <a href="/admin/condition/delete/{{$condition->id}}" class="delete_action" msg="Are you sure you want to delete all entries of this condition?">
                                        <span class="fa fa-trash"></span>
                                    </a>
                                </td>
                            </tr>
                          @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <!-- 1st Column -->
            <!-- 2nd Column -->
            <div class="col-md-8 col-sm-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Conditions for Devices
                            <a href="#" class="pull-right" data-toggle="modal" data-target="#addConditionsToDeviceModal">
                                <span class="fa fa-plus-circle"></span>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Device</th>
                                <th>Condition</th>
                                <th>Price Percentage</th>
                                <th>Deduct</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($devices2condition as $item)
                            <tr>
                                <td>{{$item->DeviceName}}</td>
                                <td>{{$item->ConditionName}}</td>
                                <td>{{$item->ConditionPricePercentage}}</td>
                                @if($item->deduct==1)
                                <td>Yes</td>
                                @else
                                <td>No</td>
                                    @endif
                                <td>
                                    <a href="/admin/device2condition/update/{{$item->id}}">
                                        <span class="fa fa-pencil"></span>
                                    </a>
                                    <a href="/admin/device2condition/delete/{{$item->id}}" class="delete_action" msg="Are you sure you want to delete this condition?">
                                        <span class="fa fa-trash"></span>
                                    </a>
                                </td>
                            </tr>
                           @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- 2nd Column -->

        </div>




        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->




    <!-- addDeviceCondition -->
    <div class="modal fade" id="addDeviceCondition" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add a Condition</h4>
                </div>
                <form action="/admin/conditions/add" method="POST">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="condition_name" class="form-label">Condition</label>
                            <input type="text" class="form-control" placeholder="Like New, Poor, etc." name="condition_name">
                        </div>

                        <div class="form-group">
                            <label for="condition_description1" class="form-label">description1</label>
                            <textarea  class="form-control" placeholder="Brand new in the original factory sealed box. All plastics must still be intact." name="condition_description1"></textarea>
                        </div>


                        <div class="form-group">
                            <label for="condition_description2" class="form-label">description2</label>
                            <textarea  class="form-control" placeholder="ZERO Scratches and/or flaws. Phone must never have been taken out of the box." name="condition_description2"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="condition_description3" class="form-label">description3</label>
                            <textarea  class="form-control" placeholder="Perfect display with no dust under the screen." name="condition_description3"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="condition_description4" class="form-label">description4</label>
                            <textarea  class="form-control" placeholder="No defects or cosmetic wear whatsoever" name="condition_description4"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="condition_description5" class="form-label">description5</label>
                            <textarea  class="form-control" placeholder="Clear ESN/IMEI for activation by a new user" name="condition_description5"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="condition_description6" class="form-label">description6</label>
                            <textarea  class="form-control" placeholder="Please Note: Any device found to be reported as lost or stolen is not eligible for trade-in" name="condition_description6"></textarea>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /addDeviceCondition -->

    <!-- addConditionsToDeviceModal -->
    <div class="modal fade" id="addConditionsToDeviceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Different Conditions for each Model</h4>
                </div>
                <form action="/admin/device2condition/add" method="POST">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="device" class="form-label">Device</label>
                            <select name="device_id" id="device" class="form-control">

                                @foreach($devices as $device)
                                    <option value="{{$device->id}}">{{$device->DeviceName}}</option>

                                @endforeach


                            </select>
                        </div>

                        <div class="form-group">
                            <label for="condition" class="form-label">Condition</label>
                            <select name="condition_id" id="condition" class="form-control">
                                @foreach($conditions as $condition)
                                <option value="{{$condition->id}}">{{$condition->ConditionName}}</option>
                        @endforeach
                            </select>
                        </div>


                        <div class="form-group">
                            <label for="price_percentage" class="form-label">Price Percentage</label>
                            <input type="text" class="form-control" name="price_percentage" placeholder="Add or subtract a percentage of price.">
                        </div>

                        <div class="form-group">
                            <label for="deduct" class="form-label">Operation</label>
                            <select name="deduct" id="deduct" class="form-control">
                                <option value="1">Add</option>
                                <option value="0">Deduct</option>
                            </select>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /addConditionsToDeviceModal -->





@endsection
