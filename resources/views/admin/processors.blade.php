@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Processorss</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="row">
            <br>
            <!-- 1st Column -->
            <div class="col-md-4 col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            processorss
                            <a href="#" class="pull-right" data-toggle="modal" data-target="#addDeviceprocessors">
                                <span class="fa fa-plus-circle"></span>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Processors</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($processors as $processor)
                                <tr>
                                    <td>{{$processor->ProcessorName}}</td>
                                    <td>
                                        <a href="/admin/processor/update/{{$processor->id}}">
                                            <span class="fa fa-pencil"></span>
                                        </a>
                                        <a href="/admin/processor/delete/{{$processor->id}}" class="delete_action" msg="Are you sure you want to delete all entries of this processors?">
                                            <span class="fa fa-trash"></span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <!-- 1st Column -->
            <!-- 2nd Column -->
            <div class="col-md-8 col-sm-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            Processors for Devices
                            <a href="#" class="pull-right" data-toggle="modal" data-target="#addprocessorssToDeviceModal">
                                <span class="fa fa-plus-circle"></span>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Model</th>
                                <th>Color</th>
                                <th>Price </th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($models2processors as $item)
                                <tr>
                                    <td>{{$item->ModelName}}</td>
                                    <td>{{$item->ProcessorName}}</td>
                                    <td>{{$item->ProcessorPrice}}</td>
                                    <td>
                                        <a href="/admin/model2processor/update/{{$item->id}}">
                                            <span class="fa fa-pencil"></span>
                                        </a>
                                        <a href="/admin/model2processor/delete/{{$item->id}}" class="delete_action" msg="Are you sure you want to delete this processors?">
                                            <span class="fa fa-trash"></span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- 2nd Column -->

        </div>




        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->




    <!-- addDeviceprocessors -->
    <div class="modal fade" id="addDeviceprocessors" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add a processors</h4>
                </div>
                <form action="/admin/processor/add" method="POST">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="processors_name" class="form-label">processors</label>
                            <input type="text" class="form-control" placeholder="Like New, Poor, etc." name="processor_name">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /addDeviceprocessors -->

    <!-- addprocessorssToDeviceModal -->
    <div class="modal fade" id="addprocessorssToDeviceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Different processorss for each Model</h4>
                </div>
                <form action="/admin/model2processors/add" method="POST">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="model_id" class="form-label">Model</label>
                            <select name="model_id" id="model_id" class="form-control">

                                @foreach($models as $model)
                                    <option value="{{$model->id}}">{{$model->ModelName}}</option>

                                @endforeach


                            </select>
                        </div>

                        <div class="form-group">
                            <label for="processors" class="form-label">processors</label>
                            <select name="processor_id" id="processors" class="form-control">
                                @foreach($processors as $processors)
                                    <option value="{{$processors->id}}">{{$processors->ProcessorName}}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="form-group">
                            <label for="price_percentage" class="form-label">Price </label>
                            <input type="text" class="form-control" name="price" placeholder="Add or subtract a percentage of price.">
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /addprocessorssToDeviceModal -->





@endsection
