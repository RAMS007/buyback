@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">carriers</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <div class="row">
            <br>
            <!-- 1st Column -->
            <div class="col-md-4 col-sm-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            carriers
                            <a href="#" class="pull-right" data-toggle="modal" data-target="#addDevicecarrier">
                                <span class="fa fa-plus-circle"></span>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>carrier</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($carriers as $carrier)
                            <tr>
                                <td>{{$carrier->CarrierName}}</td>
                                <td>
                                    <a href="/admin/carrier/update/{{$carrier->id}}">
                                        <span class="fa fa-pencil"></span>
                                    </a>
                                    <a href="/admin/carrier/delete/{{$carrier->id}}" class="delete_action" msg="Are you sure you want to delete all entries of this carrier?">
                                        <span class="fa fa-trash"></span>
                                    </a>
                                </td>
                            </tr>
                          @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <!-- 1st Column -->
            <!-- 2nd Column -->
            <div class="col-md-8 col-sm-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">
                            carriers for Devices
                            <a href="#" class="pull-right" data-toggle="modal" data-target="#addcarriersToDeviceModal">
                                <span class="fa fa-plus-circle"></span>
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Device</th>
                                <th>carrier</th>
                                <th>Price </th>

                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($devices2carrier as $item)
                            <tr>
                                <td>{{$item->ModelName}}</td>
                                <td>{{$item->CarrierName}}</td>
                                <td>{{$item->CarrierPrice}}</td>

                                <td>
                                    <a href="/admin/model2carrier/update/{{$item->id}}">
                                        <span class="fa fa-pencil"></span>
                                    </a>
                                    <a href="/admin/model2carrier/delete/{{$item->id}}" class="delete_action" msg="Are you sure you want to delete this carrier?">
                                        <span class="fa fa-trash"></span>
                                    </a>
                                </td>
                            </tr>
                           @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- 2nd Column -->

        </div>




        <!-- /.row -->

    </div>
    <!-- /#page-wrapper -->




    <!-- addDevicecarrier -->
    <div class="modal fade" id="addDevicecarrier" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add a carrier</h4>
                </div>
                <form action="/admin/carriers/add" method="POST">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="carrier_name" class="form-label">carrier</label>
                            <select name="carrier_name" >
                                <option value="AT&T"> AT&T</option>
                                <option value="Sprint"> Sprint</option>
                                <option value="T-Mobile"> T-Mobile</option>
                                <option value="Verizon"> Verizon</option>
                                <option value="Metro PCS"> Metro PCS</option>
                                <option value="Factory Unlocked"> Factory Unlocked </option>
                                <option value="Boost Mobile"> Boost Mobile</option>
                                <option value="Virgin Mobile"> Virgin Mobile</option>
                                <option value="Straight Talk"> Straight Talk</option>
                                <option value="U.S. Cellular"> U.S. Cellular</option>
                                <option value="Cricket"> Cricket</option>
                                <option value="Other Carriers"> Other Carriers</option>
                            </select>

                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /addDevicecarrier -->

    <!-- addcarriersToDeviceModal -->
    <div class="modal fade" id="addcarriersToDeviceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Different carriers for each Model</h4>
                </div>
                <form action="/admin/model2carriers/add" method="POST">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="model_id" class="form-label">Model</label>
                            <select name="model_id" id="model_id" class="form-control">

                                @foreach($models as $model)
                                    <option value="{{$model->id}}">{{$model->ModelName}}</option>

                                @endforeach


                            </select>
                        </div>

                        <div class="form-group">
                            <label for="carrier" class="form-label">carrier</label>
                            <select name="carrier_id" id="carrier" class="form-control">
                                @foreach($carriers as $carrier)
                                <option value="{{$carrier->id}}">{{$carrier->CarrierName}}</option>
                        @endforeach
                            </select>
                        </div>


                        <div class="form-group">
                            <label for="price_percentage" class="form-label">Price </label>
                            <input type="text" class="form-control" name="price" placeholder="Add or subtract a percentage of price.">
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /addcarriersToDeviceModal -->





@endsection
