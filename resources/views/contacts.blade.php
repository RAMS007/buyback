@extends('layouts.app')

@section('mainContent')


    <div class="history_directory">
        <div class="container">
            <a href="/">Home</a> /
            <span>Contact US</span>
        </div>
    </div>
    <hr>
    <section class="contacts">
        <div class="container">
            <div class="l_side">

                <div class="slogn">keep <br>
                    in <br>
                    touch
                </div>

                <div class="cont_info">
                    <div class="cont_adr">
                        <h2 class="city"><strong>Illinois, </strong> USA</h2>
                        <address>Comes  <br>
                            1112 We Go Trl <br>
                            Deerfield, il 69/25 <br>
                            Zipcode: 60015
                        </address>
                        <a class="cont_mail" href="mailto:support@cashyourtronic"><strong>Email:</strong> support@cashyourtronic</a>
                    </div>
                    <div class="cont_tel">
                        <p>Toll Free:</p>
                        <a href="tel:+847-338-4830">+847-338-4830</a>
                    </div>
                </div>

                <div class="cont_form_wrapp">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <h2>Contact us</h2>
                    <div class="cont_form">
                        @if($sended ==true)
                            <h1>We get your request </h1>
                        @else
                        <form method="post">
                            {{csrf_field()}}
                            <label>
                                <span>Email</span>
                                <input type="email" required placeholder='sample@gmail.com' name="email">
                            </label>
                            <label>
                                <span>Phone Number</span>
                                <input type="tel" required placeholder='+xxx-xxx-xxx' name="phone">
                            </label>
                            <label>
                                <span>Type text</span>
                                <textarea name="text"></textarea>
                            </label>
                            <input type="submit" value="Send">
                        </form>
                        @endif
                    </div>
                </div>

            </div>
            <div class="r_side">
                <img src="img/contact.png" alt="">
            </div>
            <div class="clearfix"></div>
        </div>
    </section>


@endsection