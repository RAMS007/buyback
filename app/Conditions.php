<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conditions extends Model
{
    protected $table='Conditions';
    protected $guarded=['id'];
}
