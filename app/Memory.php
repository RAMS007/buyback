<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Memory extends Model
{
    protected $table='Memory';
    protected $guarded=['id'];
}
