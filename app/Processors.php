<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Processors extends Model
{
    protected $table='Processors';
    protected $guarded=['id'];
}
