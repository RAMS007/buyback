<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactUsRequest extends Model
{
    protected $table='ContactUsRequests';
    protected $guarded=['id'];
}
