<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Models extends Model
{
    protected $table='Models';
    protected $guarded=['id'];


    public function Device()
    {
        return $this->belongsTo('App\Devices', 'DeviceId', 'id');
    }

}
