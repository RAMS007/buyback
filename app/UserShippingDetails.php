<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserShippingDetails extends Model
{
    protected $table='UserShippingDetails';
    protected $guarded=['id'];
}
