<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HardDrive extends Model
{
    protected $table='HardDrive';
    protected $guarded=['id'];
}
