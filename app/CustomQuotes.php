<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomQuotes extends Model
{
    protected $table='CustomQuotes';
    protected $guarded=['id'];
}
