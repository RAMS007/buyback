<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPaymentDetails extends Model
{
    protected $table='UserPaymentDetails';
    protected $guarded=['id'];
}
