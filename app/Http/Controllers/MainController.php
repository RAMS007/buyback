<?php

namespace App\Http\Controllers;

use App\Accesories;
use App\Carriers;
use App\Colors;
use App\Conditions;
use App\ContactUsRequest;
use App\CustomerStories;
use App\CustomQuotes;
use App\Device2Conditions;
use App\Devices;
use App\HardDrive;
use App\Memory;
use App\Model2Accesories;
use App\Model2Carriers;
use App\Model2Colors;
use App\Model2Harddrives;
use App\Model2Memory;
use App\Model2Processors;
use App\Model2ScreenSizes;
use App\Model2Storage;
use App\Model2Year;
use App\Orders;
use App\Processors;
use App\ScreenSize;
use App\Storage;
use App\UserPaymentDetails;
use App\Year;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Models;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use App\UserShippingDetails;


class MainController extends Controller
{
    public function index()
    {

        $allCategories = Devices::all();
        $allCustomerStories = CustomerStories::all();
        return view('index', ['categories' => $allCategories, 'allStories' => $allCustomerStories]);
    }

    public function index1()
    {

        $allCategories = Devices::all();
        $allCustomerStories = CustomerStories::all();
        return view('index1', ['categories' => $allCategories, 'allStories' => $allCustomerStories]);
    }


    public function getDeviceModels($id)
    {

        $user = Auth::user();
        if (empty($user)) {
            $user = '';
            $userPaymentDetails = '';
            $userShippingDetails = '';
        } else {
            $userPaymentDetails = UserPaymentDetails::where('user_id', $user->id)->first();
            $userShippingDetails = UserShippingDetails::where('user_id', $user->id)->first();

        }
        $device = Devices::find($id);
        if (empty($device)) {
            abort(404);
        }

        $models = $device->models()->get();

        return view('models1', ['models' => $models, 'user' => $user, 'userPaymentDetails' => $userPaymentDetails, 'userShippingDetails' => $userShippingDetails]);
        //    return view('models', ['models' => $models]);

    }


    public function getModelParametrs($id)
    {

        $model = Models::find($id);
        if (empty($model)) {
            abort(404);
        }

        $device = $model->Device()->first();


        $sql = "  SELECT Model2Colors.id AS Id, Colors.ColorName AS Name
                FROM 
                Model2Colors
                LEFT JOIN
                Colors ON Colors.id = Model2Colors.ColorId
                WHERE Model2Colors.ModelId=?";
        $colors = DB::select($sql, [$model->id]);

        $sql = "  SELECT `Model2Storage`.id AS Id, `Storage`.StorageName AS Name
                FROM 
                Model2Storage
                LEFT JOIN
                `Storage` ON `Storage`.id = Model2Storage.StorageId
                WHERE Model2Storage.ModelId=?";
        $storage = DB::select($sql, [$model->id]);


        $sql = "  SELECT Model2ScreenSize.id AS Id, ScreenSizes.ScreenSizeName AS Name
                FROM 
                Model2ScreenSize
                LEFT JOIN
                ScreenSizes ON ScreenSizes.id = Model2ScreenSize.ScreenSizeId
                WHERE Model2ScreenSize.ModelId=?";
        $screensizes = DB::select($sql, [$model->id]);


        $sql = "  SELECT Model2Processor.id AS Id, Processors.ProcessorName AS Name
                FROM 
                Model2Processor
                LEFT JOIN
                Processors ON Processors.id = Model2Processor.ProcessorId
                WHERE Model2Processor.ModelId=?";
        $processors = DB::select($sql, [$model->id]);

        $sql = "  SELECT Model2HardDrive.id AS Id, HardDrive.HardDriveName AS Name
                FROM 
                Model2HardDrive
                LEFT JOIN
                HardDrive ON HardDrive.id = Model2HardDrive.HardDriveId
                WHERE Model2HardDrive.ModelId=?";
        $harddrives = DB::select($sql, [$model->id]);

        $sql = "  SELECT Model2Memory.id AS Id, Memory.MemoryName AS Name
                FROM 
                Model2Memory
                LEFT JOIN
                Memory ON Memory.id = Model2Memory.MemoryId
                WHERE Model2Memory.ModelId=?";
        $memorys = DB::select($sql, [$model->id]);

        $sql = "  SELECT Model2Year.id AS Id, Year.YearName AS Name
                FROM 
                Model2Year
                LEFT JOIN 
                Year ON Year.id = Model2Year.YearId
                WHERE Model2Year.ModelId=?";
        $years = DB::select($sql, [$model->id]);

        $sql = "  SELECT Model2Accesories.id AS Id, Accesoriers.AccesoriesName AS Name
                FROM 
                Model2Accesories
                LEFT JOIN 
                Accesoriers ON Accesoriers.id = Model2Accesories.AccesorieId
                WHERE Model2Accesories.ModelId=?";
        $accesories = DB::select($sql, [$model->id]);


        $sql = "  SELECT Model2Carriers.id AS Id, Carriers.CarrierName AS Name, Carriers.CarrierImage AS Image
                FROM 
                Model2Carriers
                LEFT JOIN 
                Carriers ON Carriers.id = Model2Carriers.CarrierId
                WHERE Model2Carriers.ModelId=?";
        $carriers = DB::select($sql, [$model->id]);


        $parametrs = [

            'colors' => $colors,
            'storages' => $storage, 'screensizes' => $screensizes, 'processors' => $processors, 'harddrives' => $harddrives,
            'memorys' => $memorys, 'years' => $years, 'accesories' => $accesories, 'carriers' => $carriers

        ];


        return response()->json(['error' => false, 'device' => $device, 'model' => $model, 'parametrs' => $parametrs]);

        /*
                return view('modelFilters', ['device' => $device, 'model' => $model, 'conditions' => $conditions, 'colors' => $colors,
                    'storages' => $storage, 'screensizes' => $screensizes, 'processors' => $processors, 'harddrives' => $harddrives,
                    'memorys' => $memorys, 'years' => $years, 'accesories' => $accesories, 'carriers' => $carriers

                ]);*/

    }


    public function calculatePrice(Request $request)
    {

        $offer = 0;
        $optionsList = [];
        $model = Models::find($request->modelId);
        if (!empty($model)) {
            $offer += $model->ModelPrice;
        }
        $device = Devices::find($request->deviceId);
        if (!empty($device)) {
            $offer += $device->DevicePrice;
        }

        $color = Model2Colors::find($request->colors);
        if (!empty($color)) {
            $offer += $color->ColorPrice;
            $selected = Colors::find($color->ColorId);
            $optionsList[] = $selected->ColorName;
        }
        $storage = Model2Storage::find($request->storages);
        if (!empty($storage)) {
            $offer += $storage->StoragePrice;
            $selected = Storage::find($storage->StorageId);
            $optionsList[] = $selected->StorageName;
        }
        $screenSize = Model2ScreenSizes::find($request->screensizes);
        if (!empty($screenSize)) {
            $offer += $screenSize->ScreenSizePrice;
            $selected = ScreenSize::find($screenSize->ScreenSizeId);
            $optionsList[] = $selected->ScreenSizeName;

        }
        $processor = Model2Processors::find($request->processors);
        if (!empty($processor)) {
            $offer += $processor->ProcessorPrice;
            $selected = Processors::find($processor->ProcessorId);
            $optionsList[] = $selected->ProcessorName;


        }
        $hardDrive = Model2Harddrives::find($request->harddrives);
        if (!empty($hardDrive)) {
            $offer += $hardDrive->HardDrivePrice;

            $selected = HardDrive::find($hardDrive->HardDriveId);
            $optionsList[] = $selected->HardDriveName;

        }
        $memory = Model2Memory::find($request->memorys);
        if (!empty($memory)) {
            $offer += $memory->MemoryPrice;

            $selected = Memory::find($memory->MemoryId);
            $optionsList[] = $selected->MemoryName;

        }
        $year = Model2Year::find($request->years);
        if (!empty($year)) {
            $offer += $year->YearPrice;
            $selected = Year::find($year->YearId);
            $optionsList[] = $selected->YearName;

        }
        $accesories = Model2Accesories::find($request->accesories);
        if (!empty($accesories)) {
            $offer += $accesories->AccesoriePrice;
            $selected = Accesories::find($accesories->AccesorieId);
            $optionsList[] = $selected->AccesoriesName;

        }
        $carriers = Model2Carriers::find($request->carrier);
        if (!empty($carriers)) {
            $offer += $carriers->CarrierPrice;

            $selected = Carriers::find($carriers->CarrierId);
            $optionsList[] = $selected->CarrierName;

        }


        $sql = "  SELECT Conditions.id AS Id, Conditions.ConditionName AS Name, Device2Conditions.ConditionPricePercentage AS Percent, 
                  Device2Conditions.deduct AS Deduct, Conditions.condition_description1, Conditions.condition_description2,
                  Conditions.condition_description3, Conditions.condition_description4, Conditions.condition_description5,
                  Conditions.condition_description6  
                FROM 
                Device2Conditions
                LEFT JOIN
                Conditions ON Conditions.id =Device2Conditions.ConditionId
                WHERE Device2Conditions.DeviceId=?";
        $conditions = DB::select($sql, [$device->id]);


        return response()->json(['error' => false, 'offer' => $offer, 'conditions' => $conditions,
            'device' => ['ModelImage' => $model->ModelImage, 'ModelName' => $model->ModelName, 'options' => $optionsList]]);


        $condition = Device2Conditions::find($request->condition);
        if (!empty($condition)) {
            $offer = $offer * ($condition->ConditionPricePercentage / 100);
        }

        return view('result', ['offer' => round($offer, 2)]);

    }


    public function showAboutUs(Request $request)
    {
        return view('aboutus');
    }

    public function showContactUs(Request $request)
    {
        return view('contacts', ['sended' => false]);
    }

    public function sendMessage(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'phone' => 'required',
            'text' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('contactUs')
                ->withErrors($validator)
                ->withInput();
        }


        $r = ContactUsRequest::create(['userEmail' => $request->email, 'userPhone' => $request->phone, 'message' => $request->text, 'status' => 'new']);
        if (empty($r)) {
            $validator->errors()->add('general', 'Cant process your request');


            return redirect('contactUs')
                ->withErrors($validator)
                ->withInput();


        }


        return view('contacts', ['sended' => true]);
    }


    public function showFaq()
    {
        return view('faq');

    }

    public function showPrivacy()
    {
        return view('privacy');

    }

    public function showTerms()
    {
        return view('terms');

    }
    public function showQuotes()
    {
        return view('quotes');

    }


    public function saveQuotes(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'category' => 'required',
            'brand' => 'required',
            'model' => 'required',
            'condition' => 'required',
            'photo' => 'required|file',
            'quantity' => 'required|numeric',
            'comment' => 'required'
        ]);


        if ($validator->fails()) {
            return redirect('quotes')
                ->withErrors($validator)
                ->withInput();
        }


        $ImagePath = '';
        $path = public_path() . '/custom';
        if (!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        if ($request->file('photo')->isValid()) {
            $ImagePath = $request->photo->store('/pictures/custom', 'uploads');
        }


        $CustomQuote = CustomQuotes::create([
            'category' => $request->category,
            'ProductBrand' => $request->brand,
            'UserEmail' => $request->email,
            'ProductModel' => $request->model,
            'ProductCondition' => $request->condition,
            'ProductImage' => $ImagePath,
            'ProductQuantity' => $request->quantity,
            'ProductComment' => $request->comment,
            'status' => 'new'
        ]);


        if (!empty($CustomQuote)) {

            return redirect('quotes');


        } else {

            $validator->errors()->add('field', 'Cant create quote');
            return redirect('quotes')
                ->withErrors($validator)
                ->withInput();

        }


    }

    public function showUserProfile()
    {
        $user = Auth::user();
        $payment = UserPaymentDetails::where('user_id', $user->id)->first();
        $shipping = UserShippingDetails::where('user_id', $user->id)->first();

        $sql = 'SELECT Orders.id, Orders.PaymentType, Orders.shipping_Addres, Orders.shipping_city, Orders.shipping_zip, Orders.`status`, Models.ModelName, Models.ModelImage , Orders.created_at
FROM Orders
LEFT JOIN Models ON 
Models.id=Orders.modelId
WHERE Orders.user_id=?';

        $orders = DB::select($sql, [$user->id]);

        foreach($orders as $key=>$order){

            $createdTs= strtotime($order->created_at);
            $order->date=date('M d, Y', $createdTs);
            $order->time=date('G A', $createdTs);
            $order->ModelImage= '/'. $order->ModelImage;
            $order->address = $order->shipping_Addres.' '.$order->shipping_city. ' '.$order->shipping_zip;

        }

        return view('profile', ['user' => $user, 'payment' => $payment, 'shipping' => $shipping, 'page' => 'profile', 'orders'=>$orders]);


    }

    public function showUserProfileOrders()
    {
        $user = Auth::user();
        $payment = UserPaymentDetails::where('user_id', $user->id)->first();
        $shipping = UserShippingDetails::where('user_id', $user->id)->first();


        $sql = 'SELECT Orders.id, Orders.PaymentType, Orders.shipping_Addres, Orders.shipping_city, Orders.shipping_zip, Orders.`status`, Models.ModelName, Models.ModelImage , Orders.created_at
FROM Orders
LEFT JOIN Models ON 
Models.id=Orders.modelId
WHERE Orders.user_id=?';

        $orders = DB::select($sql, [$user->id]);

        foreach($orders as $key=>$order){

           $createdTs= strtotime($order->created_at);
            $order->date=date('M d, Y', $createdTs);
            $order->time=date('G A', $createdTs);
$order->ModelImage= '/'. $order->ModelImage;
            $order->address = $order->shipping_Addres.' '.$order->shipping_city. ' '.$order->shipping_zip;

        }


        return view('profile', ['user' => $user, 'payment' => $payment, 'shipping' => $shipping, 'page' => 'orders', 'orders' => $orders]);


    }


    public function showEditProfile()
    {

        $user = Auth::user();
        if (empty($user)) {
            return redirect('/login');
        }
        $payment = UserPaymentDetails::where('user_id', $user->id)->first();
        if (empty($payments)) {
            $payment = new \stdClass;
            $payment->paymentType = 'check';
            $payment->AddressName = '';
            $payment->FirstName = '';
            $payment->LastName = '';
            $payment->addres1 = '';
            $payment->addres2 = '';
            $payment->city = '';
            $payment->state = '';
            $payment->zip = '';
            $payment->paypalId = '';
        }
        $shipping = UserShippingDetails::where('user_id', $user->id)->first();
        if (empty($shipping)) {
            $shipping = new \stdClass;


            $shipping->shipping_email = '';
            $shipping->shipping_FirstName = '';
            $shipping->shipping_LastName = '';
            $shipping->shipping_Addres = '';
            $shipping->shipping_city = '';
            $shipping->shipping_country = '';
            $shipping->shipping_province = '';
            $shipping->shipping_zip = '';
            $shipping->shipping_phone = '';


        }

        return view('profileEdit', ['user' => $user, 'payment' => $payment, 'shipping' => $shipping]);

    }


    public function saveProfile(Request $request)
    {

        $user = Auth::user();
        $payment = UserPaymentDetails::where('user_id', $user->id)->first();
        $shipping = UserShippingDetails::where('user_id', $user->id)->first();


        $validator = Validator::make(['email' => $request->email], [
            'email' => 'required|email',
        ]);


        if ($validator->fails()) {

        } else {
            $user->email = $request->email;
        }

        if (isset($request->FirstName) AND !empty($request->FirstName)) {
            $user->FirstName = $request->FirstName;
        }

        if (isset($request->LastName) AND !empty($request->LastName)) {
            $user->LastName = $request->LastName;
        }
        if (isset($request->password) AND !empty($request->password)) {
            $user->password = Hash::make($request->password);
        }


        if (empty($shipping)) {
            $shippingArr = [];
            if (isset($request->shipping_Addres) AND !empty($request->shipping_Addres)) {
                $shippingArr['shipping_Addres'] = $request->shipping_Addres;
            }
            if (isset($request->shipping_city) AND !empty($request->shipping_city)) {
                $shippingArr['shipping_city'] = $request->shipping_city;
            }
            if (isset($request->shipping_country) AND !empty($request->shipping_country)) {
                $shippingArr['shipping_country'] = $request->shipping_country;
            }
            if (isset($request->shipping_province) AND !empty($request->shipping_province)) {
                $shippingArr['shipping_province'] = $request->shipping_province;
            }
            if (isset($request->shipping_zip) AND !empty($request->shipping_zip)) {
                $shippingArr['shipping_zip'] = $request->shipping_zip;
            }
            if (isset($request->shipping_phone) AND !empty($request->shipping_phone)) {
                $shippingArr['shipping_phone'] = $request->shipping_phone;
            }
            $shippingArr['user_id'] = $user->id;

            UserShippingDetails::create($shippingArr);

        } else {
            if (isset($request->shipping_Addres) AND !empty($request->shipping_Addres)) {
                $shipping->shipping_Addres = $request->shipping_Addres;
            }
            if (isset($request->shipping_city) AND !empty($request->shipping_city)) {
                $shipping->shipping_city = $request->shipping_city;
            }
            if (isset($request->shipping_country) AND !empty($request->shipping_country)) {
                $shipping->shipping_country = $request->shipping_country;
            }
            if (isset($request->shipping_province) AND !empty($request->shipping_province)) {
                $shipping->shipping_province = $request->shipping_province;
            }
            if (isset($request->shipping_zip) AND !empty($request->shipping_zip)) {
                $shipping->shipping_zip = $request->shipping_zip;
            }
            if (isset($request->shipping_phone) AND !empty($request->shipping_phone)) {
                $shipping->shipping_phone = $request->shipping_phone;
            }
        }


        if (empty($payment)) {

            $paymentsArray = [];

            if (isset($request->AddressName) AND !empty($request->AddressName)) {
                $paymentsArray['AddressName'] = $request->AddressName;
            }
            if (isset($request->payment_FirstName) AND !empty($request->payment_FirstName)) {
                $paymentsArray['FirstName'] = $request->payment_FirstName;
            }
            if (isset($request->payment_LastName) AND !empty($request->payment_LastName)) {
                $paymentsArray['LastName'] = $request->payment_LastName;
            }
            if (isset($request->addres1) AND !empty($request->addres1)) {
                $paymentsArray['addres1'] = $request->addres1;
            }
            if (isset($request->addres2) AND !empty($request->addres2)) {
                $paymentsArray['addres2'] = $request->addres2;
            }
            if (isset($request->city) AND !empty($request->city)) {
                $paymentsArray['city'] = $request->city;
            }
            if (isset($request->zip) AND !empty($request->zip)) {
                $paymentsArray['zip'] = $request->zip;
            }
            if (isset($request->paypalId) AND !empty($request->paypalId)) {
                $paymentsArray['paypalId'] = $request->paypalId;
            }
            $paymentsArray['user_id'] = $user->id;

            UserPaymentDetails::create($paymentsArray);

        } else {


            if (isset($request->AddressName) AND !empty($request->AddressName)) {
                $payment->AddressName = $request->AddressName;
            }
            if (isset($request->payment_FirstName) AND !empty($request->payment_FirstName)) {
                $payment->FirstName = $request->payment_FirstName;
            }
            if (isset($request->payment_LastName) AND !empty($request->payment_LastName)) {
                $payment->LastName = $request->payment_LastName;
            }
            if (isset($request->addres1) AND !empty($request->addres1)) {
                $payment->addres1 = $request->addres1;
            }
            if (isset($request->addres2) AND !empty($request->addres2)) {
                $payment->addres2 = $request->addres2;
            }
            if (isset($request->city) AND !empty($request->city)) {
                $payment->city = $request->city;
            }
            if (isset($request->zip) AND !empty($request->zip)) {
                $payment->zip = $request->zip;
            }
            if (isset($request->paypalId) AND !empty($request->paypalId)) {
                $payment->paypalId = $request->paypalId;
            }
        }

        $user->save();
        $shipping->save();
        $payment->save();
        return view('profileEdit', ['user' => $user, 'payment' => $payment, 'shipping' => $shipping]);

    }


    public function createOrder(Request $request)
    {
        $allData = $request->all();

        $user = Auth::user();
        if (empty($user)) {
            $user_id = 0;
        } else {
            $user_id = $user->id;
        }

        $order = Orders::create([
            'user_id' => $user_id,
            'parametrs' => serialize(['colors' => $allData['colors'],
                'storages' => $allData['storages'],
                'screensizes' => $allData['screensizes'],
                'processors' => $allData['processors'],
                'harddrives' => $allData['harddrives'],
                'memorys' => $allData['memorys'],
                'years' => $allData['years'],
                'accesories' => $allData['accesories'],
                'carrier' => $allData['carrier'],
                'condition' => $allData['condition']
            ]),
            'deviceId' => $allData['deviceId'],
            'modelId' => $allData['modelId'],
            'PaymentType' => $allData['PaymentType'],
            'Payments_AddressName' => $allData['Payments_AddressName'],
            'Payments_FirstName' => $allData['Payments_FirstName'],
            'Payments_LastName' => $allData['Payments_LastName'],
            'Payments_addres1' => $allData['Payments_addres1'],
            'Payments_addres2' => $allData['Payments_addres2'],
            'Payments_city' => $allData['Payments_city'],
            'Payments_zip' => $allData['Payments_zip'],
            'shipping_email' => $allData['shipping_email'],
            'shipping_FirstName' => $allData['shipping_FirstName'],
            'shipping_LastName' => $allData['shipping_LastName'],
            'shipping_Addres' => $allData['shipping_Addres'],
            'shipping_zip' => $allData['shipping_zip'],
            'shipping_phone' => $allData['shipping_phone'],
            'email' => $allData['email'],
            'paypal_account' => $allData['paypal_account'],
            'paypal_account2' => $allData['paypal_account2'],
            'offeredPrice' => $allData['offeredPrice']
        ]);

        if (!empty($order)) {

            return response()->json(['error' => false]);
        } else {
            return response()->json(['error' => true]);

        }

    }

}

