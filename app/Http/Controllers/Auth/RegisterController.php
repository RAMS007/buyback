<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use App\UserPaymentDetails;
use App\UserShippingDetails;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'LastName' => 'required|string|max:255',
            'FirstName' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users|confirmed',
            'password' => 'required|string|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'LastName' => $data['LastName'],
            'FirstName' => $data['FirstName'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }


    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'LastName' => 'required|string|max:255',
            'FirstName' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users|confirmed',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $lastnameEr = $errors->get('LastName');
            $firstNameEr = $errors->get('FirstName');
            $emailEr = $errors->get('email');
            $passwordEr = $errors->get('password');


            return response()->json(['error' => true, 'LastName' => $lastnameEr, 'FirstName' => $firstNameEr, 'email' => $emailEr, 'password' => $passwordEr]);

        } else {

            $user = User::create([
                'LastName' => $request->LastName,
                'FirstName' => $request->FirstName,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ]);
            session(['user' => $user]);
            return response()->json(['error' => false, 'user' => $user]);

        }

    }

    public function registerPayments(Request $request)
    {

        $user = session('user', '');
        if (!isset($user->id)) {

            abort(404);
        }
        if ($request->paymentType == 'check') {

            $validator = Validator::make($request->all(), [
                'AddressName' => 'required|string|max:255',
                'FirstName' => 'required|string|max:255',
                'LastName' => 'required|string|max:255',
                'addres1' => 'required|string|max:255',
                'addres2' => 'required|string|max:255',
                'city' => 'required|string|max:255',
                'state' => 'required|string|max:255',
                'zip' => 'required|string|max:255',

            ]);


        } else {

            $validator = Validator::make($request->all(), [
                'paypalId' => 'required|string|max:255|confirmed',

            ]);

        }


        if ($validator->fails()) {
            $errors = $validator->errors();


            return response()->json(['error' => true, 'errors' => $errors]);

        } else {


            $payment = UserPaymentDetails::create([

                'user_id' => $user->id,
                'paymentType' => $request->paymentType,
                'AddressName' => isset($request->AddressName) ? $request->AddressName : '',
                'FirstName' => isset($request->FirstName) ? $request->FirstName : '',
                'LastName' => isset($request->LastName) ? $request->LastName : '',
                'addres1' => isset($request->addres1) ? $request->addres1 : '',
                'addres2' => isset($request->addres2) ? $request->addres2 : '',
                'city' => isset($request->city) ? $request->city : '',
                'state' => isset($request->state) ? $request->state : '',
                'zip' => isset($request->zip) ? $request->zip : '',
                'paypalId' => isset($request->paypalId) ? $request->paypalId : '',

            ]);

            if ($payment) {
                return response()->json(['error' => false]);

            } else {
                return response()->json(['error' => true, 'msg' => 'Cant create record']);

            }
        }
    }

    public function registerShipping(Request $request)
    {
        $user = session('user', '');
        if (!isset($user->id)) {

            abort(404);
        }


        $validator = Validator::make($request->all(), [
            'shipping_email' => 'required|string|max:255',
            'shipping_FirstName' => 'required|string|max:255',
            'shipping_LastName' => 'required|string|max:255',
            'shipping_Addres' => 'required|string|max:255',
            'shipping_city' => 'required|string|max:255',
            'shipping_country' => 'required|string|max:255',
            'shipping_province' => 'required|string|max:255',
            'shipping_zip' => 'required|string|max:255',
            'shipping_phone' => 'required|string|max:255',


        ]);


        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json(['error' => true, 'errors' => $errors]);

        } else {


            $shipping = UserShippingDetails::create([

                'user_id' => $user->id,
                'shipping_email' => $request->shipping_email,
                'shipping_FirstName' => isset($request->shipping_FirstName) ? $request->shipping_FirstName : '',
                'shipping_LastName' => isset($request->shipping_LastName) ? $request->shipping_LastName : '',
                'shipping_Addres' => isset($request->shipping_Addres) ? $request->shipping_Addres : '',
                'shipping_city' => isset($request->shipping_city) ? $request->shipping_city : '',
                'shipping_country' => isset($request->shipping_country) ? $request->shipping_country : '',
                'shipping_province' => isset($request->shipping_province) ? $request->shipping_province : '',
                'shipping_zip' => isset($request->shipping_zip) ? $request->shipping_zip : '',
                'shipping_phone' => isset($request->shipping_phone) ? $request->shipping_phone : '',


            ]);

            if ($shipping) {
                return response()->json(['error' => false]);
            } else {
                return response()->json(['error' => true, 'msg' => 'Cant create record']);

            }
        }
    }


    public function showRegistrationForm()
    {
        return view('auth.sign', ['page' => 'register']);
    }


}
