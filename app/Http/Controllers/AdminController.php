<?php

namespace App\Http\Controllers;

use App\ContactUsRequest;
use App\CustomerStories;
use App\CustomQuotes;
use App\Devices;
use App\Models;
use App\Orders;
use App\Settings;
use App\Year;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Conditions;
use App\Colors;
use App\Accesories;
use App\Carriers;
use App\HardDrive;
use App\Memory;
use App\Processors;
use App\Storage;
use App\ScreenSize;


class AdminController extends Controller
{
    public function Dashboard(Request $request)
    {

        $customQuotes = CustomQuotes::all()->count();
        $contactUsRequests = ContactUsRequest::all()->count();
        //  $totalOrders =Orders::all()->count();
        $totalOrders = 0;

        return view('admin.dashboard', ['customQuotes' => $customQuotes, 'contactUsRequests' => $contactUsRequests, 'totalOrders' => $totalOrders]);
    }


    public function showDevices(Request $request)
    {
        $allDevices = Devices::all();
        return view('admin.devices', ['devices' => $allDevices]);
    }

    public function editDevice($deviceId)
    {
        $Device = Devices::find($deviceId);
        if (empty($Device)) {
            abort(404);
        }
        return view('admin.device', ['device' => $Device]);
    }


    public function deleteDevice($deviceId)
    {
        $Device = Devices::find($deviceId);
        if (empty($Device)) {
            abort(404);
        } else {
            $Device->delete();
        }
        return redirect('admin/devices/');
    }


    public function saveDevice($deviceId, Request $request)
    {
        $pathImage = '';
        $path = public_path() . '/pictures';
        if (!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }
        $path = public_path() . '/pictures/devices';
        if (!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        $validator = Validator::make($request->all(), [
            'device_name' => 'required|max:255',
            'device_price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return redirect('admin/device/update/' . $deviceId)
                ->withErrors($validator)
                ->withInput();

        }

        if ($request->hasFile('device_img')) {
            if ($request->file('device_img')->isValid()) {
                $pathImage = $request->device_img->store('/pictures/devices', 'uploads');
            }
        }
        $device = Devices::find($deviceId);
        if (empty($device)) {
            $validator->errors()->add('field', 'Device not found!');
            return redirect('admin/device/update/' . $deviceId)
                ->withErrors($validator)
                ->withInput();
        } else {

            $device->DeviceName = $request->device_name;
            $device->DevicePrice = $request->device_price;
            if (!empty($pathImage)) {
                $device->DeviceImage = $pathImage;
            }
            $device->save();
            return redirect('admin/devices/');
        }
    }


    public function addDevice(Request $request)
    {
        $ImagePath = '';
        $path = public_path() . '/pictures';
        if (!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }
        $path = public_path() . '/pictures/devices';
        if (!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        $validator = Validator::make($request->all(), [
            'device_name' => 'required|max:255',
            'device_price' => 'required|numeric',
            'device_img' => 'image',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['error' => true, 'msg' => $errors]);
        }

        if ($request->file('device_img')->isValid()) {
            $ImagePath = $request->device_img->store('/pictures/devices', 'uploads');
        }
        $device = Devices::create(['DeviceName' => $request->device_name, 'DeviceImage' => $ImagePath, 'DevicePrice' => $request->device_price]);
        if (!empty($device)) {
            return redirect('admin/devices/');
            return response()->json(['error' => false, 'path' => $path]);
        } else {
            return response()->json(['error' => true, 'msg' => 'Cant create records']);
        }
    }


    public function showModels(Request $request)
    {

        $sql = '  SELECT Models.DeviceId, Models.ModelName, Models.ModelImage, Models.ModelPrice, Devices.DeviceName , Models.id
                FROM 
                Models
                LEFT JOIN Devices ON Devices.id =Models.DeviceId';
        $allModels = DB::select($sql);
        $allDevices = Devices::all();
        return view('admin.models', ['models' => $allModels, 'devices' => $allDevices]);
    }

    public function addModel(Request $request)
    {

        $ImagePath = '';
        $path = public_path() . '/pictures';
        if (!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }
        $path = public_path() . '/pictures/models';
        if (!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        $validator = Validator::make($request->all(), [
            'model_name' => 'required|max:255',
            'model_price' => 'required|numeric',
            'device_id' => 'required|integer',
            'model_img' => 'image',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['error' => true, 'msg' => $errors]);
        }

        if ($request->file('model_img')->isValid()) {
            $ImagePath = $request->model_img->store('/pictures/models', 'uploads');
        }


        $model = Models::create(['DeviceId' => $request->device_id, 'ModelImage' => $ImagePath, 'ModelPrice' => $request->model_price, 'ModelName' => $request->model_name]);
        if (!empty($model)) {
            return redirect('admin/models/');
            return response()->json(['error' => false, 'path' => $ImagePath]);
        } else {
            return response()->json(['error' => true, 'msg' => 'Cant create model record']);
        }
    }


    public function deleteModel($modelId)
    {
        $model = Models::find($modelId);
        if (empty($model)) {
            abort(404);
        } else {
            $model->delete();
        }
        return redirect('admin/models/');
    }


    public function editModel($modelId)
    {
        //   $model = Models::find($modelId);

        $sql = '  SELECT Models.DeviceId, Models.ModelName, Models.ModelImage, Models.ModelPrice, Devices.DeviceName , Models.id
                FROM 
                Models
                LEFT JOIN Devices ON Devices.id =Models.DeviceId
                WHERE Models.id=?
                
                ';
        $model = DB::select($sql, [$modelId]);
        $allDevices = Devices::all();


        if (!isset($model[0])) {
            abort(404);
        }
        return view('admin.model', ['model' => $model[0], 'devices' => $allDevices]);
    }


    public function saveModel($modelId, Request $request)
    {
        $pathImage = '';
        $path = public_path() . '/pictures';
        if (!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }
        $path = public_path() . '/pictures/models';
        if (!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        $validator = Validator::make($request->all(), [

            'device_id' => 'required|integer',
            'model_name' => 'required|max:255',
            'model_price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return redirect('admin/model/update/' . $modelId)
                ->withErrors($validator)
                ->withInput();

        }

        if ($request->hasFile('model_img')) {
            if ($request->file('model_img')->isValid()) {
                $pathImage = $request->model_img->store('/pictures/models', 'uploads');
            }
        }
        $model = Models::find($modelId);
        if (empty($model)) {
            $validator->errors()->add('field', 'Device not found!');
            return redirect('admin/device/update/' . $modelId)
                ->withErrors($validator)
                ->withInput();
        } else {

            $model->ModelName = $request->model_name;
            $model->ModelPrice = $request->model_price;
            $model->DeviceId = $request->device_id;
            if (!empty($pathImage)) {
                $model->ModelImage = $pathImage;
            }
            $model->save();
            return redirect('admin/models/');
        }
    }

////////////////////////////////////////////

    public function showConditions(Request $request)
    {
        $allconditions = Conditions::all();
        $sql = "  SELECT Device2Conditions.ConditionPricePercentage, Device2Conditions.deduct,Devices.DeviceName,Conditions.ConditionName, Device2Conditions.id
                FROM
                Device2Conditions
                LEFT JOIN Devices ON Device2Conditions.DeviceId =Devices.id
                LEFT JOIN Conditions ON Device2Conditions.ConditionId =Conditions.id
                
                
                ";
        $devices2condition = DB::select($sql);
        $allDevices = Devices::all();
        return view('admin.conditions', ['conditions' => $allconditions, 'devices2condition' => $devices2condition, 'devices' => $allDevices]);
    }

    public function addCondition(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'condition_name' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['error' => true, 'msg' => $errors]);
        }


        $condition = Conditions::create(['ConditionName' => $request->condition_name,
                'condition_description1' => $request->condition_description1,
                'condition_description2' => $request->condition_description2,
                'condition_description3' => $request->condition_description3,
                'condition_description4' => $request->condition_description4,
                'condition_description5' => $request->condition_description5,
                'condition_description6' => $request->condition_description6,


            ]
        );
        if (!empty($condition)) {
            return redirect('admin/conditions/');
            return response()->json(['error' => false, 'path' => $ImagePath]);
        } else {
            return response()->json(['error' => true, 'msg' => 'Cant create condition record']);
        }
    }


    public function deleteCondition($conditionId)
    {
        $condition = Conditions::find($conditionId);
        if (empty($condition)) {
            abort(404);
        } else {
            $condition->delete();
        }
        return redirect('admin/conditions/');
    }


    public function editCondition($conditionId)
    {
        $condition = Conditions::find($conditionId);
        if (empty($condition)) {
            abort(404);
        }
        return view('admin.condition', ['condition' => $condition]);
    }


    public function saveCondition($conditionId, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'condition_name' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            return redirect('admin/condition/update/' . $conditionId)
                ->withErrors($validator)
                ->withInput();

        }
        $condition = Conditions::find($conditionId);
        if (empty($condition)) {
            $validator->errors()->add('field', 'Condition not found!');
            return redirect('admin/condition/update/' . $conditionId)
                ->withErrors($validator)
                ->withInput();
        } else {
            $condition->conditionName = $request->condition_name;
            $condition->condition_description1 = $request->condition_description1;
            $condition->condition_description2 = $request->condition_description2;
            $condition->condition_description3 = $request->condition_description3;
            $condition->condition_description4 = $request->condition_description4;
            $condition->condition_description5 = $request->condition_description5;
            $condition->condition_description6 = $request->condition_description6;
            $condition->save();
            return redirect('admin/conditions/');
        }
    }


///////////////////////////////


    public function showColors(Request $request)
    {
        $allcolors = Colors::all();
        $sql = "  SELECT Model2Colors.ColorPrice, Models.ModelName, Colors.ColorName, Model2Colors.id
                FROM
                 Model2Colors
                LEFT JOIN Models ON Model2Colors.ModelId =Models.id
                LEFT JOIN Colors ON Model2Colors.ColorId =Colors.id
                ";
        $model2color = DB::select($sql);
        $allModels = Models::all();
        return view('admin.colors', ['colors' => $allcolors, 'model2color' => $model2color, 'models' => $allModels]);
    }

    public function addColor(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'color_name' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['error' => true, 'msg' => $errors]);
        }


        $color = Colors::create(['colorName' => $request->color_name]);
        if (!empty($color)) {
            return redirect('admin/colors/');
            return response()->json(['error' => false, 'path' => $ImagePath]);
        } else {
            return response()->json(['error' => true, 'msg' => 'Cant create color record']);
        }
    }


    public function deleteColor($colorId)
    {
        $color = Colors::find($colorId);
        if (empty($color)) {
            abort(404);
        } else {
            $color->delete();
        }
        return redirect('admin/colors/');
    }


    public function editColor($colorId)
    {
        $color = Colors::find($colorId);
        if (empty($color)) {
            abort(404);
        }
        return view('admin.color', ['color' => $color]);
    }


    public function saveColor($colorId, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'color_name' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            return redirect('admin/color/update/' . $colorId)
                ->withErrors($validator)
                ->withInput();

        }
        $color = Colors::find($colorId);
        if (empty($color)) {
            $validator->errors()->add('field', 'color not found!');
            return redirect('admin/color/update/' . $colorId)
                ->withErrors($validator)
                ->withInput();
        } else {
            $color->colorName = $request->color_name;
            $color->save();
            return redirect('admin/colors/');
        }
    }

//////////////////////////////

    public function showStorages(Request $request)
    {
        $allstorages = Storage::all();
        $sql = " SELECT Model2Storage.StoragePrice, Models.ModelName, Storage.StorageName, Model2Storage.id
                FROM
                 Model2Storage
                LEFT JOIN Models ON Model2Storage.ModelId =Models.id
                LEFT JOIN Storage ON Model2Storage.StorageId =Storage.id
                
                ";
        $devices2storage = DB::select($sql);

        $allModels = Models::all();
        return view('admin.storages', ['storages' => $allstorages, 'devices2storage' => $devices2storage, 'models' => $allModels]);
    }

    public function addstorage(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'storage_name' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['error' => true, 'msg' => $errors]);
        }


        $storage = Storage::create(['storageName' => $request->storage_name]);
        if (!empty($storage)) {
            return redirect('admin/storages/');
            return response()->json(['error' => false, 'path' => $ImagePath]);
        } else {
            return response()->json(['error' => true, 'msg' => 'Cant create storage record']);
        }
    }


    public function deleteStorage($storageId)
    {
        $storage = Storage::find($storageId);
        if (empty($storage)) {
            abort(404);
        } else {
            $storage->delete();
        }
        return redirect('admin/storages/');
    }


    public function editStorage($storageId)
    {
        $storage = Storage::find($storageId);
        if (empty($storage)) {
            abort(404);
        }
        return view('admin.storage', ['storage' => $storage]);
    }


    public function saveStorage($storageId, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'storage_name' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            return redirect('admin/storage/update/' . $storageId)
                ->withErrors($validator)
                ->withInput();

        }
        $storage = Storage::find($storageId);
        if (empty($storage)) {
            $validator->errors()->add('field', 'storage not found!');
            return redirect('admin/storage/update/' . $storageId)
                ->withErrors($validator)
                ->withInput();
        } else {
            $storage->storageName = $request->storage_name;
            $storage->save();
            return redirect('admin/storages/');
        }
    }

///////////////////////////////////////


    public function showScreenSizes(Request $request)
    {
        $allscreensizes = ScreenSize::all();
        $sql = "  SELECT Model2ScreenSize.ScreenSizePrice, Models.ModelName, ScreenSizes.ScreenSizeName, Model2ScreenSize.id
                FROM
                 Model2ScreenSize
                LEFT JOIN Models ON Model2ScreenSize.ModelId =Models.id
                LEFT JOIN ScreenSizes ON Model2ScreenSize.ScreenSizeId =ScreenSizes.id
                
                
                
                ";
        $devices2screensize = DB::select($sql);


        $allModels = Models::all();
        return view('admin.screensizes', ['screensizes' => $allscreensizes, 'devices2screensize' => $devices2screensize, 'models' => $allModels]);
    }

    public function addScreenSize(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'screensize_name' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['error' => true, 'msg' => $errors]);
        }


        $screensize = ScreenSize::create(['screensizeName' => $request->screensize_name]);
        if (!empty($screensize)) {
            return redirect('admin/screensizes/');
            return response()->json(['error' => false, 'path' => $ImagePath]);
        } else {
            return response()->json(['error' => true, 'msg' => 'Cant create screensize record']);
        }
    }


    public function deleteScreenSize($screensizeId)
    {
        $screensize = ScreenSize::find($screensizeId);
        if (empty($screensize)) {
            abort(404);
        } else {
            $screensize->delete();
        }
        return redirect('admin/screensizes/');
    }


    public function editScreenSize($screensizeId)
    {
        $screensize = ScreenSize::find($screensizeId);
        if (empty($screensize)) {
            abort(404);
        }
        return view('admin.screensize', ['screensize' => $screensize]);
    }


    public function saveScreenSize($screensizeId, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'screensize_name' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            return redirect('admin/screensize/update/' . $screensizeId)
                ->withErrors($validator)
                ->withInput();

        }
        $screensize = ScreenSize::find($screensizeId);
        if (empty($screensize)) {
            $validator->errors()->add('field', 'screensize not found!');
            return redirect('admin/screensize/update/' . $screensizeId)
                ->withErrors($validator)
                ->withInput();
        } else {
            $screensize->screensizeName = $request->screensize_name;
            $screensize->save();
            return redirect('admin/screensizes/');
        }
    }

/////////////////////////////////////////////////////////

    public function showProcessors(Request $request)
    {
        $allprocessors = Processors::all();
        $sql = "  SELECT Model2Processor.ProcessorPrice, Models.ModelName, Processors.ProcessorName, Model2Processor.id
                FROM
                 Model2Processor
                LEFT JOIN Models ON Model2Processor.ModelId =Models.id
                LEFT JOIN Processors ON Model2Processor.ProcessorId =Processors.id
 
                ";
        $devices2processor = DB::select($sql);

        $allModels = Models::all();
        return view('admin.processors', ['processors' => $allprocessors, 'models2processors' => $devices2processor, 'models' => $allModels]);
    }

    public function addProcessor(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'processor_name' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['error' => true, 'msg' => $errors]);
        }


        $processor = Processors::create(['ProcessorName' => $request->processor_name]);
        if (!empty($processor)) {
            return redirect('admin/processors/');
            return response()->json(['error' => false, 'path' => $ImagePath]);
        } else {
            return response()->json(['error' => true, 'msg' => 'Cant create processor record']);
        }
    }


    public function deleteProcessor($processorId)
    {
        $processor = Processors::find($processorId);
        if (empty($processor)) {
            abort(404);
        } else {
            $processor->delete();
        }
        return redirect('admin/processors/');
    }


    public function editProcessor($processorId)
    {
        $processor = Processors::find($processorId);
        if (empty($processor)) {
            abort(404);
        }
        return view('admin.processor', ['processor' => $processor]);
    }


    public function saveProcessor($processorId, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'processor_name' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            return redirect('admin/processor/update/' . $processorId)
                ->withErrors($validator)
                ->withInput();

        }
        $processor = Processors::find($processorId);
        if (empty($processor)) {
            $validator->errors()->add('field', 'processor not found!');
            return redirect('admin/processor/update/' . $processorId)
                ->withErrors($validator)
                ->withInput();
        } else {
            $processor->processorName = $request->processor_name;
            $processor->save();
            return redirect('admin/processors/');
        }
    }


    ////////////////////////////////////////////

    public function showHardDrives(Request $request)
    {
        $allharddrives = HardDrive::all();
        $sql = "  
                SELECT Model2HardDrive.HardDrivePrice, Models.ModelName, HardDrive.HardDriveName, Model2HardDrive.id
                FROM
                 Model2HardDrive
                LEFT JOIN Models ON Model2HardDrive.ModelId =Models.id
                LEFT JOIN HardDrive ON Model2HardDrive.HardDriveId =HardDrive.id
                
                
                ";
        $devices2harddrive = DB::select($sql);

        $allModels = Models::all();
        return view('admin.harddrives', ['harddrives' => $allharddrives, 'devices2harddrive' => $devices2harddrive, 'models' => $allModels]);
    }

    public function addHardDrive(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'harddrive_name' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['error' => true, 'msg' => $errors]);
        }


        $harddrive = HardDrive::create(['harddriveName' => $request->harddrive_name]);
        if (!empty($harddrive)) {
            return redirect('admin/harddrives/');
            return response()->json(['error' => false, 'path' => $ImagePath]);
        } else {
            return response()->json(['error' => true, 'msg' => 'Cant create harddrive record']);
        }
    }


    public function deleteHardDrive($harddriveId)
    {
        $harddrive = HardDrive::find($harddriveId);
        if (empty($harddrive)) {
            abort(404);
        } else {
            $harddrive->delete();
        }
        return redirect('admin/harddrives/');
    }


    public function editHardDrive($harddriveId)
    {
        $harddrive = HardDrive::find($harddriveId);
        if (empty($harddrive)) {
            abort(404);
        }
        return view('admin.harddrive', ['harddrive' => $harddrive]);
    }


    public function saveHardDrive($harddriveId, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'harddrive_name' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            return redirect('admin/harddrive/update/' . $harddriveId)
                ->withErrors($validator)
                ->withInput();

        }
        $harddrive = HardDrive::find($harddriveId);
        if (empty($harddrive)) {
            $validator->errors()->add('field', 'harddrive not found!');
            return redirect('admin/harddrive/update/' . $harddriveId)
                ->withErrors($validator)
                ->withInput();
        } else {
            $harddrive->harddriveName = $request->harddrive_name;
            $harddrive->save();
            return redirect('admin/harddrives/');
        }
    }

////////////////////////////////////////////

    public function showMemorys(Request $request)
    {
        $allmemorys = Memory::all();
        $sql = "  
               SELECT Model2Memory.MemoryPrice, Models.ModelName, Memory.MemoryName, Model2Memory.id
                FROM
                 Model2Memory
                LEFT JOIN Models ON Model2Memory.ModelId =Models.id
                LEFT JOIN Memory ON Model2Memory.MemoryId =Memory.id
 
                
                
                
                
                
                ";
        $devices2memory = DB::select($sql);
        $allModels = Models::all();
        return view('admin.memorys', ['memorys' => $allmemorys, 'devices2memory' => $devices2memory, 'models' => $allModels]);
    }

    public function addMemory(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'memory_name' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['error' => true, 'msg' => $errors]);
        }


        $memory = Memory::create(['MemoryName' => $request->memory_name]);
        if (!empty($memory)) {
            return redirect('admin/memorys/');
            return response()->json(['error' => false, 'path' => $ImagePath]);
        } else {
            return response()->json(['error' => true, 'msg' => 'Cant create memory record']);
        }
    }


    public function deleteMemory($memoryId)
    {
        $memory = Memory::find($memoryId);
        if (empty($memory)) {
            abort(404);
        } else {
            $memory->delete();
        }
        return redirect('admin/memorys/');
    }


    public function editMemory($memoryId)
    {
        $memory = Memory::find($memoryId);
        if (empty($memory)) {
            abort(404);
        }
        return view('admin.memory', ['memory' => $memory]);
    }


    public function saveMemory($memoryId, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'memory_name' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            return redirect('admin/memory/update/' . $memoryId)
                ->withErrors($validator)
                ->withInput();

        }
        $memory = Memory::find($memoryId);
        if (empty($memory)) {
            $validator->errors()->add('field', 'memory not found!');
            return redirect('admin/memory/update/' . $memoryId)
                ->withErrors($validator)
                ->withInput();
        } else {
            $memory->memoryName = $request->memory_name;
            $memory->save();
            return redirect('admin/memorys/');
        }
    }


////////////////////////////////////////////

    public function showYears(Request $request)
    {
        $allyears = Year::all();
        $sql = "  
  
    SELECT Model2Year.YearPrice, Models.ModelName, Year.YearName, Model2Year.id
                FROM
                 Model2Year
                LEFT JOIN Models ON Model2Year.ModelId =Models.id
                LEFT JOIN Year ON Model2Year.YearId =Year.id
 
                
                
                
                ";
        $devices2year = DB::select($sql);

        $allModels = Models::all();
        return view('admin.years', ['years' => $allyears, 'devices2year' => $devices2year, 'models' => $allModels]);
    }

    public function addYear(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'year_name' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['error' => true, 'msg' => $errors]);
        }


        $year = Year::create(['YearName' => $request->year_name]);
        if (!empty($year)) {
            return redirect('admin/years/');
            return response()->json(['error' => false, 'path' => $ImagePath]);
        } else {
            return response()->json(['error' => true, 'msg' => 'Cant create year record']);
        }
    }


    public function deleteYear($yearId)
    {
        $year = Year::find($yearId);
        if (empty($year)) {
            abort(404);
        } else {
            $year->delete();
        }
        return redirect('admin/years/');
    }


    public function editYear($yearId)
    {
        $year = Year::find($yearId);
        if (empty($year)) {
            abort(404);
        }
        return view('admin.year', ['year' => $year]);
    }


    public function saveYear($yearId, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'year_name' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            return redirect('admin/year/update/' . $yearId)
                ->withErrors($validator)
                ->withInput();

        }
        $year = Year::find($yearId);
        if (empty($year)) {
            $validator->errors()->add('field', 'year not found!');
            return redirect('admin/year/update/' . $yearId)
                ->withErrors($validator)
                ->withInput();
        } else {
            $year->YearName = $request->year_name;
            $year->save();
            return redirect('admin/years/');
        }
    }


////////////////////////////////////////////

    public function showAccesories(Request $request)
    {
        $allaccesories = Accesories::all();
        $sql = "  
  
                SELECT Model2Accesories.AccesoriePrice, Models.ModelName, Accesoriers.AccesoriesName, Model2Accesories.id
                FROM
                 Model2Accesories
                LEFT JOIN Models ON Model2Accesories.ModelId =Models.id
                LEFT JOIN Accesoriers ON Model2Accesories.AccesorieId =Accesoriers.id
 
                
                
                
                ";
        $devices2accesorie = DB::select($sql);

        $allModels = Models::all();
        return view('admin.accesories', ['accesories' => $allaccesories, 'devices2accesorie' => $devices2accesorie, 'models' => $allModels]);
    }

    public function addAccesorie(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'accesorie_name' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['error' => true, 'msg' => $errors]);
        }


        $accesorie = Accesories::create(['AccesoriesName' => $request->accesorie_name]);
        if (!empty($accesorie)) {
            return redirect('admin/accesories/');
            return response()->json(['error' => false, 'path' => $ImagePath]);
        } else {
            return response()->json(['error' => true, 'msg' => 'Cant create accesorie record']);
        }
    }


    public function deleteAccesorie($accesorieId)
    {
        $accesorie = Accesories::find($accesorieId);
        if (empty($accesorie)) {
            abort(404);
        } else {
            $accesorie->delete();
        }
        return redirect('admin/accesories/');
    }


    public function editAccesorie($accesorieId)
    {
        $accesorie = Accesories::find($accesorieId);
        if (empty($accesorie)) {
            abort(404);
        }
        return view('admin.accesorie', ['accesorie' => $accesorie]);
    }


    public function saveAccesorie($accesorieId, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'accesorie_name' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            return redirect('admin/accesorie/update/' . $accesorieId)
                ->withErrors($validator)
                ->withInput();

        }
        $accesorie = Accesories::find($accesorieId);
        if (empty($accesorie)) {
            $validator->errors()->add('field', 'accesorie not found!');
            return redirect('admin/accesorie/update/' . $accesorieId)
                ->withErrors($validator)
                ->withInput();
        } else {
            $accesorie->AccesoriesName = $request->accesorie_name;
            $accesorie->save();
            return redirect('admin/accesories/');
        }
    }

////////////////////////////////////////////

    public function showCarriers(Request $request)
    {
        $allcarriers = Carriers::all();
        $sql = " 
                SELECT Model2Carriers.CarrierPrice, Models.ModelName, Carriers.CarrierName, Model2Carriers.id
                FROM
                 Model2Carriers
                LEFT JOIN Models ON Model2Carriers.ModelId =Models.id
                LEFT JOIN Carriers ON Model2Carriers.CarrierId =Carriers.id
 
                
                
                ";
        $devices2carrier = DB::select($sql);


        $allModels = Models::all();
        return view('admin.carriers', ['carriers' => $allcarriers, 'devices2carrier' => $devices2carrier, 'models' => $allModels]);
    }

    public function addCarrier(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'carrier_name' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['error' => true, 'msg' => $errors]);
        }

        $carrierImages = [
            "AT&T" => "/img/br1.png",
            "Sprint" => "/img/br2.png",
            "T-Mobile" => "/img/br3.png",
            "Verizon" => "/img/br4.png",
            "Metro PCS" => "/img/br5.png",
            "Factory Unlocked" => "/img/br6.png",
            "Boost Mobile" => "/img/br7.png",
            "Virgin Mobile" => "/img/br8.png",
            "Straight Talk" => "/img/br9.png",
            "U.S. Cellular" => "/img/br10.png",
            "Cricket" => "/img/br11.png",
            "Other Carriers" => "",
        ];


        $carrier = Carriers::create(['CarrierName' => $request->carrier_name, 'CarrierImage'=>$carrierImages[$request->carrier_name]]);
        if (!empty($carrier)) {
            return redirect('admin/carriers/');
            return response()->json(['error' => false, 'path' => $ImagePath]);
        } else {
            return response()->json(['error' => true, 'msg' => 'Cant create carrier record']);
        }
    }


    public function deleteCarrier($carrierId)
    {
        $carrier = Carriers::find($carrierId);
        if (empty($carrier)) {
            abort(404);
        } else {
            $carrier->delete();
        }
        return redirect('admin/carriers/');
    }


    public function editCarrier($carrierId)
    {
        $carrier = Carriers::find($carrierId);
        if (empty($carrier)) {
            abort(404);
        }
        return view('admin.carrier', ['carrier' => $carrier]);
    }


    public function saveCarrier($carrierId, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'carrier_name' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            return redirect('admin/carrier/update/' . $carrierId)
                ->withErrors($validator)
                ->withInput();

        }
        $carrier = Carriers::find($carrierId);
        if (empty($carrier)) {
            $validator->errors()->add('field', 'carrier not found!');
            return redirect('admin/carrier/update/' . $carrierId)
                ->withErrors($validator)
                ->withInput();
        } else {
            $carrier->CarrierName = $request->carrier_name;
            $carrier->save();
            return redirect('admin/carriers/');
        }
    }


############################
    public function showOrders()
    {
        return view('admin.orders');
    }

    public function showSeo()
    {

        $seo_home = Settings::where('key', 'seo_home')->first(['value']);
        $seo_sell = Settings::where('key', 'seo_sell')->first(['value']);
        $seo_register = Settings::where('key', 'seo_register')->first(['value']);

        $seo_login = Settings::where('key', 'seo_login')->first(['value']);
        $seo_terms = Settings::where('key', 'seo_terms')->first(['value']);
        $seo_faqs = Settings::where('key', 'seo_faqs')->first(['value']);
        $seo_contact = Settings::where('key', 'seo_contact')->first(['value']);


        return view('admin.seo', [


            'seo_home' => $seo_home->value,
            'seo_sell' => $seo_sell->value,
            'seo_register' => $seo_register->value,
            'seo_login' => $seo_login->value,
            'seo_terms' => $seo_terms->value,
            'seo_faqs' => $seo_faqs->value,
            'seo_contact' => $seo_contact->value,


        ]);
    }

    public function saveSeo(Request $request)
    {
        $allRequest = $request->all();
        foreach ($allRequest as $key => $value) {
            if (preg_match('/^seo/', $key)) {
                $settings = Settings::where('key', $key)->first();
                if (empty($settings)) {
                    Settings::create(['key' => $key, 'value' => $value]);
                } else {
                    $settings->value = $value;
                    $settings->save();
                }
            }
        }
        return redirect('admin/seo');
    }


    public function getContactUsRequests($type = 'all')
    {
        if ($type == 'all') {
            $allRequests = ContactUsRequest::all();
        } else {
            $allRequests = ContactUsRequest::where('status', $type)->get();

        }


        return view('admin.contactus', ['requests' => $allRequests]);

    }


    public function deleteContactUsRequests($id)
    {

        $Request = ContactUsRequest::find($id);

        if (!empty($Request)) {

            $Request->delete();
        }
        return redirect('admin/requests');


    }


    public function getCustomQuotes()
    {

        $allQuotes = CustomQuotes::all();

        return view('admin.quotes', ['quotes' => $allQuotes]);

    }

    public function saveContactUsRequests(Request $request)
    {

        $contactRequest = ContactUsRequest::find($request->requestId);
        if (empty($contactRequest)) {
            abort(404);
        }
        $contactRequest->status = $request->status;
        $contactRequest->message = $request->request_mesage;
        $contactRequest->save();

        return redirect('admin/requests');

    }


    public function getCustomerStories()
    {

        $allStories = CustomerStories::all();

        return view('admin.stories', ['stories' => $allStories]);

    }


    public function addStory(Request $request)
    {
        $ImagePath = '';
        $path = public_path() . '/pictures';
        if (!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }
        $path = public_path() . '/pictures/story';
        if (!File::exists($path)) {
            File::makeDirectory($path, $mode = 0777, true, true);
        }

        $validator = Validator::make($request->all(), [
            'customer_name' => 'required|max:255',
            'customer_position' => 'required|max:255',
            'customer_text' => 'required',
            'customer_img' => 'image',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['error' => true, 'msg' => $errors]);
        }

        if (!empty($request->file('customer_img'))) {
            if ($request->file('customer_img')->isValid()) {
                $ImagePath = $request->customer_img->store('/pictures/story', 'uploads');
            }

        }


        if (isset($request->storyId) and (!empty($request->storyId))) {

            $story = CustomerStories::find($request->storyId);
            if (empty($story)) {
                return redirect('admin/stories/');

            } else {

                $story->CustomerName = $request->customer_name;
                $story->CustomerPosition = $request->customer_position;
                $story->CustomerText = $request->customer_text;

                if (!empty($ImagePath)) {
                    $story->CustomerImage = $ImagePath;
                }

                $story->save();

            }

        } else {

            $story = CustomerStories::create([
                'CustomerName' => $request->customer_name,
                'CustomerImage' => $ImagePath,
                'CustomerPosition' => $request->customer_position,
                'CustomerText' => $request->customer_text
            ]);

        }


        if (!empty($story)) {
            return redirect('admin/stories/');

        } else {
            return response()->json(['error' => true, 'msg' => 'Cant create records']);
        }
    }

    public function deleteStory($id)
    {

        $story = CustomerStories::find($id);

        if (!empty($story)) {

            $story->delete();
        }
        return redirect('admin/stories');


    }


############################


}
