<?php
/**
 * Use for functions  link models to properties
 */

namespace App\Http\Controllers;

use App\Accesories;
use App\HardDrive;
use App\Memory;
use App\Processors;
use App\ScreenSize;
use App\Storage;
use Illuminate\Http\Request;
use App\Device2Conditions;
use App\Model2Accesories;
use App\Model2Carriers;

use App\Model2Colors;
use App\Model2Harddrives;
use App\Model2Memory;
use App\Model2Processors;
use App\Model2ScreenSizes;
use App\Model2Storage;
use App\Model2Year;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Devices;
use App\Conditions;
use App\Models;
use App\Colors;
use App\Year;
use App\Carriers;

class AdminLinkingController extends Controller
{
///////////////
    public function addCondition2Device(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'device_id' => 'required|integer',
            'condition_id' => 'required|integer',
            'price_percentage' => 'required|numeric',
            'deduct' => 'required|integer',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['error' => true, 'msg' => $errors]);
        }
        try {


            $model = Device2Conditions::create(['DeviceId' => $request->device_id, 'ConditionId' => $request->condition_id,
                'ConditionPricePercentage' => $request->price_percentage, 'deduct' => $request->deduct]);

        } catch (\Exception $e) {
            return response()->json(['error' => true, 'msg' => $e->getMessage()]);

        }
        if (!empty($model)) {
            return redirect('admin/conditions/');
            return response()->json(['error' => false]);
        } else {
            return response()->json(['error' => true, 'msg' => 'Cant create model record']);
        }
    }


    public function deleteCondition2Device($Id)
    {
        $model = Device2Conditions::find($Id);
        if (empty($model)) {
            abort(404);
        } else {
            $model->delete();
        }
        return redirect('admin/conditions/');
    }


    public function editCondition2Device($Id)
    {
        $model = Device2Conditions::find($Id);
        $allDevices = Devices::all();
        $allconditions = Conditions::all();

        if (empty($model)) {
            abort(404);
        }
        return view('admin.device2condition', ['model' => $model, 'devices' => $allDevices, 'conditions' => $allconditions]);
    }


    public function saveCondition2Device($Id, Request $request)
    {


        $validator = Validator::make($request->all(), [

            'device_id' => 'required|integer',
            'condition_id' => 'required|integer',
            'price_percentage' => 'required|numeric',
            'deduct' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return redirect('admin/device2condition/update/' . $Id)
                ->withErrors($validator)
                ->withInput();

        }


        $model = Device2Conditions::find($Id);
        if (empty($model)) {
            $validator->errors()->add('field', 'Device not found!');
            return redirect('admin/device/update/' . $Id)
                ->withErrors($validator)
                ->withInput();
        } else {


            try {

                $model->DeviceId = $request->device_id;
                $model->ConditionId = $request->condition_id;
                $model->ConditionPricePercentage = $request->price_percentage;
                $model->deduct = $request->deduct;
                $model->save();

            } catch (\Exception $e) {
                return response()->json(['error' => true, 'msg' => $e->getMessage()]);

            }


            return redirect('admin/conditions/');
        }
    }
/////////////////
///////////////
    public function addModel2Color(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'model_id' => 'required|integer',
            'color_id' => 'required|integer',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['error' => true, 'msg' => $errors]);
        }
        try {
            $model = Model2Colors::create(['ModelId' => $request->model_id, 'ColorId' => $request->color_id,
                'ColorPrice' => $request->price]);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'msg' => $e->getMessage()]);
        }
        if (!empty($model)) {
            return redirect('admin/colors/');
            return response()->json(['error' => false]);
        } else {
            return response()->json(['error' => true, 'msg' => 'Cant create model record']);
        }
    }


    public function deleteModel2Color($Id)
    {
        $model = Model2Colors::find($Id);
        if (empty($model)) {
            abort(404);
        } else {
            $model->delete();
        }
        return redirect('admin/colors/');
    }


    public function editModel2Color($Id)
    {
        $model = Model2Colors::find($Id);
        $allModels = Models::all();
        $allColors = Colors::all();

        if (empty($model)) {
            abort(404);
        }
        return view('admin.model2colors', ['Model2Colors' => $model, 'models' => $allModels, 'colors' => $allColors]);
    }


    public function saveModel2Color($Id, Request $request)
    {


        $validator = Validator::make($request->all(), [

            'model_id' => 'required|integer',
            'color_id' => 'required|integer',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return redirect('admin/device2condition/update/' . $Id)
                ->withErrors($validator)
                ->withInput();
        }

        $model = Model2Colors::find($Id);
        if (empty($model)) {
            $validator->errors()->add('field', 'Device not found!');
            return redirect('admin/model2color/update/' . $Id)
                ->withErrors($validator)
                ->withInput();
        } else {
            try {
                $model->ModelId = $request->model_id;
                $model->ColorId = $request->color_id;
                $model->ColorPrice = $request->price;
                $model->save();
            } catch (\Exception $e) {
                return response()->json(['error' => true, 'msg' => $e->getMessage()]);
            }

            return redirect('admin/colors/');
        }
    }
/////////////////


    public function addModel2Storage(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'model_id' => 'required|integer',
            'storage_id' => 'required|integer',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['error' => true, 'msg' => $errors]);
        }
        try {
            $model = Model2Storage::create(['ModelId' => $request->model_id, 'storageId' => $request->storage_id,
                'storagePrice' => $request->price]);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'msg' => $e->getMessage()]);
        }
        if (!empty($model)) {
            return redirect('admin/storages/');
            return response()->json(['error' => false]);
        } else {
            return response()->json(['error' => true, 'msg' => 'Cant create model record']);
        }
    }


    public function deleteModel2Storage($Id)
    {
        $model = Model2Storage::find($Id);
        if (empty($model)) {
            abort(404);
        } else {
            $model->delete();
        }
        return redirect('admin/storages/');
    }


    public function editModel2Storage($Id)
    {
        $model = Model2Storage::find($Id);
        $allModels = Models::all();
        $allstorages = Storage::all();

        if (empty($model)) {
            abort(404);
        }
        return view('admin.model2storage', ['Model2storages' => $model, 'models' => $allModels, 'storages' => $allstorages]);
    }


    public function saveModel2Storage($Id, Request $request)
    {


        $validator = Validator::make($request->all(), [

            'model_id' => 'required|integer',
            'storage_id' => 'required|integer',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return redirect('admin/device2condition/update/' . $Id)
                ->withErrors($validator)
                ->withInput();
        }

        $model = Model2Storage::find($Id);
        if (empty($model)) {
            $validator->errors()->add('field', 'Device not found!');
            return redirect('admin/model2storage/update/' . $Id)
                ->withErrors($validator)
                ->withInput();
        } else {
            try {
                $model->ModelId = $request->model_id;
                $model->storageId = $request->storage_id;
                $model->storagePrice = $request->price;
                $model->save();
            } catch (\Exception $e) {
                return response()->json(['error' => true, 'msg' => $e->getMessage()]);
            }

            return redirect('admin/storages/');
        }
    }
/////////////////

    public function addModel2ScreenSizes(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'model_id' => 'required|integer',
            'screensize_id' => 'required|integer',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['error' => true, 'msg' => $errors]);
        }
        try {
            $model = Model2ScreenSizes::create(['ModelId' => $request->model_id, 'ScreenSizeId' => $request->screensize_id,
                'ScreenSizePrice' => $request->price]);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'msg' => $e->getMessage()]);
        }
        if (!empty($model)) {
            return redirect('admin/screensizes/');
            return response()->json(['error' => false]);
        } else {
            return response()->json(['error' => true, 'msg' => 'Cant create model record']);
        }
    }


    public function deleteModel2ScreenSizes($Id)
    {
        $model = Model2ScreenSizes::find($Id);
        if (empty($model)) {
            abort(404);
        } else {
            $model->delete();
        }
        return redirect('admin/screensizes/');
    }


    public function editModel2ScreenSizes($Id)
    {
        $model = Model2ScreenSizes::find($Id);
        $allModels = Models::all();
        $allscreensizes = ScreenSize::all();

        if (empty($model)) {
            abort(404);
        }
        return view('admin.model2screensizes', ['Model2screensizess' => $model, 'models' => $allModels, 'screensizes' => $allscreensizes]);
    }


    public function saveModel2ScreenSizes($Id, Request $request)
    {


        $validator = Validator::make($request->all(), [

            'model_id' => 'required|integer',
            'screensize_id' => 'required|integer',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return redirect('admin/model2screensize/update/' . $Id)
                ->withErrors($validator)
                ->withInput();
        }

        $model = Model2ScreenSizes::find($Id);
        if (empty($model)) {
            $validator->errors()->add('field', 'Device not found!');
            return redirect('admin/model2screensizes/update/' . $Id)
                ->withErrors($validator)
                ->withInput();
        } else {
            try {
                $model->ModelId = $request->model_id;
                $model->ScreenSizeId = $request->screensize_id;
                $model->ScreenSizePrice = $request->price;
                $model->save();
            } catch (\Exception $e) {
                return response()->json(['error' => true, 'msg' => $e->getMessage()]);
            }

            return redirect('admin/screensizes/');
        }
    }
/////////////////

    public function addModel2Processor(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'model_id' => 'required|integer',
            'processor_id' => 'required|integer',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['error' => true, 'msg' => $errors]);
        }
        try {
            $model = Model2Processors::create(['ModelId' => $request->model_id, 'ProcessorId' => $request->processor_id,
                'ProcessorPrice' => $request->price]);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'msg' => $e->getMessage()]);
        }
        if (!empty($model)) {
            return redirect('admin/processors/');
            return response()->json(['error' => false]);
        } else {
            return response()->json(['error' => true, 'msg' => 'Cant create model record']);
        }
    }


    public function deleteModel2Processor($Id)
    {
        $model = Model2Processors::find($Id);
        if (empty($model)) {
            abort(404);
        } else {
            $model->delete();
        }
        return redirect('admin/processors/');
    }


    public function editModel2Processor($Id)
    {
        $model = Model2Processors::find($Id);
        $allModels = Models::all();
        $allprocessors = Processors::all();

        if (empty($model)) {
            abort(404);
        }
        return view('admin.model2processors', ['Model2processors' => $model, 'models' => $allModels, 'processors' => $allprocessors]);
    }


    public function saveModel2Processor($Id, Request $request)
    {


        $validator = Validator::make($request->all(), [

            'model_id' => 'required|integer',
            'processor_id' => 'required|integer',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return redirect('admin/device2condition/update/' . $Id)
                ->withErrors($validator)
                ->withInput();
        }

        $model = Model2Processors::find($Id);
        if (empty($model)) {
            $validator->errors()->add('field', 'Device not found!');
            return redirect('admin/model2processor/update/' . $Id)
                ->withErrors($validator)
                ->withInput();
        } else {
            try {
                $model->ModelId = $request->model_id;
                $model->ProcessorId = $request->processor_id;
                $model->ProcessorPrice = $request->price;
                $model->save();
            } catch (\Exception $e) {
                return response()->json(['error' => true, 'msg' => $e->getMessage()]);
            }

            return redirect('admin/processors/');
        }
    }
/////////////////

    public function addModel2HardDrive(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'model_id' => 'required|integer',
            'harddrive_id' => 'required|integer',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['error' => true, 'msg' => $errors]);
        }
        try {
            $model = Model2Harddrives::create(['ModelId' => $request->model_id, 'harddriveId' => $request->harddrive_id,
                'harddrivePrice' => $request->price]);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'msg' => $e->getMessage()]);
        }
        if (!empty($model)) {
            return redirect('admin/harddrives/');
            return response()->json(['error' => false]);
        } else {
            return response()->json(['error' => true, 'msg' => 'Cant create model record']);
        }
    }


    public function deleteModel2HardDrive($Id)
    {
        $model = Model2Harddrives::find($Id);
        if (empty($model)) {
            abort(404);
        } else {
            $model->delete();
        }
        return redirect('admin/harddrives/');
    }


    public function editModel2HardDrive($Id)
    {
        $model = Model2Harddrives::find($Id);
        $allModels = Models::all();
        $allharddrives = HardDrive ::all();

        if (empty($model)) {
            abort(404);
        }
        return view('admin.model2harddrives', ['Model2harddrives' => $model, 'models' => $allModels, 'harddrives' => $allharddrives]);
    }


    public function saveModel2HardDrive($Id, Request $request)
    {


        $validator = Validator::make($request->all(), [

            'model_id' => 'required|integer',
            'harddrive_id' => 'required|integer',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return redirect('admin/device2condition/update/' . $Id)
                ->withErrors($validator)
                ->withInput();
        }

        $model = Model2Harddrives::find($Id);
        if (empty($model)) {
            $validator->errors()->add('field', 'Device not found!');
            return redirect('admin/model2harddrive/update/' . $Id)
                ->withErrors($validator)
                ->withInput();
        } else {
            try {
                $model->ModelId = $request->model_id;
                $model->harddriveId = $request->harddrive_id;
                $model->harddrivePrice = $request->price;
                $model->save();
            } catch (\Exception $e) {
                return response()->json(['error' => true, 'msg' => $e->getMessage()]);
            }

            return redirect('admin/harddrives/');
        }
    }
/////////////////

    public function addModel2Memory(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'model_id' => 'required|integer',
            'memory_id' => 'required|integer',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['error' => true, 'msg' => $errors]);
        }
        try {
            $model = Model2Memory::create(['ModelId' => $request->model_id, 'memoryId' => $request->memory_id,
                'memoryPrice' => $request->price]);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'msg' => $e->getMessage()]);
        }
        if (!empty($model)) {
            return redirect('admin/memorys/');
            return response()->json(['error' => false]);
        } else {
            return response()->json(['error' => true, 'msg' => 'Cant create model record']);
        }
    }


    public function deleteModel2Memory($Id)
    {
        $model = Model2Memory::find($Id);
        if (empty($model)) {
            abort(404);
        } else {
            $model->delete();
        }
        return redirect('admin/memorys/');
    }


    public function editModel2Memory($Id)
    {
        $model = Model2Memory::find($Id);
        $allModels = Models::all();
        $allmemorys = Memory::all();

        if (empty($model)) {
            abort(404);
        }
        return view('admin.model2memorys', ['Model2memory' => $model, 'models' => $allModels, 'memorys' => $allmemorys]);
    }


    public function saveModel2Memory($Id, Request $request)
    {


        $validator = Validator::make($request->all(), [

            'model_id' => 'required|integer',
            'memory_id' => 'required|integer',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return redirect('admin/device2condition/update/' . $Id)
                ->withErrors($validator)
                ->withInput();
        }

        $model = Model2Memory::find($Id);
        if (empty($model)) {
            $validator->errors()->add('field', 'Device not found!');
            return redirect('admin/model2memory/update/' . $Id)
                ->withErrors($validator)
                ->withInput();
        } else {
            try {
                $model->ModelId = $request->model_id;
                $model->memoryId = $request->memory_id;
                $model->memoryPrice = $request->price;
                $model->save();
            } catch (\Exception $e) {
                return response()->json(['error' => true, 'msg' => $e->getMessage()]);
            }

            return redirect('admin/memorys/');
        }
    }
/////////////////
    public function addModel2Year(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'model_id' => 'required|integer',
            'year_id' => 'required|integer',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['error' => true, 'msg' => $errors]);
        }
        try {
            $model = Model2Year::create(['ModelId' => $request->model_id, 'yearId' => $request->year_id,
                'yearPrice' => $request->price]);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'msg' => $e->getMessage()]);
        }
        if (!empty($model)) {
            return redirect('admin/years/');
            return response()->json(['error' => false]);
        } else {
            return response()->json(['error' => true, 'msg' => 'Cant create model record']);
        }
    }


    public function deleteModel2Year($Id)
    {
        $model = Model2Year::find($Id);
        if (empty($model)) {
            abort(404);
        } else {
            $model->delete();
        }
        return redirect('admin/years/');
    }


    public function editModel2Year($Id)
    {
        $model = Model2Year::find($Id);
        $allModels = Models::all();
        $allyears = Year::all();

        if (empty($model)) {
            abort(404);
        }
        return view('admin.model2years', ['Model2years' => $model, 'models' => $allModels, 'years' => $allyears]);
    }


    public function saveModel2Year($Id, Request $request)
    {


        $validator = Validator::make($request->all(), [

            'model_id' => 'required|integer',
            'year_id' => 'required|integer',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return redirect('admin/device2condition/update/' . $Id)
                ->withErrors($validator)
                ->withInput();
        }

        $model = Model2Year::find($Id);
        if (empty($model)) {
            $validator->errors()->add('field', 'Device not found!');
            return redirect('admin/model2year/update/' . $Id)
                ->withErrors($validator)
                ->withInput();
        } else {
            try {
                $model->ModelId = $request->model_id;
                $model->yearId = $request->year_id;
                $model->yearPrice = $request->price;
                $model->save();
            } catch (\Exception $e) {
                return response()->json(['error' => true, 'msg' => $e->getMessage()]);
            }

            return redirect('admin/years/');
        }
    }
/////////////////
    public function addModel2Accesorie(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'model_id' => 'required|integer',
            'accesorie_id' => 'required|integer',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['error' => true, 'msg' => $errors]);
        }
        try {
            $model = Model2Accesories::create(['ModelId' => $request->model_id, 'AccesorieId' => $request->accesorie_id,
                'AccesoriePrice' => $request->price]);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'msg' => $e->getMessage()]);
        }
        if (!empty($model)) {
            return redirect('admin/accesories/');
            return response()->json(['error' => false]);
        } else {
            return response()->json(['error' => true, 'msg' => 'Cant create model record']);
        }
    }


    public function deleteModel2Accesorie($Id)
    {
        $model = Model2Accesories::find($Id);
        if (empty($model)) {
            abort(404);
        } else {
            $model->delete();
        }
        return redirect('admin/accesories/');
    }


    public function editModel2Accesorie($Id)
    {
        $model = Model2Accesories::find($Id);
        $allModels = Models::all();
        $allAccesories = Accesories::all();

        if (empty($model)) {
            abort(404);
        }
        return view('admin.model2Accesories', ['Model2accesories' => $model, 'models' => $allModels, 'accesories' => $allAccesories]);
    }


    public function saveModel2Accesorie($Id, Request $request)
    {


        $validator = Validator::make($request->all(), [

            'model_id' => 'required|integer',
            'accesorie_id' => 'required|integer',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return redirect('admin/device2condition/update/' . $Id)
                ->withErrors($validator)
                ->withInput();
        }

        $model = Model2Accesories::find($Id);
        if (empty($model)) {
            $validator->errors()->add('field', 'Device not found!');
            return redirect('admin/model2Accesorie/update/' . $Id)
                ->withErrors($validator)
                ->withInput();
        } else {
            try {
                $model->ModelId = $request->model_id;
                $model->AccesorieId = $request->accesorie_id;
                $model->AccesoriePrice = $request->price;
                $model->save();
            } catch (\Exception $e) {
                return response()->json(['error' => true, 'msg' => $e->getMessage()]);
            }

            return redirect('admin/accesories/');
        }
    }
/////////////////
    public function addModel2Carrier(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'model_id' => 'required|integer',
            'carrier_id' => 'required|integer',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return response()->json(['error' => true, 'msg' => $errors]);
        }
        try {
            $model = Model2Carriers::create(['ModelId' => $request->model_id, 'carrierId' => $request->carrier_id,
                'carrierPrice' => $request->price]);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'msg' => $e->getMessage()]);
        }
        if (!empty($model)) {
            return redirect('admin/carriers/');
            return response()->json(['error' => false]);
        } else {
            return response()->json(['error' => true, 'msg' => 'Cant create model record']);
        }
    }


    public function deleteModel2Carrier($Id)
    {
        $model = Model2Carriers::find($Id);
        if (empty($model)) {
            abort(404);
        } else {
            $model->delete();
        }
        return redirect('admin/carriers/');
    }


    public function editModel2Carrier($Id)
    {
        $model = Model2Carriers::find($Id);
        $allModels = Models::all();
        $allcarriers = Carriers::all();

        if (empty($model)) {
            abort(404);
        }
        return view('admin.model2carriers', ['Model2carrier' => $model, 'models' => $allModels, 'carriers' => $allcarriers]);
    }


    public function saveModel2Carrier($Id, Request $request)
    {


        $validator = Validator::make($request->all(), [

            'model_id' => 'required|integer',
            'carrier_id' => 'required|integer',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return redirect('admin/device2condition/update/' . $Id)
                ->withErrors($validator)
                ->withInput();
        }

        $model = Model2Carriers::find($Id);
        if (empty($model)) {
            $validator->errors()->add('field', 'Device not found!');
            return redirect('admin/model2carrier/update/' . $Id)
                ->withErrors($validator)
                ->withInput();
        } else {
            try {
                $model->ModelId = $request->model_id;
                $model->carrierId = $request->carrier_id;
                $model->carrierPrice = $request->price;
                $model->save();
            } catch (\Exception $e) {
                return response()->json(['error' => true, 'msg' => $e->getMessage()]);
            }

            return redirect('admin/carriers/');
        }
    }
/////////////////
}
