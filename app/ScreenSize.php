<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScreenSize extends Model
{
    protected $table='ScreenSizes';
    protected $guarded=['id'];
}
