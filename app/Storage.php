<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Storage extends Model
{
    protected $table='Storage';
    protected $guarded=['id'];
}
