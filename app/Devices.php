<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Devices extends Model
{
    protected $table='Devices';
    protected $guarded=['id'];


    public function models (){
        return $this->hasMany('App\Models', 'DeviceId', 'id');
    }
}
