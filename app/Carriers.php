<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carriers extends Model
{
    protected $table='Carriers';
    protected $guarded=['id'];
}
