<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerStories extends Model
{
    protected $table='CustomerStories';
    protected $guarded=['id'];

}
