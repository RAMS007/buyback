<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index');
Route::get('/index1', 'MainController@index1');

Route::get('logout', 'Auth\LoginController@logout')->name('logout');
//Auth::routes();



// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

Route::post('register/payments', 'Auth\RegisterController@registerPayments');
Route::post('register/shipping', 'Auth\RegisterController@registerShipping');


// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');


Route::get('profile', 'MainController@showUserProfile');
Route::get('profile/orders', 'MainController@showUserProfileOrders');
Route::get('profile/edit', 'MainController@showEditProfile');
Route::post('profile/edit', 'MainController@saveProfile');



Route::post('/calculate', 'MainController@calculatePrice');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/device/{id}', 'MainController@getDeviceModels');
Route::get('/model/{id}', 'MainController@getModelParametrs');
Route::post('/calculate', 'MainController@calculatePrice');

Route::get('aboutUs', 'MainController@showAboutUs');
Route::get('contactUs', 'MainController@showContactUs');
Route::post('contactUs', 'MainController@sendMessage');
Route::get('faq', 'MainController@showFaq');

Route::get('privacy', 'MainController@showPrivacy');
Route::get('terms', 'MainController@showTerms');
Route::get('quotes', 'MainController@showQuotes');
Route::post('quotes', 'MainController@saveQuotes');


Route::post('order', 'MainController@createOrder');



Route::group(['prefix' => 'admin',  'middleware' => 'checkAdmin'], function(){
    Route::get('/', 'AdminController@Dashboard');

    Route::get('/request/delete/{Id}', 'AdminController@deleteContactUsRequests');
    Route::get('/requests/{type?}', 'AdminController@getContactUsRequests');

    Route::post('/requests/save', 'AdminController@saveContactUsRequests');

    Route::get('/quotes', 'AdminController@getCustomQuotes');

    Route::get('/stories', 'AdminController@getCustomerStories');
    Route::post('/stories/add', 'AdminController@addStory');
    Route::get('/story/delete/{Id}', 'AdminController@deleteStory');


//////////////////////////////////////////
    Route::get('/devices', 'AdminController@showDevices');
    Route::post('/devices/add', 'AdminController@addDevice');
    Route::get('/device/update/{deviceId}', 'AdminController@editDevice');
    Route::post('/device/update/{deviceId}', 'AdminController@saveDevice');
    Route::get('/device/delete/{deviceId}', 'AdminController@deleteDevice');

    ////////////////////////////////////
    Route::get('/models', 'AdminController@showModels');
    Route::post('/models/add', 'AdminController@addModel');
    Route::get('/model/update/{modelId}', 'AdminController@editModel');
    Route::post('/model/update/{modelId}', 'AdminController@saveModel');
    Route::get('/model/delete/{modelId}', 'AdminController@deleteModel');
##########################################
    ////////////////////////////////////
    Route::get('/conditions', 'AdminController@showConditions');
    Route::post('/conditions/add', 'AdminController@addCondition');
    Route::get('/condition/update/{conditionId}', 'AdminController@editCondition');
    Route::post('/condition/update/{conditionId}', 'AdminController@saveCondition');
    Route::get('/condition/delete/{conditionId}', 'AdminController@deleteCondition');


    ////////////////////////////////////
    Route::get('/colors', 'AdminController@showColors');
    Route::post('/color/add', 'AdminController@addColor');
    Route::get('/color/update/{colorId}', 'AdminController@editColor');
    Route::post('/color/update/{colorId}', 'AdminController@saveColor');
    Route::get('/color/delete/{colorId}', 'AdminController@deleteColor');

    ////////////////////////////////////
    Route::get('/storages', 'AdminController@showStorages');
    Route::post('/storages/add', 'AdminController@addStorage');
    Route::get('/storage/update/{storageId}', 'AdminController@editStorage');
    Route::post('/storage/update/{storageId}', 'AdminController@saveStorage');
    Route::get('/storage/delete/{storageId}', 'AdminController@deleteStorage');


    ////////////////////////////////////
    Route::get('/screensizes', 'AdminController@showScreenSizes');
    Route::post('/screenSize/add', 'AdminController@addScreenSize');
    Route::get('/screenSize/update/{screenSizeId}', 'AdminController@editScreenSize');
    Route::post('/screenSize/update/{screenSizeId}', 'AdminController@saveScreenSize');
    Route::get('/screenSize/delete/{screenSizeId}', 'AdminController@deleteScreenSize');




    ////////////////////////////////////
    Route::get('/processors', 'AdminController@showProcessors');
    Route::post('/processor/add', 'AdminController@addProcessor');
    Route::get('/processor/update/{processorId}', 'AdminController@editProcessor');
    Route::post('/processor/update/{processorId}', 'AdminController@saveProcessor');
    Route::get('/processor/delete/{processorId}', 'AdminController@deleteProcessor');



    ////////////////////////////////////
    Route::get('/harddrives', 'AdminController@showHarddrives');
    Route::post('/harddrives/add', 'AdminController@addHarddrive');
    Route::get('/harddrive/update/{harddriveId}', 'AdminController@editHarddrive');
    Route::post('/harddrive/update/{harddriveId}', 'AdminController@saveHarddrive');
    Route::get('/harddrive/delete/{harddriveId}', 'AdminController@deleteHarddrive');


    ////////////////////////////////////
    Route::get('/memorys', 'AdminController@showMemorys');
    Route::post('/memorys/add', 'AdminController@addMemory');
    Route::get('/memory/update/{memoryId}', 'AdminController@editMemory');
    Route::post('/memory/update/{memoryId}', 'AdminController@saveMemory');
    Route::get('/memory/delete/{memoryId}', 'AdminController@deleteMemory');


    ////////////////////////////////////
    Route::get('/years', 'AdminController@showYears');
    Route::post('/years/add', 'AdminController@addYear');
    Route::get('/year/update/{yearId}', 'AdminController@editYear');
    Route::post('/year/update/{yearId}', 'AdminController@saveYear');
    Route::get('/year/delete/{yearId}', 'AdminController@deleteYear');


    ////////////////////////////////////
    Route::get('/accesories', 'AdminController@showAccesories');
    Route::post('/accesories/add', 'AdminController@addAccesorie');
    Route::get('/accesorie/update/{accesorieId}', 'AdminController@editAccesorie');
    Route::post('/accesorie/update/{accesorieId}', 'AdminController@saveAccesorie');
    Route::get('/accesorie/delete/{accesorieId}', 'AdminController@deleteAccesorie');


    ////////////////////////////////////
    Route::get('/carriers', 'AdminController@showCarriers');
    Route::post('/carriers/add', 'AdminController@addCarrier');
    Route::get('/carrier/update/{carrierId}', 'AdminController@editCarrier');
    Route::post('/carrier/update/{carrierId}', 'AdminController@saveCarrier');
    Route::get('/carrier/delete/{carrierId}', 'AdminController@deleteCarrier');

  /////////////////////////////////////

    Route::get('/orders', 'AdminController@showOrders');
    Route::get('/seo', 'AdminController@showSeo');
    Route::post('/seo', 'AdminController@saveSeo');



///////////////Linking  block/////////////////////////
    //Condition2Device

    Route::post('/device2condition/add', 'AdminLinkingController@addCondition2Device');
    Route::get('/device2condition/update/{Id}', 'AdminLinkingController@editCondition2Device');
    Route::post('/device2condition/update/{Id}', 'AdminLinkingController@saveCondition2Device');
    Route::get('/device2condition/delete/{Id}', 'AdminLinkingController@deleteCondition2Device');

    Route::post('/model2color/add', 'AdminLinkingController@addModel2Color');
    Route::get('/model2color/update/{Id}', 'AdminLinkingController@editModel2Color');
    Route::post('/model2color/update/{Id}', 'AdminLinkingController@saveModel2Color');
    Route::get('/model2color/delete/{Id}', 'AdminLinkingController@deleteModel2Color');


    Route::post('/model2storage/add', 'AdminLinkingController@addModel2Storage');
    Route::get('/model2storage/update/{Id}', 'AdminLinkingController@editModel2Storage');
    Route::post('/model2storage/update/{Id}', 'AdminLinkingController@saveModel2Storage');
    Route::get('/model2storage/delete/{Id}', 'AdminLinkingController@deleteModel2Storage');


    Route::post('/model2screensizes/add', 'AdminLinkingController@addModel2ScreenSizes');
    Route::get('/model2screensize/update/{Id}', 'AdminLinkingController@editModel2ScreenSizes');
    Route::post('/model2screensize/update/{Id}', 'AdminLinkingController@saveModel2ScreenSizes');
    Route::get('/model2screensize/delete/{Id}', 'AdminLinkingController@deleteModel2ScreenSizes');


    Route::post('/model2processors/add', 'AdminLinkingController@addModel2Processor');
    Route::get('/model2processor/update/{Id}', 'AdminLinkingController@editModel2Processor');
    Route::post('/model2processor/update/{Id}', 'AdminLinkingController@saveModel2Processor');
    Route::get('/model2processor/delete/{Id}', 'AdminLinkingController@deleteModel2Processor');


    Route::post('/model2harddrives/add', 'AdminLinkingController@addModel2HardDrive');
    Route::get('/model2harddrive/update/{Id}', 'AdminLinkingController@editModel2HardDrive');
    Route::post('/model2harddrive/update/{Id}', 'AdminLinkingController@saveModel2HardDrive');
    Route::get('/model2harddrive/delete/{Id}', 'AdminLinkingController@deleteModel2HardDrive');


    Route::post('/model2memorys/add', 'AdminLinkingController@addModel2Memory');
    Route::get('/model2memory/update/{Id}', 'AdminLinkingController@editModel2Memory');
    Route::post('/model2memory/update/{Id}', 'AdminLinkingController@saveModel2Memory');
    Route::get('/model2memory/delete/{Id}', 'AdminLinkingController@deleteModel2Memory');


    Route::post('/model2years/add', 'AdminLinkingController@addModel2Year');
    Route::get('/model2year/update/{Id}', 'AdminLinkingController@editModel2Year');
    Route::post('/model2year/update/{Id}', 'AdminLinkingController@saveModel2Year');
    Route::get('/model2year/delete/{Id}', 'AdminLinkingController@deleteModel2Year');

    Route::post('/model2accesories/add', 'AdminLinkingController@addModel2Accesorie');
    Route::get('/model2accesorie/update/{Id}', 'AdminLinkingController@editModel2Accesorie');
    Route::post('/model2accesorie/update/{Id}', 'AdminLinkingController@saveModel2Accesorie');
    Route::get('/model2accesorie/delete/{Id}', 'AdminLinkingController@deleteModel2Accesorie');


    Route::post('/model2carriers/add', 'AdminLinkingController@addModel2Carrier');
    Route::get('/model2carrier/update/{Id}', 'AdminLinkingController@editModel2Carrier');
    Route::post('/model2carrier/update/{Id}', 'AdminLinkingController@saveModel2Carrier');
    Route::get('/model2carrier/delete/{Id}', 'AdminLinkingController@deleteModel2Carrier');




    /////////End linking////////////////

/*
    ////////////////////////////////////
    Route::get('/colors', 'AdminController@showColors');
    Route::post('/colors/add', 'AdminController@addColor');
    Route::get('/color/update/{colorId}', 'AdminController@editColor');
    Route::post('/color/update/{colorId}', 'AdminController@saveColor');
    Route::get('/color/delete/{colorId}', 'AdminController@deleteColor');

    ////////////////////////////////////
    Route::get('/colors', 'AdminController@showColors');
    Route::post('/colors/add', 'AdminController@addColor');
    Route::get('/color/update/{colorId}', 'AdminController@editColor');
    Route::post('/color/update/{colorId}', 'AdminController@saveColor');
    Route::get('/color/delete/{colorId}', 'AdminController@deleteColor');

*/


});



